import React, {useState, useEffect} from 'react';
import { StyleSheet, ScrollView, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { Card, Button, Text, Provider, IconButton, Surface, Paragraph, Dialog, Portal, DarkTheme } from 'react-native-paper';

import colors from '../assets/colors/colors';
import { vaciarCarrito, getCarrito, quitarItem, vaciarPedidos } from '../components/auth/auth'

const CarritoScreen = ({navigation, seguirComprando, carrito}) => {
    const [pedidos, setPedidos] = useState();

    // console.log('CarritoScreen')
    // console.log(carrito)
    // console.log(pedidos)

    useEffect( () => {
        // console.log('useEffect')
        // console.log(carrito)

        let restaurantes = [];
        restaurantes =[...new Set(carrito.map(x => x.idRestaurante))]
     
        let listaPedidos = [];
        let pedido = null;
        restaurantes.forEach(r => {
            let carritoRestaurante = carrito.filter((item) => item.idRestaurante == r)

            if(carritoRestaurante.length > 0){
                let restaurante = {
                    idRestaurante: carritoRestaurante[0].idRestaurante, 
                    emailRestaurante: carritoRestaurante[0].emailRestaurante,
                    nombreRestaurante: carritoRestaurante[0].nombreRestaurante,
                    imagenRestaurante: carritoRestaurante[0].imagenRestaurante,
                    costoEnvio: carritoRestaurante[0].costoEnvio,
                }

                let precioTotal = 0
                let productos = carritoRestaurante.map((menu) => 
                {
                    precioTotal += menu.total;
                    return {
                        id: menu.id,
                        titulo: menu.titulo,
                        imagen: menu.imagen,
                        precio: menu.precio,
                        descuento: menu.descuento,
                        total: menu.total,
                        //aclaraciones: menu.aclaraciones,
                    }
                })

                pedido = {
                    restaurante: restaurante,
                    productos: productos,
                    precioTotal: precioTotal
                }
                listaPedidos.push(pedido);
            }
        });
        // console.log('listaPedidos')
        // console.log(listaPedidos)
        setPedidos(listaPedidos);

        return function() {
            //console.log('---')
        };
    }, [carrito]);


    const crearPedido = () => {
        navigation.push('ConfirmarPedido', { pedidos:pedidos })
    }

    const eliminarProducto = (prod) => {
        quitarItem(prod)
    }  

    const carritoLimpiar = () => {
        setPedidos();
        vaciarCarrito();
        setVisible(false);
    }

    const [visible, setVisible] = useState(false);
    const showDialog = () => setVisible(true);
    const hideDialog = () => setVisible(false);

    return(
        <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
            <Provider>
                <Card style={styles.card}>
                    {pedidos && pedidos.length > 0 ?
                        <ScrollView style={{maxHeight: "100%"}}>
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 10}}>
                                {pedidos && pedidos.map((pedido, index) => {
                                    return(
                                        <Surface key={index} style={styles.left}>

                                            <View style={styles.restaurante}>
                                                {pedido.restaurante.imagenRestaurante  && pedido.restaurante.imagenRestaurante != "" ?
                                                    <Image style={styles.imagen} source={{uri: pedido.restaurante.imagenRestaurante}}/>
                                                : null }
                                                <View>
                                                    <Text style={styles.textTitulo}>{pedido.restaurante.nombreRestaurante}</Text>
                                                    <Text style={styles.text}>Envío $ {pedido.restaurante.costoEnvio}</Text>
                                                </View>
                                            </View>

                                            <View style={styles.listaProductos}>
                                                {pedido.productos.map((prod, _index) => {
                                                    return(
                                                        <Surface key={_index} style={styles.producto}>
                                                            <View style={{alignItems: 'center', flexDirection: 'row'}}>
                                                                <Image style={{height:35, width:35}} source={{uri: prod.imagen}}/>
                                                                <Text style={styles.text}>  {prod.titulo}</Text>
                                                            </View>
                                                            
                                                            <View style={{alignItems: 'center', flexDirection: 'row'}}>
                                                                {prod.descuento && prod.descuento != 0 ? 
                                                                    <View style={{alignItems: 'center', flexDirection: 'row', marginRight:5}}>
                                                                        <Text style={styles.textPrecioOg}>$ {prod.precio}</Text>
                                                                        <Text style={styles.textDescuento}> %{prod.descuento}</Text>
                                                                    </View>
                                                                : null }
                                                                <Text style={styles.text}>$ {prod.total}</Text>
                                                            
                                                                <IconButton
                                                                    icon="trash-can"
                                                                    color={colors.red}
                                                                    size={25}
                                                                    onPress={() => eliminarProducto(prod)}
                                                                />
                                                            </View>
                                                        </Surface>
                                                    )
                                                })}
                                            </View>

                                            <View style={styles.precioTotal}>
                                                <Text style={styles.textTitulo}>Total: $ {pedido.precioTotal}</Text>
                                            </View>
                                        </Surface>
                                    )
                                })}
                            </View>
                        </ScrollView>
                    :
                        <Surface style={styles.sinItems}>
                            <Text style={styles.textTitulo}>No tienes elementos en el carrito aún</Text>
                        </Surface>
                    }
                </Card>


                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity onPress={showDialog} style={styles.vaciarCarrito}>
                        <Text style={styles.vaciarCarritoText}>Vaciar carrito</Text>
                    </TouchableOpacity>
                </View>

                {/* <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity onPress={() => vaciarPedidos()} style={styles.vaciarCarrito}>
                        <Text style={styles.vaciarCarritoText}>Vaciar pedidos (test)</Text>
                    </TouchableOpacity>
                </View> */}
                
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                    <Button mode="contained" style={styles.btn} onPress={seguirComprando} >Seguir comprando</Button>

                    <Button mode="contained" style={styles.btn} onPress={crearPedido} disabled={!pedidos || pedidos.length == 0} >Confirmar pedido</Button>
                </View>
                
                <Portal>
                    <Dialog visible={visible} onDismiss={hideDialog} theme={DarkTheme}>
                        <Dialog.Content>
                        <Paragraph style={styles.text}>¿Estas seguro que deseas vaciar el carrito?</Paragraph>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button theme={{ colors: {primary : colors.red}}} onPress={hideDialog}>Cancelar</Button>
                            <Button theme={{ colors: {primary : colors.red}}} onPress={carritoLimpiar}>   Confirmar</Button>
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </Provider>
        </ImageBackground>
    )
}
export default CarritoScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    card: {
        flex: 8,
        backgroundColor: colors.background,
        margin: 10,
        borderRadius: 10,
        borderWidth: 3,
        minWidth: "95%",
        //minHeight: "70%"
    },
    restaurante:{
        flex:1, 
        justifyContent: 'flex-start', 
        alignItems: 'center', 
        flexDirection: 'row', 
        margin: 10,
    },
    imagen: {
        height:50, 
        width:50, 
        marginRight: 10,
        borderRadius: 10,
        overflow: "hidden",
        borderWidth: 3,
    },
    text: {
        color: "white",
        fontSize: 14
    },
    textTitulo: {
        color: "white",
        fontSize: 17,
        fontWeight: 'bold'
    },
    textPrecio: {
        //marginVertical: 10,
        color: "white",
        fontSize: 16
    },
    textProductos: {
        marginVertical: 10,
        color: "white",
        fontSize: 16
    },
    textPrecioOg: {
        color: colors.red,
        fontSize: 12,
        textDecorationLine: 'line-through'
    },
    textDescuento: {
        color: "white",
        fontSize: 12,
    },
    left: {
        flex: 1, 
        width: "95%", 
        justifyContent: 'center', 
        alignItems: 'flex-start',
        backgroundColor: colors.backgroundColor,
        padding: 5,
        elevation: 2,
        borderRadius: 2,
        marginVertical: 5,
    },
    sinItems: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: colors.backgroundColor,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
        margin: 5,
    },
    right: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'flex-end'
    },
    precioTotal: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'flex-end',
        padding: 10,
    },
    center: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    listaProductos: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'center',
        padding: 5,
    },
    producto: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        backgroundColor: colors.backgroundColor,
        paddingLeft: 10,
        elevation: 2,
        borderRadius: 2,
    },
    btn: {
        marginVertical: 10,
        marginHorizontal: 3,
        color: colors.white,
        backgroundColor: colors.red,
    },
    textInput: {
        width: "100%",
        backgroundColor: "#ffffff00",
        fontSize: 15,
        marginBottom: 10
    },
    cantBtn: {
        marginHorizontal: 10,
    },
    vaciarCarrito: {
        marginVertical: 10,
    },
    vaciarCarritoText: {
        color: colors.red,
        fontSize: 16,
        textDecorationLine: 'underline',
    },
});