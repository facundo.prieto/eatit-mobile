import React, {useState, useEffect} from 'react';
import { View, StyleSheet, ImageBackground, Image } from 'react-native';
import { Card, Button, Paragraph, Dialog, Portal, Provider, Text, TextInput, DarkTheme } from 'react-native-paper';

import colors from '../assets/colors/colors';
import axiosCliente from "../components/axios/AxiosCliente";
import axiosUsuario from "../components/axios/AxiosUsuario";
import { setSession } from "../components/auth/auth";

const CrearCliente = ({route, navigation}) => {
    
    // console.log("CrearCliente ") 

    let esGoogleUser = null;
    let user = null;
    let googleToken = null;
    if(route && route.params && route.params.esGoogleUser){
        esGoogleUser = route.params.esGoogleUser;
    }
    if(route && route.params && route.params.user){
        user = route.params.user;
    }
    if(route && route.params && route.params.googleToken){
        googleToken = route.params.googleToken;
    }

    // console.log("xxxxxx ")
    // console.log("navigation ", navigation)
    // console.log("esGoogleUser ", esGoogleUser)
    // console.log("user ",user)
    // console.log("googleToken ",googleToken)
    // console.log("zzzzzz ")

    const [visibleMsgSucces, setVisibleSucces] = useState(false);
    const [visibleMsgError, setVisibleError] = useState(false);
    const [errorTexto, setErrorTexto] = useState('');
    const [nombre, setNombre] = useState('');
    const [apellido, setApellido] = useState('');
    //const [celular, setCelular] = useState('');
    const [email, setEmail] = useState('');
    const [contrasena, setContrasena] = useState('');
    const [repcontrasena, setRepContrasena] = useState('');

    const onChangeNombre = nombre => setNombre(nombre);
    const onChangeApellido = apellido => setApellido(apellido);
    //const onChangeCelular = celular => setCelular(celular);
    const onChangeEmail = email => setEmail(email);
    const onChangeContrasena = contrasena => setContrasena(contrasena);
    const onChangeRepContrasena = repcontrasena => setRepContrasena(repcontrasena);

    const [mailValido, setMailValido] = useState(false);
    const validate = (text) => {
        //console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(text) === false) {
            //console.log("Email is Not Correct");
            setEmail(text);
            setMailValido(false);
        }
        else {
            setEmail(text);
            setMailValido(true);
            //console.log("Email is Correct");
        }
    }

    useEffect(() => {
        // console.log('useEffect')
        // console.log(esGoogleUser) 
        // console.log(user) 
        if(esGoogleUser && user){
            let listN = user.displayName.split(' '); // returns ["Paul", "Steve", "Panakkal"]

            // console.log("listN ", listN)
            setNombre(listN[0]);
            setApellido(listN[1]);
            setEmail(user.email);
        }
        
    }, []);

    const crearCliente = () => {
        // console.log("crearCliente ");
        if(contrasena != repcontrasena){
            setErrorTexto("Debe ingresar la misma contraseña dos veces.");
            setVisibleError(true);
        }else{

            if(!mailValido){
                setErrorTexto("La dirección de correo ingresada no es válida.");
                setVisibleError(true);
            }else{

                //Mando los datos del state al crearUsuarioCliente del ClienteController
                axiosCliente
                .post("/crear", {
                    nombre: nombre,
                    apellido: apellido,
                    googleUser: false,
                    email: email,
                    password: contrasena,
                    direcciones: []
                })
                .then(function (response) {
                    // console.log("response", response);
                    
                    if(response.status == 200){
                        setErrorTexto(response.data);
                        axiosUsuario
                            .post("/login", {
                            email: email,
                            password: contrasena,
                            })
                            .then(function (response) {
                                if (response.status == 200) {
                                    setVisibleSucces(true);
                                    setSession({
                                        nombre: response.data.nombre,
                                        email: response.data.email,
                                        token: response.data.token,
                                        direcciones: response.data.direcciones,
                                        rol: response.data.rol,
                                    });
                                } else {
                                    setErrorTexto(response.data);
                                    setVisibleError(true);
                                }
                                })
                            .catch(function (error) {
                            console.log(error);
                            });
                    } else {
                        setErrorTexto(response.data);
                        setVisibleError(true);
                    }
                })
                .catch(function (error) {
                    // console.log("error.response ", JSON.stringify(error.response));
                    if (error.response.status == 400) {
                        // Request made and server responded
                        setErrorTexto(error.response.data);
                        setVisibleError(true);
                    } else {
                        if(error.response.status == 500 && error.response.data.includes("El Cliente ya existe")){
                            setVerRechazoMsg(true);
                        } else {
                            // Something happened in setting up the request that triggered an Error
                            setErrorTexto(error.message);
                            setVisibleError(true);
                        }
                    }
                });
            }
        }
    }

    const crearClienteGoogle = () => {
        //Mando los datos del state al crearUsuarioCliente del ClienteController
        // console.log('crearClienteGoogle');
        axiosCliente
        .post("/crear", {
            nombre: nombre,
            apellido: apellido,
            googleUser: true,
            email: email,
            password: "",
            direcciones: []
        })
        .then(function (response) {
            // console.log(response);
            // console.log('crear response', response.status);
            if(response.status == 200){
                setErrorTexto(response.data);
                axiosUsuario
                    .post("/login/google", {
                        token: googleToken,
                    })
                    .then(function (response) {
                    if (response.status == 200) {
                        setVisibleSucces(true);
                        setSession({
                            nombre: response.data.nombre,
                            email: response.data.email,
                            token: response.data.token,
                            direcciones: response.data.direcciones,
                            rol: response.data.rol,
                        });
                    } else {
                        setErrorTexto(response.data);
                        setVisibleError(true);
                    }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                setErrorTexto(response.data);
                setVisibleError(true);
            }
        })
        .catch(function (error) {
            //console.log('crear error', error.message);
            if (error.response.status == 400) {
                // Request made and server responded
                setErrorTexto(error.response.data);
                setVisibleError(true);
            } else {
                // Something happened in setting up the request that triggered an Error
                setErrorTexto(error.message);
                setVisibleError(true);
            }
        });
    }

    const hideDialogSucces = () => {
        setVisibleSucces(false);
        navigation.navigate('Direcciones')
    }

    const hideDialogError = () => {
        setVisibleError(false);
    }

    const [visibleRechazoMsg, setVerRechazoMsg] = useState(false);

    return(
        <Provider>
            <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
                <Image style={styles.tinyLogo} source={require('../assets/images/logoLetras.png')}/>
                <Card style={styles.card}>

                    <TextInput dense={true} mode="flat" label="Nombre" value={nombre} 
                        onChangeText={onChangeNombre} 
                        style={styles.textInput} 
                        theme={{ colors: { text: colors.white, primary: colors.white} }}
                    />

                    <TextInput dense={true} mode="flat" label="Apellido" value={apellido} 
                        onChangeText={onChangeApellido} 
                        style={styles.textInput} 
                        theme={{ colors: { text: colors.white, primary: colors.white} }}
                    />

                    {/* <TextInput dense={true} mode="flat" label="Celular" value={celular} 
                        onChangeText={onChangeCelular} 
                        style={styles.textInput} 
                        theme={{ colors: { text: colors.white, primary: colors.white} }}
                    /> */}

                    {esGoogleUser && user ? 
                        <Button mode="contained" style={styles.btn} onPress={crearClienteGoogle} 
                            disabled={nombre=='' && apellido=='' && email==''}
                        >Comenzar</Button>
                    :
                        <View>

                            <TextInput dense={true} mode="flat" label="Correo electrónico" value={email} 
                                //onChangeText={onChangeEmail} 
                                onChangeText={(text) => validate(text)}
                                style={styles.textInput} 
                                theme={{ colors: { text: colors.white, primary: colors.white} }}
                            />

                            <TextInput dense={true} mode="flat" label="Contraseña" value={contrasena} secureTextEntry={true}
                                onChangeText={onChangeContrasena} 
                                style={styles.textInput} 
                                theme={{ colors: { text: colors.white, primary: colors.white} }}
                            />

                            <TextInput dense={true} mode="flat" label="Repetir contraseña" value={repcontrasena} secureTextEntry={true}
                                onChangeText={onChangeRepContrasena} 
                                style={styles.textInput} 
                                theme={{ colors: { text: colors.white, primary: colors.white} }}
                            />

                            <Button mode="contained" style={styles.btn} onPress={crearCliente} 
                                disabled={nombre=='' && apellido=='' && email=='' && contrasena=='' && repcontrasena==''}
                            >Comenzar</Button>
                        </View>
                    }
                    <Portal>
                        <Dialog visible={visibleMsgSucces} onDismiss={hideDialogSucces} style={styles.alert}>
                            <Dialog.Title style={styles.alertText}>Bienvenido a EatIT!</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph style={styles.alertText}>Por favor ingresa una dirección a donde se te entregarán tus pedidos.</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button mode="contained" style={styles.btn} onPress={hideDialogSucces}>Vamos!</Button>
                            </Dialog.Actions>
                        </Dialog>
                        <Dialog visible={visibleMsgError} onDismiss={hideDialogError} style={styles.alert}>
                            <Dialog.Title style={styles.alertText}>Ha ocurrido un error:</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph style={styles.alertText}>{errorTexto}</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button mode="contained" style={styles.btn} onPress={hideDialogError}>Entendido</Button>
                            </Dialog.Actions>
                        </Dialog>

                        <Dialog visible={visibleRechazoMsg} onDismiss={() => setVerRechazoMsg(false)} theme={DarkTheme}>
                            <Dialog.Title style={styles.alertText}>Cuenta eliminada</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph style={styles.alertText}>Parece que esa cuenta fué eliminada definitivamente.</Paragraph>
                                <Paragraph style={styles.alertText}>Lamentablemente no puedes utilizar ese correo en una cuenta nueva. </Paragraph>
                                <Paragraph style={styles.alertText}>Por favor intenta con otro email.</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button theme={{ colors: {primary : colors.red}}} onPress={() => setVerRechazoMsg(false)} >Ok</Button>
                            </Dialog.Actions>
                        </Dialog>
                    </Portal>
                </Card>
            </ImageBackground>
        </Provider>
    )
}
export default CrearCliente;

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignContent:"center",
        justifyContent:"center",
    },
    tinyLogo: {
        height:60, 
        width:150,
        alignSelf: "center",
        //marginHorizontal: 120,
    },
    card: {
        padding: 10,
        marginHorizontal: 20,
        marginVertical: 20,
        alignContent:"center",
        backgroundColor: "#414141", //colors.backBox,
        borderRadius: 10,
        borderWidth: 3,
        borderEndColor: '#ffffff'
    },
    titleText: {
        color: colors.white,
        paddingTop: 10,
        paddingBottom: 30,
        fontSize: 20,
        marginHorizontal: 110
    },
    textInput: {
        backgroundColor: "#ffffff00",
        fontSize: 15,
        marginBottom: 10
    },
    btn: {
        marginTop: 20,
        color: colors.white,
        backgroundColor: colors.red,
        marginBottom: 20
    },
    alert: {
        backgroundColor: colors.background,
    },
    alertText: {
        color: colors.white,
    },
});