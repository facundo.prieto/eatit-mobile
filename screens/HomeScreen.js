import React from 'react';
import { RefreshControl, StyleSheet, ScrollView, ImageBackground } from 'react-native';

import Navbar from '../components/Navbar';
import ListaPedidosActivos from '../components/ListaPedidosActivos';
import ListaRestaurantes from '../components/ListaRestaurantes';

const HomeScreen = ({navigation}) => {
    console.log('HomeScreen')

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    
    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true)
        wait(100).then(() => setRefreshing(false));
    }, []);

    console.log('refreshing Home', refreshing)
    return(
        <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
            <ScrollView
                refreshControl={
                    <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                    />
                }
            >
                <Navbar navigation={navigation} ocultarGoBack={true}/>
                <ListaPedidosActivos navigation={navigation}/>
                <ListaRestaurantes navigation={navigation} recargarlista={refreshing}/>
            </ScrollView>
        </ImageBackground>
    )
}
export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10
        // justifyContent: 'center', 
        // alignItems: 'center'
    },
    text: {
        color: "white",
    },
});