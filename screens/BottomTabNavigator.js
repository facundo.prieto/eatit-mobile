import React, {useState, useEffect} from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { NavigationContainer } from '@react-navigation/native';
import { DarkTheme, Provider as PaperProvider } from 'react-native-paper';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { DeviceEventEmitter } from 'react-native';

import colors from '../assets/colors/colors';

import HomeScreen from './HomeScreen';
import CarritoScreen from './CarritoScreen';
import PerfilScreen from './PerfilScreen';
import Direcciones from '../components/Direcciones';
import ConfirmarPedido from '../components/ConfirmarPedido';
import PayPalWebView from '../components/PayPalWebView';
import PaymentOk from '../components/PaymentOk';
import HistoricoPedidos from '../components/HistoricoPedidos';
import ListaMenusRestaurante from '../components/ListaMenusRestaurante';
import Menu from '../components/Menu';
import DetallesPedido from '../components/DetallesPedido';
import DetallesPedidoHistorico from '../components/DetallesPedidoHistorico';
import IngresarReclamo from '../components/IngresarReclamo';
import DetallesReclamo from '../components/DetallesReclamo';
import HistoricoReclamos from '../components/HistoricoReclamos';
import RepetirPedido from '../components/RepetirPedido';

import { getCarrito } from '../components/auth/auth'

const HomeStack = createNativeStackNavigator();
const HomeNavigator = ({navigation}) => {

  //A la pantalla de menu le paso el navigation.navigate('CarritoScreen') para que vuelva al tab de carrito si quiere terminar la compra

  return (
      <HomeStack.Navigator initialRouteName="HomeScreen" screenOptions={{ headerShown: false }}>
        <HomeStack.Screen name="HomeScreen" component={HomeScreen} options={{headerShown: false}} />
        <HomeStack.Screen name="Direcciones" component={Direcciones} options={{headerShown: false}} />
        <HomeStack.Screen name="ListaMenusRestaurante" component={ListaMenusRestaurante} options={{headerShown: false}} />
        <HomeStack.Screen name="Menu" component={Menu} options={{headerShown: false}} />
        <HomeStack.Screen name="DetallesPedido" component={DetallesPedido} options={{headerShown: false}} />
        <HomeStack.Screen name="DetallesPedidoHistorico" component={DetallesPedidoHistorico} options={{headerShown: false}} />
        <HomeStack.Screen name="IngresarReclamo" component={IngresarReclamo} options={{headerShown: false}} />
        <HomeStack.Screen name="DetallesReclamo" component={DetallesReclamo} options={{headerShown: false}} />
        <HomeStack.Screen name="HistoricoPedidos" component={HistoricoPedidos} options={{headerShown: false}} />
        <HomeStack.Screen name="HistoricoReclamos" component={HistoricoReclamos} options={{headerShown: false}} />
        <HomeStack.Screen name="RepetirPedido" component={RepetirPedido} options={{headerShown: false}} />
      </HomeStack.Navigator>
  );
}


const CarritoStack = createNativeStackNavigator();
const CarritoNavigator = ({navigation, carrito}) => {
  
  //A la pantalla de carrito le paso el navigation.navigate('HomeScreen') para que vuelva al tab de home si quiere seguir comprando

  return (
      <CarritoStack.Navigator initialRouteName="CarritoScreen" screenOptions={{ headerShown: false }}>
        <CarritoStack.Screen name="CarritoScreen" options={{headerShown: false}} >
              {props => <CarritoScreen {...props} seguirComprando={() => navigation.navigate('HomeScreen')} carrito={carrito} />}
        </CarritoStack.Screen>
        <CarritoStack.Screen name="ConfirmarPedido" component={ConfirmarPedido} options={{headerShown: false}} />
        <CarritoStack.Screen name="PayPalWebView" component={PayPalWebView} options={{headerShown: false}} />
        <CarritoStack.Screen name="PaymentOk" component={PaymentOk} options={{headerShown: false}} />
        <CarritoStack.Screen name="DetallesPedido" component={DetallesPedido} options={{headerShown: false}} />
        <CarritoStack.Screen name="Direcciones" component={Direcciones} options={{headerShown: false}} />
        <CarritoStack.Screen name="IngresarReclamo" component={IngresarReclamo} options={{headerShown: false}} />
        <CarritoStack.Screen name="DetallesReclamo" component={DetallesReclamo} options={{headerShown: false}} />
        <CarritoStack.Screen name="RepetirPedido" component={RepetirPedido} options={{headerShown: false}} />
      </CarritoStack.Navigator>
  );
}


const PerfilStack = createNativeStackNavigator();
const PerfilNavigator = ({navigation, setLogueado}) => {

  //A la pantalla de perfil le paso el setLogueado para que vuelva al login despues de cerrar sesión

  return (
      <PerfilStack.Navigator initialRouteName="PerfilScreen" screenOptions={{ headerShown: false }} >
        <PerfilStack.Screen name="PerfilScreen" options={{headerShown: false}} >
              {props => <PerfilScreen {...props} setLogueado={setLogueado} />}
        </PerfilStack.Screen>
        <PerfilStack.Screen name="Direcciones" component={Direcciones} options={{headerShown: false}} />
        <PerfilStack.Screen name="HistoricoPedidos" component={HistoricoPedidos} options={{headerShown: false}} />
        <PerfilStack.Screen name="DetallesPedido" component={DetallesPedido} options={{headerShown: false}} />
        <PerfilStack.Screen name="DetallesPedidoHistorico" component={DetallesPedidoHistorico} options={{headerShown: false}} />
        <PerfilStack.Screen name="IngresarReclamo" component={IngresarReclamo} options={{headerShown: false}} />
        <PerfilStack.Screen name="DetallesReclamo" component={DetallesReclamo} options={{headerShown: false}} />
        <PerfilStack.Screen name="HistoricoReclamos" component={HistoricoReclamos} options={{headerShown: false}} />
        <PerfilStack.Screen name="RepetirPedido" component={RepetirPedido} options={{headerShown: false}} />
        <PerfilStack.Screen name="PayPalWebView" component={PayPalWebView} options={{headerShown: false}} />
        <PerfilStack.Screen name="PaymentOk" component={PaymentOk} options={{headerShown: false}} />
      </PerfilStack.Navigator>
  );
}

const Tab = createMaterialBottomTabNavigator();
const BottomTabNavigator = ({setLogueado}) => {
  
  const [cont, setContador] = useState(0);
  const [carrito, setCarrito] = useState([]);

  useEffect( () => {
    let eventListenerIncrementar = DeviceEventEmitter.addListener('incrementarBadge',ingrementarCont);
    let eventListenerVaciar = DeviceEventEmitter.addListener('vaciarBadge',vaciarCont);
    let eventListenerInicial = DeviceEventEmitter.addListener('cargaInicial',cargaInicial);
 
    return function() {
      eventListenerIncrementar.remove();
      eventListenerVaciar.remove();
      eventListenerInicial.remove();
    };
  }, []);

  const cargaInicial = async () => {
    let c = await getCarrito();
    // console.log('cargaInicial')
    // console.log(c)

    if(c != null){
      setContador(c.length)
      setCarrito(c)
    }
  }

  const ingrementarCont = (event) => {
    // console.log('ingrementarCont')
    // console.log(event.carrito)
    setContador(event.carrito.length)
    setCarrito(event.carrito) 
  }

  const vaciarCont = () => {
    //console.log('vaciarCont')
    setContador(0)
    setCarrito([]) 
  }

  return (
    <PaperProvider theme={DarkTheme}>
      <NavigationContainer>
        <Tab.Navigator 
          initialRouteName="HomeNavigator" 
          // initialRouteName="PerfilNavigator" 
          // activeColor={colors.background}
          inactiveColor={colors.backBox}
          barStyle={{ backgroundColor: colors.red, paddingTop: 2 }}>

          <Tab.Screen name="HomeNavigator" component={HomeNavigator}
            options={{
              tabBarLabel: false, //'Home',
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="home" color={color} size={26} />
              ),
            }}
          />

          <Tab.Screen name="CarritoNavigator" 
            options={{
              tabBarLabel: false, //'Perfil',
              tabBarBadge: cont ? cont : null,
              tabBarIcon: ({ color }) => (<MaterialCommunityIcons name="basket" color={color} size={26} />)
            }}>
              {props => <CarritoNavigator {...props} carrito={carrito} />}
          </Tab.Screen>
          
          <Tab.Screen name="PerfilNavigator" 
            options={{
              tabBarLabel: false, //'Perfil',
              tabBarIcon: ({ color }) => (<MaterialCommunityIcons name="account" color={color} size={26} />)
            }}>
              {props => <PerfilNavigator {...props} setLogueado={setLogueado} />}
          </Tab.Screen>
        </Tab.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
}
export default BottomTabNavigator;