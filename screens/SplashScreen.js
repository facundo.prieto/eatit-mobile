import React, {useState, useEffect } from 'react';
import { ActivityIndicator, ImageBackground, Image, StyleSheet, DeviceEventEmitter} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import messaging from '@react-native-firebase/messaging';
//import auth from '@react-native-firebase/auth';
//import firestore from '@react-native-firebase/firestore';

import axiosUsuario from '../components/axios/AxiosUsuario';

import LoginScreen from '../screens/LoginScreen';
import CrearCliente from '../screens/CrearCliente';
import RecuperarPasswordScreen from '../screens/RecuperarPasswordScreen';
import Direcciones from '../components/Direcciones';
import BottomTabNavigator from '../screens/BottomTabNavigator';

import { getSession } from '../components/auth/auth'

//Pantallas con las que interactúa un usuario no logueado
const NotLoguedStack = createNativeStackNavigator();
function NotLoguedNavigator(setLogueado) {
  return (
    <NavigationContainer >
      <NotLoguedStack.Navigator initialRouteName="LoginScreen" screenOptions={{ headerShown: false }} >
        <NotLoguedStack.Screen name="LoginScreen">
            {props => <LoginScreen {...props} options={{headerShown: false}} setLogueado={setLogueado} />}
        </NotLoguedStack.Screen>
        <NotLoguedStack.Screen name="RecuperarPasswordScreen" component={RecuperarPasswordScreen} options={{headerShown: false}} />
        <NotLoguedStack.Screen name="CrearCliente">  
            {props => <CrearCliente {...props} options={{headerShown: false}} setLogueado={setLogueado} />}
        </NotLoguedStack.Screen>
        <NotLoguedStack.Screen name="Direcciones">  
            {props => <Direcciones {...props} options={{headerShown: false}} setLogueado={setLogueado} />}
        </NotLoguedStack.Screen>
      </NotLoguedStack.Navigator>
    </NavigationContainer>
  );
}

async function saveTokenToDatabase(token) {
    // // Assume user is already signed in
    // const userId = auth().currentUser.uid;
    //
    // // Add the token to the users datastore
    // await firestore()
    //     .collection('users')
    //     .doc(userId)
    //     .update({
    //     tokens: firestore.FieldValue.arrayUnion(token),
    // });
    
    var session = await getSession();
    // console.log('session en SplashScreen -> ');
    // console.log(session);
    if(session !== null) {
        axiosUsuario
        .put("/push-token", { 
            email: session.email,
            pushToken: token,
        })
        .then(function (response) {
            // console.log("response", response);
            if (response.status == 200) {
                // console.log("status 200", response.data);
            }
        })
        .catch(function (error) {
            if (error.response.status == 400) {
                console.log("error.response.data", error.response.data);
            } else {
                console.log("error.message", error.message);
            }
        });
    }
}

const SplashScreen = () => {
    // console.log("SplashScreen")

    useEffect(() => {
        // Get the device token
        // console.log("useEffect")

        // messaging()
        // .getToken()
        // .then(token => {
            
        //     console.log("messaging")
        //     console.log(token)
        //     return saveTokenToDatabase(token);
        // });

        function foregroundSubscriber() {
          messaging().onMessage(async (remoteMessage) => {
            console.log("Push notification recibida", remoteMessage);
          });
        }
    
        function backgroundSubcriber() {
          messaging().setBackgroundMessageHandler(async (remoteMessage) => {
            console.log("Push notification recibida background", remoteMessage);
          });
        }
    

        // Listen to whether the token changes
        return () => {
            messaging().onTokenRefresh(token => {
                saveTokenToDatabase(token);
            })
            backgroundSubcriber();
            foregroundSubscriber();
        };
  
    }, []);

    //Mostrar pantalla de carga
    const [loading, setLoadingVisible] = useState(true);
    const setLoading = (load) => setLoadingVisible(load);
    
    //Mostrar pantallas de usuario logueado o no logueado
    const [logueado, setLogueadoScreen] = useState(false);
    const setLogueado = (logued) => {
        // console.log("setLogueado")
        // console.log(logued)
        if(logued){
            messaging()
            .getToken()
            .then(token => {
                
                // console.log("logued messaging")
                // console.log(token)
                return saveTokenToDatabase(token);
            });
        }
        setLogueadoScreen(logued);
    }

    setTimeout(() => {
        readData();
        DeviceEventEmitter.emit('cargaInicial');
    }, 1000)

    //Consulta al almacenamiento local para ver si ya se inició sesión en el dispositivo
    const readData = async () => {
        var session = await getSession();
        // console.log('session en SplashScreen -> ');
        // console.log(session);
        if(session !== null) {
            setLoading(false);
            setLogueado(true);
        } else {
            setLoading(false);
            setLogueado(false);
        }
    };

    //Muestro el grupo de pantallas que corresponda
    if(loading){
        return(
            <ImageBackground style={{flex: 1}} source={require('../assets/images/fondo.jpg')}>
                <Image style={styles.image} source={require('../assets/images/splashscreen.png')}/>
                <ActivityIndicator style={styles.loading} animating={true} color="#ff4343" size="large" />
            </ImageBackground>
        )
    }else if(logueado){
        return(
            <BottomTabNavigator setLogueado={setLogueado}/>
        )
    }else{
        return(
            <NotLoguedNavigator setLogueado={setLogueado}/>
        )
    }
};
export default SplashScreen;

const styles = StyleSheet.create({
    loading: {
        marginVertical: 10
    },
    image: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'contain'
    },
})