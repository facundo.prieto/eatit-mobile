import React, {useState, useEffect} from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Image, StyleSheet, Alert } from 'react-native';
import { Dialog, Portal, Button, DarkTheme, Paragraph } from 'react-native-paper';
import { getSession, endSession } from '../components/auth/auth';
import auth from '@react-native-firebase/auth';
// import { GoogleSignin } from '@react-native-google-signin/google-signin';
import colors from '../assets/colors/colors';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import axiosCliente from "../components/axios/AxiosCliente";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const PerfilScreen = ({navigation, setLogueado}) => {

    const [visibleMsgSucces, setVisibleSucces] = useState(false);
    const [visibleMsgError, setVisibleError] = useState(false);
    const [errorTexto, setErrorTexto] = useState('');
    const [calificacion, setCalificacion] = useState(0);
    const [nombre, setNombre] = useState('');
    const [email, setEmail] = useState('');
    const [direccion, setDireccion] = useState('');
    const [seguro, setSeguro] = useState(false);
    const [visibleEliminar, setVisibleEliminar] = useState(false);
    const showDialog = () => setVisibleEliminar(true);
    const hideDialog = () => setVisibleEliminar(false);

    useEffect(() => {
        getDatos();
        return () =>{
        }
    }, []);

    const getDatos = async () => {
        var session = await getSession();
        // console.log(`session: `, session)

        // CALIFICACION
        if(session != null){
            // console.log(`Cargo calificacion`)
            axiosCliente
            .get(`/calificacion/visualizar/${session.email}`)
            .then(function (response) {
                if(response.status == 200){
                    setCalificacion(response.data);
                } else {
                    setErrorTexto(response.data);
                    setVisibleError(true);
                }
            })
            .catch(function (error) {
                if (error.response.status == 400) {
                    // Request made and server responded
                    setErrorTexto(error.response.data);
                    setVisibleError(true);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    setErrorTexto(error.message);
                    setVisibleError(true);
                }
            });
            // console.log(`Calificacion cargada: ${calificacion}`)
        }

        // NOMBRE
        if(session != null && session.nombre != null){
            setNombre(session.nombre);
        }

        // CORREO
        if(session != null && session.email != null){
            setEmail(session.email);
        }
        
        // DIRECCION
        if(session != null && session.direcciones != null && session.direcciones.length > 0){
            setDireccion(session.direcciones[0].calle + " " + session.direcciones[0].numero);
        }
    };

    const cerrarSesion = async () => {
        endSession();
        auth().signOut()
        setLogueado(false);
    }

    const eliminarCuenta = async (seguro) => {
        var session = await getSession();
        if(seguro){
            // console.log('eliminando cuenta...')
            axiosCliente
            .delete(`/${session.email}/`, {
                headers: { Authorization: `${session.token}` },
            })
            .then(function (response) {
                // console.log('cuenta eliminada!')
                // console.log(response)
                // console.log(`response.status = ${response.status}`)
                if(response.status == 200){
                    // console.log('cerrando sesion...')
                    endSession();
                    auth().signOut()
                    setLogueado(false);
                    //GoogleSignin.revokeAccess();
                } else {
                    setErrorTexto(response.data);
                    setVisibleError(true);
                }
            })
            .catch(function (error) {
                // console.log('seguro.catch')
                if (error.response.status == 400) {
                    // Request made and server responded
                    setErrorTexto(error.response.data);
                    setVisibleError(true);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    setErrorTexto(error.message);
                    setVisibleError(true);
                }
            });
        }
    }

    const estrellasPopup = (calif) => {
        let cali = 0;
        if(calificacion != 0){
            cali = calificacion;
        }else if(calif && calif != 0 && calif!=calificacion){
            setCalificacion(calif)
            cali = calif
        }

        var lis = [];

        if(cali==0){
            lis.push(
                <Text style={styles.textCali}>USUARIO NUEVO</Text>
            );
        }else{
            let index = 0;
            for (let i = 0; i < cali; i++){
                index += 1
                lis.push(
                    <MaterialCommunityIcons key={index} style={{marginHorizontal: 2}} name="star" size={20} color={colors.red}/> 
                );
            }
            for (let j = 0; j < 5-cali; j++){
                index += 1
                lis.push(
                    <MaterialCommunityIcons key={index} style={{marginHorizontal: 2}} name="star" size={20} color={colors.backBox}/> 
                );
            }
        }

        return lis;
    }
     
    const mostrarAlerta = (msg) => {
        setErrorTexto(msg)
        setVisibleError(true)
    }

    return(
        <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
            <View>
                <View style={{height:90, marginVertical: 20}}>
                    <Image style={styles.image} source={require('../assets/images/globoLogo.png')}/>
                </View>

                {calificacion && calificacion != 0 ?
                    <View>
                        <View style={styles.alertestrellas}>
                            <Text style={{color:"white", fontSize:16}}>Tu calificación:   </Text>
                            {estrellasPopup(calificacion)}
                        </View>
                    </View>
                : null}
                
                <TouchableOpacity style={styles.directionContainer}>
                    <MaterialIcon name="person" size={22} color={colors.white}/>
                    <Text style={styles.directionText}>{nombre}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.directionContainer}>
                    <MaterialIcon name="alternate-email" size={22} color={colors.white}/>
                    <Text style={styles.directionText}>{email}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.directionContainer} onPress={() => navigation.push('Direcciones')}>
                    <MaterialIcon name="home" size={22} color={colors.white}/>
                    <Text style={styles.directionText}>{direccion}</Text>
                </TouchableOpacity>
                {/* <View style={styles.directionContainer}>
                    <View style={styles.alertestrellas}>
                        <MaterialCommunityIcons name="star" size={20} color={colors.white}/> 
                        <Text style={styles.directionText}>Tu calificación:</Text>
                    </View>
                    <View style={styles.alertestrellas}>
                        {estrellasPopup(calificacion)}
                    </View>
                </View> */}
                <TouchableOpacity style={styles.directionContainer} onPress={() => navigation.push('HistoricoPedidos')}>
                    <MaterialIcon name="shopping-cart" size={22} color={colors.white}/>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={styles.directionText}>MIS PEDIDOS</Text>
                        <MaterialIcon name="chevron-right" size={22} color={colors.white}/>
                    </View>
                </TouchableOpacity>

                <Button mode="contained" style={{...styles.btn, marginTop: 20}} onPress={cerrarSesion} >
                    <Text style={styles.text}>Cerrar sesion</Text>
                </Button>
                <Button mode="contained" style={styles.btn} onPress={showDialog} >
                    <Text style={styles.text}>Eliminar cuenta</Text>
                </Button>
            </View>
            
            <Portal>
                <Dialog visible={visibleEliminar} onDismiss={hideDialog} theme={DarkTheme}>
                    <Dialog.Content>
                    <Paragraph style={styles.text}>¿Estas seguro que deseas eliminar tu cuenta?</Paragraph>
                    <Paragraph style={styles.text}>Se eliminará de forma permanente</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button theme={{ colors: {primary : colors.red}}} onPress={hideDialog}>Cancelar</Button>
                        <Button theme={{ colors: {primary : colors.red}}} onPress={() => eliminarCuenta(true)}>   Confirmar</Button>
                    </Dialog.Actions>
                </Dialog>
                <Dialog visible={visibleMsgError} onDismiss={() => setVisibleError(false)} theme={DarkTheme}>
                    <Dialog.Content>
                    <Paragraph style={styles.text}>{errorTexto}</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button theme={{ colors: {primary : colors.red}}} onPress={() => setVisibleError(false)}>Ok</Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>

        </ImageBackground>
    )
}
export default PerfilScreen;

const styles = StyleSheet.create({
    text: {
      color: "white",
      fontSize: 12
    },
    btn: {
        marginVertical: 10,
        marginHorizontal: 70,
        color: colors.white,
        textDecorationColor: colors.white,
        backgroundColor: colors.red,
    },
    container: {
        flex: 1,
        padding: 10,
        justifyContent: 'center',
    },
    card: {

        alignContent:"center",
        backgroundColor: "#414141",
        borderRadius: 10,
        borderWidth: 3,
        borderEndColor: '#ffffff'
    },
    viewButton: {
        height: 50,
        padding: 50
    },
    directionContainer: {
        height: 40,
        backgroundColor: "#0000004a",
        alignItems: 'flex-end',
        justifyContent: 'space-between', 
        paddingHorizontal: 10,
        paddingTop: 10,
        paddingBottom: 10,
        marginHorizontal: 20,
        marginVertical: 5,
        flexDirection: 'row',
        borderRadius: 5
    },
    directionText: {
        color: colors.white, 
        fontSize:16, 
        paddingHorizontal: 5
    },
    textTitulo: {
        color: "white",
        fontSize: 18,
        paddingHorizontal: 10,
        fontWeight: 'bold'
    },
    title: {
      fontSize: 24,
      margin: 10
    },
    alert: {
        backgroundColor: colors.background,
    },
    alertText: {
        color: colors.white,
    },
    image: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'contain'
    },
    alertestrellas: {
        marginVertical: 10,
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'row',
    },
    textCali: {
        color: "green",
        //fontSize: 14,
    },
});