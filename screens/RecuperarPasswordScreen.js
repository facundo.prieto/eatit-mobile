import React, {useState} from 'react';
import { StyleSheet, Image, ImageBackground } from 'react-native';
import { Card, Button, Text, Paragraph, Dialog, Portal, Provider, TextInput } from 'react-native-paper';

import colors from '../assets/colors/colors';
import axiosUsuario from "../components/axios/AxiosUsuario";

const RecuperarPasswordScreen = ({navigation}) => {
    const [visibleMsgSucces, setVisibleSucces] = useState(false);
    const [visibleMsgError, setVisibleError] = useState(false);
    const [errorTxt, setErrorTexto] = useState("");
    const [succesTxt, setSuccesTexto] = useState('');
    const [correo, setCorreo] = useState('');

    const onChangeCorreo = correo => setCorreo(correo);

    const recuperarPassword = () => {
        
        let token = "";
        if(getSession() != null && getSession().token != null){
          token = getSession().token;
        }

        //Mando los datos del state al recuperarClave del ClienteController
        axiosUsuario
        .put(`/recuperarClave/${correo}`, {},
        { headers: { 'Authorization': `${token}` }}
        )
        .then(function (response) {
            // console.log(response.status);
            if(response.status == 200){
                setSuccesTexto("Por favor verifica tu correo para encontrar la nueva contraseña asignada a tu cuenta.");
                setVisibleSucces(true);
            } else {
                setErrorTexto(response.data);
                setVisibleError(true);
            }
        })
        .catch(function (error) {
            // console.log(error.response.status);
            // console.log(error.response.data);
            // console.log(error.message);
            if (error.response.status == 400) {
                // Request made and server responded
                setErrorTexto(error.response.data);
                setVisibleError(true);
            } else {
                // // Something happened in setting up the request that triggered an Error
                setErrorTexto(error.message);
                setVisibleError(true);
            }
        });
    }

    const hideDialog = () => {
        setSuccesTexto("");
        setVisibleSucces(false);
        navigation.navigate('LoginScreen')
    }

    const hideDialogError = () => {
        setErrorTexto("");
        setVisibleError(false);
    }

    return(
        <Provider>
            <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
                <Image style={styles.tinyLogo} source={require('../assets/images/logoLetras.png')}/>
                <Card style={styles.card}>
                    <Text style={styles.titleText}>Recuperar Contraseña</Text>
                    <Text style={styles.descriptionText}>Ingresa tu dirección de e-mail para que te enviemos la nueva contraseña</Text>
                    
                    <TextInput 
                        label="Ingresa tu e-mail" 
                        mode="flat" 
                        value={correo} 
                        onChangeText={onChangeCorreo} 
                        outlineColor={colors.white}
                        style={styles.textInput}
                        dense={true}
                        theme={{ colors: { text: colors.white, primary: colors.white} }}
                    />

                    <Button mode="contained" style={styles.btn} onPress={recuperarPassword} disabled={correo==''}>Continuar</Button>
                    <Portal>
                        <Dialog visible={visibleMsgSucces} onDismiss={hideDialog} style={styles.alert}>
                            <Dialog.Title style={styles.alertText}>Se reinició tu contraseña</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph style={styles.alertText}>{succesTxt}</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                            <Button mode="contained" style={styles.btn} onPress={hideDialog}>Entendido</Button>
                            </Dialog.Actions>
                        </Dialog>
                        <Dialog visible={visibleMsgError} onDismiss={hideDialogError} style={styles.alert}>
                            <Dialog.Title style={styles.alertText}>Ha ocurrido un error:</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph style={styles.alertText}>{errorTxt}</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                            <Button mode="contained" style={styles.btn} onPress={hideDialogError}>Entendido</Button>
                            </Dialog.Actions>
                        </Dialog>
                    </Portal>
                </Card>
            </ImageBackground>
        </Provider>
    )
}
export default RecuperarPasswordScreen;

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignContent:"center",
        justifyContent:"center",
    },
    tinyLogo: {
        height:60, 
        width:150,
        alignSelf: "center",
    },
    card: {
        padding: 10,
        marginHorizontal: 50,
        marginVertical: 30,
        alignContent:"center",
        backgroundColor: "#414141",
        borderRadius: 10,
        borderWidth: 3,
        borderEndColor: '#ffffff'
    },
    titleText: {
        color: colors.white,
        paddingTop: 10,
        paddingBottom: 30,
        fontSize: 20,
        marginHorizontal: 30
    },
    descriptionText: {
        color: colors.white,
        paddingBottom: 20,
        fontSize: 15
    },
    textInput: {
        backgroundColor: "#ffffff00"
    },
    btn: {
        marginTop: 20,
        color: colors.white,
        backgroundColor: colors.red,
        marginBottom: 20
    },
    alert: {
        backgroundColor: colors.background,
    },
    alertText: {
        color: colors.white,
    },
});
