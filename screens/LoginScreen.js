import React, {Component} from 'react';
import { ImageBackground, StyleSheet, Text, View, Image, Alert} from 'react-native';
import auth, { getAuth, getRedirectResult, GoogleAuthProvider } from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../assets/colors/colors';
import { Card, Button, TextInput, Provider, Paragraph, Dialog, Portal, DarkTheme, LightTheme } from 'react-native-paper';
import { setSession } from "../components/auth/auth";
import axiosUsuario from '../components/axios/AxiosUsuario';
import axiosCliente from "../components/axios/AxiosCliente";

GoogleSignin.configure({
    webClientId: '148814033726-ini7bap35gc4qt5sknl5729rt8706evb.apps.googleusercontent.com',
});

Icon.loadFont();


async function onauthstateChanged(params) {
    const { user, accessToken, navigateCrearCliente, navigateLogueado, errorCuentaEliminada } = params;
    
    // console.log("user11 ",user)
    // console.log("accessToken ",accessToken)

    if(user && user.email){
        axiosCliente.get(
            `/buscar/${user.email}`
        )
        .then(function (response) {
            // console.log("response.data ",response.data);
            if (response.status == 200) {
                if(response.data.estado == 'ELIMINADO'){
                    errorCuentaEliminada();
                }else{
                    axiosUsuario
                    .post("/login/google", {
                        token: accessToken,
                    })
                    .then(function (response) {
                    if (response.status == 200) {
                        setSession({
                            nombre: response.data.nombre,
                            email: response.data.email,
                            token: response.data.token,
                            direcciones: response.data.direcciones,
                            rol: response.data.rol
                        });
                        navigateLogueado();
                        // console.log("login response.data ",response.data);
                    } else {
                        // console.log("login response.message ",response.message);
                    }
                    })
                    .catch(function (error) {
                        if (error.response.status == 400) {
                            // console.log("login error.response.status ",error.response.status);
                        } else {
                            // console.log("login error.message ",error.message);
                        }
                    });
                }
            } else {
                // console.log("buscar response.message ",response.message);
                navigateCrearCliente(user, accessToken);
            }
        })
        .catch(function (error) {
            if (error.response.status == 400) {
                // console.log("buscar error.response.status ",error.response.status);
            } else {
                // console.log("buscar error.message ",error.message);
            }
            navigateCrearCliente(user, accessToken)
        });

    }
}

export class LoginScreen extends Component {
    constructor(){
        super();
        this.state = {
            email: "",
            contrasena: "",
            alertaError: false,
            msgErr:"",
            accessToken:"",
            visibleRechazoMsg: false
        };
    }

    logoff = () => {
        auth()
        .signOut()
        .then(() => console.log('User signed out!'));
    }

    navigateCrearCliente = (user, accessToken) => {
        this.props.navigation.navigate('CrearCliente', {esGoogleUser: true, user:user, googleToken:accessToken});
    }

    navigateLogueado = () => {
        this.props.setLogueado.setLogueado(true);
    }

    errorCuentaEliminada = () => {
        this.setState({ visibleRechazoMsg: true });
        this.logoff();
    }
    
    componentDidMount() {
        auth().onAuthStateChanged((user) =>
            onauthstateChanged({
                user:user, 
                accessToken:this.state.accessToken,
                navigateCrearCliente:this.navigateCrearCliente, 
                navigateLogueado:this.navigateLogueado, 
                errorCuentaEliminada: this.errorCuentaEliminada
            })
        );
    }

    onGoogleButtonPress = async () => {

        // console.log("onGoogleButtonPress ")

        // Get the users ID token
        const { idToken } = await GoogleSignin.signIn();
        // console.log("onGoogleButtonPress idToken", idToken)

        // Get the users access Token
        const { accessToken } = await GoogleSignin.getTokens();
        this.setState({ accessToken: accessToken });
        // console.log("onGoogleButtonPress accessToken", accessToken)
        
        // Create a Google credential with the token
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
        
        // Sign-in the user with the credential
        return auth().signInWithCredential(googleCredential);
    }

    iniciarSesion = async () => {
        if(this.state.email==="" || (this.state.contrasena==="")){
            this.setState({ alertaError: true, msgErr:"Debe completar todos los datos solicitados." });
            Alert.alert("Debe completar todos los datos solicitados.")
        }else{
            // console.log('llamado al axios');
            var t = this;
            axiosUsuario
            .post(
                "/login",
                {
                    email: this.state.email,
                    password: this.state.contrasena,
                }
            )
            .then(function (response) {
                // console.log(response);
                if (response.status == 200) {
                    if (response.data.rol == 'CLIENTE') {
                        setSession({
                            nombre: response.data.nombre,
                            email: response.data.email,
                            token: response.data.token,
                            direcciones: response.data.direcciones,
                            rol: response.data.rol
                        });
                        t.props.setLogueado.setLogueado(true);
                    } else {
                        t.setState({
                            alertaError: true,
                            msgErr: "El correo no corresponde a un cliente. Los restaurantes y administradores deben acceder desde la página web.",
                        });
                    }
                } else {
                    t.setState({
                        alertaError: true,
                        msgErr: response.data,
                    });
                }
            })
            .catch(function (error) {
                // console.log(error.response);
                if (error.response) {
                    // Request made and server responded
                    t.setState({
                        alertaError: true,
                        msgErr: error.response.data,
                    });
                    Alert.alert(error.response.data)
                } else {
                    // Something happened in setting up the request that triggered an Error
                    t.setState({
                        alertaError: true,
                        msgErr: error.message,
                    });
                    Alert.alert(error.message)
                }
            });
        }
    }

    handleClose = (event, reason) => {
        if (reason === "clickaway") {
          return;
        }
        this.setState({ alertaError: false, msgErr:"" });
    };
    
    render() {
        return (
            <Provider>
                <ImageBackground resizeMode="cover" style={styles.image} source={require('../assets/images/fondo.jpg')}>
                    <View style={styles.container}>
                        {/* esto muestra "Login" o "Bienvenido @mail" si hay alguien logueado */}
                        {/* <LoginApp/> */}
                        {/* <View style={{paddingTop:10, padding:50, alignContent:"center"}}> */}
                            {/* ejemplo de como se importan los iconos */}
                            {/* <Icon name="ios-person" size={30} color="#4F8EF7" style={{textAlign:"center", margin: 10}}/> */}
                        <Image
                            resizeMethod="scale"
                            style={{height: 60, width: 150, alignSelf: "center"}}
                            source={require('../assets/images/logoLetras.png')}
                        />
                            {/* <Card style={styles.card}>
                            <Image
                                resizeMethod="scale"
                                style={{height: 60, width: 150, textAlign:"center", marginHorizontal: 40}}
                                source={require('../assets/images/logoLetras.png')}
                            />
                            </Card> */}
                            {/* <View style={{flexDirection: 'row', justifyContent:"center", flex:1}}>
                                <Image style={{ resizeMode: 'contain', flex: 1, aspectRatio: 1}} source={require('../assets/images/logoLetras.png')} />
                            </View> */}
                        <Card style={styles.card}>
                            <TextInput
                                id="email"
                                label="Email"
                                mode="flat"
                                onChangeText={text => this.setState({ email: text })}
                                outlineColor={colors.white}
                                style={styles.input}
                                dense={true}
                                theme={{ colors: { text: colors.white, 
                                                    outlineColor: colors.white,
                                                    primary: colors.white, } }}
                            />
                            <TextInput
                                label="Password"
                                mode="flat"
                                secureTextEntry={true}
                                onChangeText={text => this.setState({ contrasena: text })}
                                outlineColor={colors.white}
                                style={styles.input}
                                dense={true}
                                theme={{ colors: { text: colors.white, 
                                                    outlineColor: colors.white,
                                                    primary: colors.white, } }}
                            />
                            
                            <Button
                                style={styles.btn}
                                mode="contained"
                                onPress={() => this.iniciarSesion().then(() => console.log('Signed in!'))}
                            >
                                <Text style={styles.textButton}>Iniciar Sesión</Text>
                            </Button>

                            <Text 
                                style={styles.textUnderline} 
                                onPress={() => this.props.navigation.navigate('RecuperarPasswordScreen')}
                            >
                                ¿Olvidaste tu contraseña?
                            </Text>
                        </Card>
                            
                        {/* </View> */}
                        {/* <View style={{paddingTop:10, padding:50, alignContent:"center"}}> */}
                        <Card style={styles.card}>
                            <Text style={styles.text}>¿Aún no te registraste?</Text>

                            <Button
                                style={styles.btn}
                                mode="contained"
                                onPress={() => this.props.navigation.navigate('CrearCliente')}
                                icon={require('../assets/images/globoLogo.png')}
                                theme={LightTheme}
                            >
                                <Text style={styles.textButton}>REGISTRATE AQUÍ</Text>
                            </Button>

                            {/* <Button title="Registrate Aquí" onPress={() => this.props.navigation.navigate('CrearCliente')} /> */}
                            
                            <Text style={styles.text}>o</Text>

                            <Button
                                style={styles.btn}
                                mode="contained"
                                onPress={() => this.onGoogleButtonPress().then(() => console.log('Signed in with Google!'))}
                                icon={{ source: "google", direction: 'auto', size:30, width:51 }}
                                theme={LightTheme}
                            >
                                <Text style={styles.textButton}>Inicia con Google</Text>
                            </Button>

                            {/* <Button
                                title="Inicia con Google"
                                onPress={() => this.onGoogleButtonPress().then(() => console.log('Signed in with Google!'))}
                            /> */}
                            {/* <Button title="Cerrar sesión" onPress={() => this.logoff()} /> */}
                            {/* <Text>---------</Text>
                            <Button title="Recuperar contraseña" onPress={() => this.props.navigation.navigate('RecuperarPasswordScreen')} />
                            <Text>---------</Text> */}
                            {/* <Text>---------</Text> */}
                        </Card>
                        {/* </View> */}
                        {/* <Button onPress={() => this.props.navigation.navigate('SplashScreen')}>
                                Volver
                        </Button> */}

                        <Portal>
                            <Dialog visible={this.state.visibleRechazoMsg} onDismiss={() => this.setState({ visibleRechazoMsg: false })} theme={DarkTheme}>
                                <Dialog.Title style={styles.text}>Cuenta eliminada</Dialog.Title>
                                <Dialog.Content>
                                    <Paragraph style={styles.text}>Parece que esa cuenta fué eliminada definitivamente.</Paragraph>
                                    <Paragraph style={styles.text}>Lamentablemente no puedes utilizar ese correo en una cuenta nueva. </Paragraph>
                                    <Paragraph style={styles.text}>Por favor intenta con otro email.</Paragraph>
                                </Dialog.Content>
                                <Dialog.Actions>
                                    <Button theme={{ colors: {primary : colors.red}}} onPress={() => this.setState({ alertaError: false })} >Ok</Button>
                                </Dialog.Actions>
                            </Dialog>
                            <Dialog visible={this.state.alertaError} onDismiss={() => this.setState({ alertaError: false })} theme={DarkTheme}>
                                <Dialog.Title style={styles.text}>Ha ocurrido un error</Dialog.Title>
                                <Dialog.Content>
                                    <Paragraph style={styles.text}>{this.state.msgErr}</Paragraph>
                                </Dialog.Content>
                                <Dialog.Actions>
                                    <Button theme={{ colors: {primary : colors.red}}} onPress={() => this.setState({ alertaError: false })} >Ok</Button>
                                </Dialog.Actions>
                            </Dialog>
                        </Portal>
                    </View>
                </ImageBackground>
            </Provider>
        );
    }
}

    export default LoginScreen;

    const styles = StyleSheet.create({
        container: {
            flex: 1,
            alignContent:"center",
            justifyContent:"center",
            //backgroundColor: colors.white,
        },
        image: {
            flex: 1,
            alignContent:"center",
            justifyContent: "center",
        },
        text: {
            color: "white",
            //   fontSize: 42,
            //   lineHeight: 84,
            //   fontWeight: "bold",
            textAlign: "center",
            //   backgroundColor: "#000000c0"
        },
        textUnderline: {
            color: "#FF0000",
            //   fontSize: 42,
            //   lineHeight: 84,
            //   fontWeight: "bold",
            textDecorationLine: 'underline',
            textAlign: "center",
            //   backgroundColor: "#000000c0"
        },
        textButton: {
            color: "white",
            fontSize: 14,
        },
        button: {
            color: 'red',
            marginTop: 10,
            backgroundColor: "#FF4242",
            height: 40,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 5,
            marginTop: 10,
            marginBottom: 10
        },
        btn: {
            marginVertical: 12,
            color: colors.white,
            backgroundColor: colors.red,
            textDecorationColor: colors.white,
        },
        card: {
            padding: 10,
            marginHorizontal: 50,
            marginVertical: 20,
            alignContent:"center",
            backgroundColor: "#414141",
            borderRadius: 10,
            borderWidth: 3,
            borderEndColor: '#ffffff'
        },
        input: {
            backgroundColor: "#ffffff00",
            marginVertical: 5
        }
    });