const colors = {
  background: '#414141',//'#343434',
  // textDark: '#313234',
  // primary: '#F5CA48',
  // secondary: '#F26C68',
  // textLight: '#ffffff',
  // price: '#E4723C',
  white: '#FFF',
  black: '#000',
  red: '#ff4343',
  backBox: '#0000004a',
  backRed: '#ff43431A',
};

export default colors;
