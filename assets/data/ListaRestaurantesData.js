const ListaRestaurantesData = [
    {
        id: 1,
        nombre: "McDonalds",
        email: "McDonalds@gmail.com",
        calificacion: 4.1,
        costoEnvio: 60,
        pedidos: 13,
        estado: "ACTIVO",
        imagen: "https://d25dk4h1q4vl9b.cloudfront.net/bundles/front/media/images/header/mcdonalds-logo.png",
        categorias: [
          {
            descripcion: "Acompañamientos"
          },
          {
            descripcion: "Hamburguesas"
          },
          {
            descripcion: "Bebidas"
          }
        ]
    },
    {
        id: 2,
        nombre: "BurguerKing",
        email: "BurguerKing@gmail.com",
        calificacion: 3.1,
        costoEnvio: 45,
        pedidos: 60,
        estado: "BLOQUEADO",
        imagen: "https://d25dk4h1q4vl9b.cloudfront.net/bundles/front/media/images/header/mcdonalds-logo.png",
        categorias: [
          {
            descripcion: "Acompañamientos"
          },
          {
            descripcion: "Hamburguesas"
          },
          {
            descripcion: "Bebidas"
          }
        ]
    },
    {
        id: 3,
        nombre: "SushiGo",
        email: "SushiGo@gmail.com",
        calificacion: 3.6,
        costoEnvio: 30,
        pedidos: 20,
        estado: "INACTIVO",
        imagen: "https://d25dk4h1q4vl9b.cloudfront.net/bundles/front/media/images/header/mcdonalds-logo.png",
        categorias: [
          {
            descripcion: "Comida asiatica"
          },
        ]
    },
    {
        id: 4,
        nombre: "RoosterHouse",
        email: "RoosterHouse@gmail.com",
        calificacion: 5,
        costoEnvio: 60,
        pedidos: 2,
        estado: "ACTIVO",
        imagen: "https://d25dk4h1q4vl9b.cloudfront.net/bundles/front/media/images/header/mcdonalds-logo.png",
        categorias: [
          {
            descripcion: "Acompañamientos"
          },
          {
            descripcion: "Pollo"
          },
        ]
    },
    {
        id: 5,
        nombre: "PanchoVa",
        email: "PanchoVa@gmail.com",
        calificacion: 4.6,
        costoEnvio: 20,
        pedidos: 4,
        estado: "ACTIVO",
        imagen: "https://d25dk4h1q4vl9b.cloudfront.net/bundles/front/media/images/header/mcdonalds-logo.png",
        categorias: [
          {
            descripcion: "Panchos"
          },
        ]
    },
    {
        id: 6,
        nombre: "Paladar venezolano",
        email: "Paladar@gmail.com",
        calificacion: 4,
        costoEnvio: 0,
        pedidos: 33,
        estado: "ACTIVO",
        imagen: "https://d25dk4h1q4vl9b.cloudfront.net/bundles/front/media/images/header/mcdonalds-logo.png",
        categorias: [
          {
            descripcion: "Comida venezolana"
          },
        ]
    },
    {
        id: 7,
        nombre: "Peperonto Pizzeria",
        email: "Peperonto@gmail.com",
        calificacion: 2,
        costoEnvio: 100,
        pedidos: 112,
        estado: "ACTIVO",
        imagen: "https://d25dk4h1q4vl9b.cloudfront.net/bundles/front/media/images/header/mcdonalds-logo.png",
        categorias: [
          {
            descripcion: "Pizza"
          },
          {
            descripcion: "Comida italiana"
          },
        ]
    },
    {
        id: 8,
        nombre: "Cacho Burguer",
        email: "CachoBurguer@gmail.com",
        calificacion: 5,
        costoEnvio: 200,
        pedidos: 112,
        estado: "ACTIVO",
        imagen: "https://d25dk4h1q4vl9b.cloudfront.net/bundles/front/media/images/header/mcdonalds-logo.png",
        categorias: [
          {
            descripcion: "Acompañamientos"
          },
          {
            descripcion: "Hamburguesas"
          },
          {
            descripcion: "Bebidas"
          }
        ]
    },
    {
        id: 9,
        nombre: "La Tortugita",
        email: "Tortugita@gmail.com",
        calificacion: 2,
        costoEnvio: 30,
        pedidos: 112,
        estado: "ACTIVO",
        imagen: "https://d25dk4h1q4vl9b.cloudfront.net/bundles/front/media/images/header/mcdonalds-logo.png",
        categorias: [
          {
            descripcion: "Pizza"
          },
          {
            descripcion: "Comida italiana"
          },
          {
            descripcion: "Bebidas"
          }
        ]
    },
]

export default ListaRestaurantesData;