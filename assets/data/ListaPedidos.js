const ListaPedidos = [
    {
        id: 1,
        nombre: "McDonalds",
        email: "McDonalds@gmail.com",
        calificacion: 4.1,
        costoEnvio: 60,
        pedidos: 13,
        estado: "ACTIVO",
    },
    {
        id: 2,
        nombre: "BurguerKing",
        email: "BurguerKing@gmail.com",
        calificacion: 3.1,
        costoEnvio: 45,
        pedidos: 60,
        estado: "BLOQUEADO",
    },
    {
        id: 3,
        nombre: "SushiGo",
        email: "SushiGo@gmail.com",
        calificacion: 3.6,
        costoEnvio: 30,
        pedidos: 20,
        estado: "INACTIVO",
    },
]

export default ListaPedidos;