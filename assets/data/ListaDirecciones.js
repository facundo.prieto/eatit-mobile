const ListaDirecciones = [
    {
        id: 1,
        calle: "Av. Italia",
        numero: "3796",
        esquina: "Comercio",
        barrio: "Malvin",
    },
    {
        id: 2,
        calle: "Av. 18 de Julio",
        numero: "1122",
        esquina: "Yi",
        barrio: "Centro",
    },
    {
        id: 3,
        calle: "Gral. Flores",
        numero: "3344",
        esquina: "Garibaldi",
        barrio: "Goes",
    },
    {
        id: 4,
        calle: "Av. 8 de Octubre",
        numero: "1122",
        esquina: "Av. Gral. Garibaldi",
        barrio: "La Blanqueada",
    },
    {
        id: 5,
        calle: "Av. Gral. Rivera",
        numero: "6663",
        esquina: "Soca",
        barrio: "Pocitos",
    },
    {
        id: 6,
        calle: "Ejido",
        numero: "1122",
        esquina: "Paysandú",
        barrio: "Centro",
    },
    {
        id: 7,
        calle: "Cno. Carrasco",
        numero: "3344",
        esquina: "Veracierto",
        barrio: "Malvin Norte",
    },
]

export default ListaDirecciones;