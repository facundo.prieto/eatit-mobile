import React, {useState, useEffect} from 'react';
import { StyleSheet, ScrollView, View, Image, ImageBackground } from 'react-native';
import { Card, Button, Text, Provider, Surface, Paragraph, Dialog, Portal, DarkTheme, ActivityIndicator } from 'react-native-paper';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import colors from '../assets/colors/colors';
import { vaciarCarrito, agregarPedido } from '../components/auth/auth';
import axiosPedido from "../components/axios/AxiosPedido";

const PaymentOk = ({route, navigation}) => {
    // console.log('PaymentOk')

    const { pedido, pedidos, token } = route.params;
    // console.log(pedidos)
    // console.log(pedido)
    // console.log(token)

    //Animaciónn de carga
    const [loading, setLoading] = useState(true);
    
    //Mensaje
    const [visibleMsg, setVisibleMsg] = useState(false);
    const [textoMsg, setTextoMsg] = useState('');
    const [severity, setSeverity] = useState('');

    useEffect(() => {
        crearPedido();
    }, []);

    const crearPedido = async () => {
        // console.log("crearPedido")
        
        setLoading(true);
        
        // console.log("pedido")
        // console.log(pedido)

        axiosPedido.post(`/crear/`, pedido, {
            headers: { Authorization: `${token}` },
        })
        .then(function (response) {
            // console.log("response")
            // console.log(response.data)
            if (response.status == 200) {
                // console.log(response.data);
                setSeverity("success");
                setVisibleMsg(true);
                setTextoMsg(`Estamos creando tu pedido `);

                vaciarCarrito();
                
                if(pedidos && pedidos.length > 0){
                    //Armo un objeto con los detalles de los productos y lo seleccionado al confirmar pedido
                    let pedidoSave = {
                        ...pedidos[0] , 
                        idPedido: response.data.idPedido,
                        fecha: response.data.fecha,
                        tiempoEstimado: response.data.tiempoEstimado,
                        comentarios: pedido.comentarios,
                        direccion: pedido.direccion,
                        estaPago: true,
                        monto: pedido.monto,
                        metodoPago: pedido.metodoPago,
                    }

                    agregarPedido(pedidoSave).then(idLocal => {
                        setLoading(false);
                        setVisibleMsg(false);

                        // console.log("then if")
                        // console.log(idLocal)
                        navigation.push('DetallesPedido', { idLocal:idLocal })
                    });

                }else{
                    let pedidoSave = {
                        ...pedido , 
                        idPedido: response.data.idPedido,
                        fecha: response.data.fecha,
                        tiempoEstimado: response.data.tiempoEstimado,
                        comentarios: pedido.comentarios,
                        direccion: pedido.direccion,
                        estaPago: true,
                        monto: pedido.monto,
                        metodoPago: pedido.metodoPago,
                    }

                    agregarPedido(pedidoSave).then(idLocal => {
                        setLoading(false);
                        setVisibleMsg(false);

                        // console.log("then else")
                        // console.log(idLocal)
                        navigation.push('DetallesPedidoHistorico', { pedido:{...pedidoSave , idLocal:idLocal}})
                    });
                    
                }
            } else {
                setSeverity("error");
                setVisibleMsg(true);
                setTextoMsg(response.message);
                // console.log(response.message);
                setLoading(false);
            }
        })
        .catch(function (error) {
            if (error.response.status == 400) {
                // Request made and server responded
                setTextoMsg(error.response.data);
                // console.log(error.response.data);
            } else {
                // Something happened in setting up the request that triggered an Error
                setTextoMsg(error.message);
                // console.log(error.mensage);
            }
            setSeverity("error");
            setVisibleMsg(true);
    
            setLoading(false);
        });
    }

    return(
        <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
            <Provider>
                <Card style={styles.card}>
                    <ScrollView style={{maxHeight: "100%"}}>
                        {loading ? 
                            <View style={styles.producto}>
                                <Text style={styles.textTitulo}>Estamos creando tu pedido</Text>
                                <ActivityIndicator style={{margin: 10}} animating={true} color={colors.red} size="large"  />
                            </View>
                        : 
                            <View style={styles.producto}>
                                <Text style={styles.textTitulo}>Tu pedido fué creado</Text>
                                <MaterialCommunityIcons name="check-outline" size={50} color={colors.red}/>
                            </View>
                        }

                        {pedidos ? 
                            <View>
                                <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20}}>
                                    <Text style={styles.textTitulo}>Resumen</Text>
                                </View>
                                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                    {pedidos && pedidos.map((pedido, index) => {
                                        return(
                                            <Surface key={index} style={styles.leftSurface}>

                                                <View style={styles.restaurante}>
                                                    {pedido.restaurante.imagenRestaurante  && pedido.restaurante.imagenRestaurante != "" ?
                                                        <Image style={styles.imagen} source={{uri: pedido.restaurante.imagenRestaurante}}/>
                                                    : null }
                                                    <View>
                                                        <Text style={styles.textTitulo}>{pedido.restaurante.nombreRestaurante}</Text>
                                                    </View>
                                                </View>
                                                
                                                <View style={styles.monto}>
                                                    <Text style={styles.textPrecio}>Subtotal</Text>
                                                    <Text style={styles.textPrecio}>$ {pedido.precioTotal}</Text>
                                                </View>
                                                <View style={styles.monto}>
                                                    <Text style={styles.textPrecio}>Envío</Text>
                                                    <Text style={styles.textPrecio}>$ {pedido.restaurante.costoEnvio}</Text>
                                                </View>

                                                <View style={styles.totalContainers}>
                                                    <View style={styles.montoTotal}>
                                                        <Text style={styles.textTotal}>Total</Text>
                                                        <Text style={styles.textTotal}>$ {pedido.precioTotal + pedido.restaurante.costoEnvio}</Text>
                                                    </View>
                                                </View>
                                            </Surface>
                                        )
                                    })}
                                </View>
                            </View>
                        :null}
                    </ScrollView>
                </Card>

                <Portal>
                    <Dialog visible={visibleMsg} onDismiss={() => setVisibleMsg(false)} theme={DarkTheme}>
                        {severity == "error" ? 
                            <Dialog.Title style={styles.textTitulo}>Ha ocurrido un error</Dialog.Title>
                        : 
                            null // <Dialog.Title style={styles.textTitulo}>Genial!</Dialog.Title>
                        }
                        <Dialog.Content style={{ flexDirection: 'row', alignContent: 'space-between' }}>
                            <Paragraph style={styles.text}>{textoMsg}</Paragraph>
                            {severity == "success" ? 
                                <ActivityIndicator style={{marginHorizontal: 10}} animating={true} color={colors.red} size="small"  />
                            : 
                                null
                            }
                        </Dialog.Content>
                        <Dialog.Actions>
                            {severity == "error" ? 
                                <Button theme={{ colors: {primary : colors.red}}} onPress={() => setVisibleMsg(false)}>Ok</Button>
                            : 
                                null
                            }
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </Provider>
        </ImageBackground>
    )
}
export default PaymentOk;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    card: {
        flex: 1,
        backgroundColor: colors.background,
        marginHorizontal: 10,
        borderRadius: 10,
        borderWidth: 3,
        minHeight: "70%"
    },
    producto: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        backgroundColor: colors.backgroundColor,
        paddingHorizontal: 5,
    },
    restaurante:{
        flex:1, 
        justifyContent: 'flex-start', 
        alignItems: 'center', 
        flexDirection: 'row', 
        marginHorizontal: 10,
        marginVertical: 5,
    },
    imagen: {
        height:40, 
        width:40, 
        marginRight: 5,
        borderRadius: 5,
    },
    text: {
        color: "white",
        fontSize: 14,
    },
    textTitulo: {
        color: "white",
        fontSize: 18,
        fontWeight: 'bold'
    },
    textPrecio: {
        color: "white",
        fontSize: 16,
    },
    textTotal: {
        color: "white",
        fontSize: 17,
        fontWeight: 'bold'
    },
    leftSurface: {
        flex: 1, 
        width: "90%", 
        minWidth: "90%", 
        justifyContent: 'center', 
        alignItems: 'flex-start',
        backgroundColor: colors.backgroundColor,
        padding: 5,
        elevation: 2,
        borderRadius: 2,
        marginBottom: 20,
    },
    precioTotal: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'flex-end',
        padding: 10,
    },
    center: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    monto: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        backgroundColor: colors.backgroundColor,
        paddingHorizontal: 10,
    },
    totalContainers: {
        flex: 1, 
        width: "100%", 
        alignItems: 'center', 
        paddingHorizontal: 10,
    },
    montoTotal: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        paddingTop: 3,
        marginTop: 3,
        borderTopColor: colors.white,
        borderTopWidth: 0.5,
    },
});