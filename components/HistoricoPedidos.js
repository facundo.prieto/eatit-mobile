import React, {useState, useEffect} from 'react';
import { RefreshControl, ImageBackground, SafeAreaView, View, ScrollView, TouchableOpacity, StyleSheet, Image , FlatList } from 'react-native';
import { Searchbar, ActivityIndicator, Card, Button, Text, Dialog, Portal, Provider, DarkTheme, Chip  } from 'react-native-paper';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Slider from '@react-native-community/slider';
import Navbar from '../components/Navbar';
import axiosCliente from "../components/axios/AxiosCliente";
import { getSession } from '../components/auth/auth';
import colors from '../assets/colors/colors';

const HistoricoPedidos = ({navigation}) => {
    const [listaPedidos, setListaPedidos] = useState([]);
    const [listaInicial, setListaInicial] = useState([]);
    const [listaFiltrada, setListaFiltrada] = useState([]);
    const [listaRestaurantes, setListaRestaurantes] = useState([]);
    const [token, setToken] = useState("");
    const [email, setEmail] = useState("");

    //Animaciónn de carga
    const [loading, setLoadingVisible] = useState(true);
    const setLoading = (load) => setLoadingVisible(load);

    //Popup de detalles
    const [popupDetalles, setVisibleDetalles] = useState(false);
    const hideDetalles = () => setVisibleDetalles(false);

    const getListaPedidos = async () => {
        
        setLoading(true);

        let loadtoken = "";
        let loademail = "";
        if(token != ""){
            loadtoken = token;
        }else{
            let session = await getSession();
            // console.log(`cargo token: ${session.token}`)
            if(session != null && session.token != null){
                loadtoken = session.token;
                setToken(session.token);
            }
            // console.log(`token cargado: ${token}`)
        }
        if(email != ""){
            loademail = email;
        }else{
            let session = await getSession();
            // console.log(`cargo email: ${session.email}`)
            if(session != null && session.email != null){
                loademail = session.email;
                setEmail(session.email);
            }
            // console.log(`email cargado: ${email}`)
        }   
        
        // PEDIDOS
        axiosCliente
        .get(
            `/pedidos/listar/${loademail}`,
            {},
            { headers: { Authorization: `${loadtoken}` } }
        )
        .then(function (response) {
            if(response.status == 200){
                // console.log(`cargo pedidos then: `);
                let listaAux = response.data //.filter((item) => item.estado === 'RECHAZADO' || item.estado === "ENTREGADO" || item.estado === "FINALIZADO")
                // console.log(`listaAux: `, listaAux);
                setListaPedidos(listaAux)
                setListaInicial(listaAux)
                setListaFiltrada(listaAux)
                // console.log(`cargo pedidos Lista Pedidos en HISTORICO PEDIDOS: `);
                // console.log(listaPedidos);
                // console.log(`setLoading: ${loading}`);
                setLoading(false);
                // console.log(`setLoading: ${loading}`);
            }
        })
        .catch(function (error) {
            // console.log(error);
            setLoading(false);
        });
        // console.log(`Pedidos cargados en HISTORICO PEDIDOS: `);
        // console.log(listaPedidos);
        
    }

    //Barra de busqueda
    const [busqueda, setBusqueda] = useState('');
    const filtrarBusqueda = (texto) => {
        if(texto){
            const newData = listaInicial.filter((item) => {
                // console.log(`item.descripcion en busqueda: ${item.restauranteNombre}`)
                if(item.restauranteNombre.toLowerCase().includes(texto.toLowerCase())){
                    return item;
                }
            })
            setListaFiltrada(newData)
            setBusqueda(texto)
        }else{
            setListaFiltrada(listaInicial)
            setBusqueda('')
        }
    }
    
    //Popup de filtros
    const [popupVisible, setPopUpVisible] = useState(false);
    const hidePopUp = () => setPopUpVisible(false);
    const [costoTope, setCostoTope] = useState(0);
    const [costoMaximo, setCostoMaximo] = useState(costoTope);
    const [restauranteSelected, setRestauranteSelected] = useState(null);

    const cargarFiltros = () => {
        
        //calculo costo máximo para el slider
        let topePrecio = 0;
        listaPedidos.forEach(pedido => {
            if(pedido.monto > topePrecio){
                topePrecio = pedido.monto
            }
        });
        setCostoTope(topePrecio)
        setCostoMaximo(topePrecio)

        //creo lista de restaurantes
        let restaurantes = [];
        let esta = false;
        let iPed = 0;
        // console.log(`creo lista de restaurantes - cantidad de pedidos: ${listaPedidos.length}`);
        listaPedidos.forEach(pedido => {
            restaurantes.forEach(restaurante => {
                    if(restaurante.restauranteImage === pedido.restauranteImage){
                        // console.log(`${pedido.restauranteNombre} vs ${restaurante.restauranteNombre}: true`);
                        esta = true;
                    } else {
                        // console.log(`${pedido.restauranteNombre} vs ${restaurante.restauranteNombre}: false`);
                    }
            });
            if(esta){
                iPed = iPed + 1;
                // console.log(`${iPed} - el restaurante: ${pedido.restauranteNombre} ya está en la lista - pedido #${pedido.idPedido}`);
                esta = false;
            } else {
                iPed = iPed + 1;
                restaurantes.push(pedido);
                // console.log(`${iPed} - el restaurante: ${pedido.restauranteNombre} fue agregado a la lista - pedido #${pedido.idPedido}`);
            }
        });
        setListaRestaurantes(restaurantes);
        setPopUpVisible(true);
        // console.log(`lista de restaurantes creada - cantidad restaurantes: ${listaRestaurantes.length}`);
        // console.log(listaRestaurantes);

    }

    const aplicarFiltros = () => {
        let newData = listaInicial;
        if(costoMaximo < costoTope){
            newData = listaInicial.filter((item) => item.monto < costoMaximo)
        }
        if(restauranteSelected){
            newData = newData.filter(item => item.restauranteNombre === restauranteSelected.restauranteNombre)
        }
        setListaFiltrada(newData)
        setPopUpVisible(false)
    }

    //limpiarFiltros
    const limpiarFiltros = () => {
        setBusqueda('');
        setListaFiltrada(listaInicial);
        setPopUpVisible(false);
    }    

    useEffect(() => {
        // console.log(`Inicio Historico Pedidos`)
        getListaPedidos();
        return () =>{
        }
    }, []);

    const [refreshing, setRefreshing] = useState(false);
  
    const onRefresh = React.useCallback(() => {
      setRefreshing(true);
      getListaPedidos().then(() => setRefreshing(false));
    }, []);

    return(
        <ImageBackground style={styles.fondocontainer} source={require('../assets/images/fondo.jpg')}>
            <ScrollView
            refreshControl={
                <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
                />
            }>
                <Navbar navigation={navigation}/>
                <View>
                    <Provider>
                        <Card style={styles.card}>
                            <SafeAreaView style={styles.container}>
                                <Provider theme={DarkTheme}>
                                    <View style={styles.filtrosContainer}>
                                        <View style={styles.filtroSearch}>
                                            <Searchbar
                                                placeholder="Buscar"
                                                onChangeText={filtrarBusqueda}
                                                value={busqueda}
                                            />
                                        </View>
                                        <TouchableOpacity onPress={() => cargarFiltros()} style={styles.filtroIcon}>
                                            <Ionicon name="options-outline" size={26} color={colors.white}/>
                                        </TouchableOpacity>
                                    </View>

                                    {loading ? 
                                        <ActivityIndicator style={styles.loading} animating={true} color={colors.red} size="small"  />
                                    :                                     
                                        <View style={styles.listaContainer}>
                                            {!listaFiltrada || listaFiltrada.length == 0 ?
                                                <View style={styles.loading}>
                                                    <Text style={styles.text} >Aún no has realizado pedidos</Text>
                                                </View>
                                            : 
                                                <ScrollView>
                                                    {listaFiltrada.map((item, index) =>
                                                        <TouchableOpacity
                                                        onPress={() => navigation.push('DetallesPedidoHistorico', { pedido: item })} 
                                                        key={index} 
                                                        style={styles.listaItem}>
                                                            <Card.Title
                                                                title={`${item.restauranteNombre}`}
                                                                titleStyle={styles.titleStyle}
                                                                subtitle={`#${item.idPedido} - ${item.fecha}`}
                                                                subtitleStyle={styles.subtitleStyle}
                                                                left={() => <Image style={styles.tinyLogo} source={item.restauranteImage ? {uri: item.restauranteImage}: null}/>}
                                                                right={() => 
                                                                    <View style={{marginRight:10,}}>
                                                                        <Text style={styles.precio}> {`$${item.monto}`}</Text>
                                                                    </View>}
                                                            />
                                                        </TouchableOpacity>
                                                    )}
                                                </ScrollView>
                                            }
                                        </View>
                                    }
                                </Provider>
                            </SafeAreaView>
                        </Card>
                    </Provider>
                    <Portal>
                        {/* PopUp de filtros */}
                        <Dialog visible={popupVisible} onDismiss={() => setPopUpVisible(false)} style={styles.alert}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: 10, marginTop: 10}}>
                                <Text> </Text>
                                <TouchableOpacity onPress={() => limpiarFiltros()}> 
                                    <Text style={styles.text} >Quitar filtros</Text>
                                </TouchableOpacity>
                            </View>
                            
                            <Dialog.Actions style={{justifyContent: 'center'}}>
                                <View style={styles.row}>
                                    <Text style={styles.costoext}> Precio</Text>
                                    <Slider 
                                        value={costoMaximo}
                                        maximumValue={costoTope}
                                        minimumValue={0}
                                        step={10}
                                        minimumTrackTintColor={colors.red}
                                        thumbTintColor={colors.red}
                                        onSlidingComplete={(value) => setCostoMaximo(parseInt(value))}
                                        style={styles.slider}
                                    />
                                    <Text style={styles.alertText} style={styles.costoMax}>{costoMaximo}</Text>
                                </View>
                            </Dialog.Actions>
                            <Dialog.Actions style={{justifyContent: 'center'}}>
                                <View style={styles.row}>
                                    <Text style={styles.costoext}> Restaurante ({listaRestaurantes.length}):</Text>
                                </View>
                            </Dialog.Actions> 
                            <ScrollView contentContainerStyle={styles.scrolllistaCates} horizontal>
                                <View style={styles.listaCates}>
                                    <ScrollView>
                                        {listaRestaurantes.map((item, index) =>
                                            <TouchableOpacity 
                                            onPress={() => setRestauranteSelected(item)} 
                                            key={index} 
                                            style={styles.cateItem}>
                                                {restauranteSelected && item.restauranteImage==restauranteSelected.restauranteImage ?
                                                    <Chip mode='outlined' theme={{ colors: {surface : colors.red}}}><Text style={styles.text}>{item.restauranteNombre}</Text></Chip>
                                                :
                                                    <Chip mode='outlined' theme={{ colors: {surface : colors.background}}}><Text style={styles.text}>{item.restauranteNombre}</Text></Chip>
                                                }
                                            </TouchableOpacity>
                                        )}
                                    </ScrollView>
                                </View>
                            </ScrollView>
                            
                            <Button mode="contained" style={styles.btn} theme={{ colors: {primary : colors.red}}} onPress={() => aplicarFiltros()}> Aplicar </Button>
                        </Dialog>
                    </Portal>
                </View>
            </ScrollView>
        </ImageBackground>
    )
}
export default HistoricoPedidos;

const styles = StyleSheet.create({
    fondocontainer: {
        flex: 1,
        paddingTop: 10,
        // justifyContent: 'center', 
        // alignItems: 'center'
    },
    card: {
        backgroundColor: colors.background,
        margin: 10,
        borderRadius: 10,
        borderWidth: 3,
        padding:5,
        // maxHeight:'85%'
    },
    loading: {
        marginVertical: 10,
    },
    text: {
      color: "white",
    },
    container: {
        flex: 1,
        backgroundColor: colors.background,
        marginHorizontal: 10,
    },
    filtrosContainer: {
        alignItems: 'center',
        alignContent: 'center',
        flexDirection: 'row',
        paddingVertical: 5,
    },
    filtroSearch: {
        flex: 9,
    },
    filtroIcon: {
        marginLeft: 5,
        alignItems: 'center',
        flex: 1,
    },
    listaContainer: {
        flex: 1,
    },
    listaItem: {
        backgroundColor: colors.white,
        color: colors.black,
        marginVertical: 8,
        borderRadius: 10,
        padding: 5,
    },
    titleStyle: {
        color: colors.black,
        fontSize: 18
    },
    subtitleStyle: {
        color: colors.black,
        fontSize: 12
    },
    precio: {
        color: colors.red,
        fontSize: 14,
        marginHorizontal: 5
    },
    tinyLogo: {
        height:40, 
        width:40,
    },
    btn: {
        marginTop: 20,
        // color: colors.white,
        // backgroundColor: colors.backBox,
        marginBottom: 20,
        marginHorizontal: 10
    },
    alert: {
        backgroundColor: colors.background,
    },
    alertText: {
        color: colors.white,
    },
    row: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginHorizontal: 5
    },
    costoext: {
        flex: 1, 
        color: colors.white,
    },
    slider: {
        flex: 4, 
        marginHorizontal: 5, 
    },
    costoMax: {
        flex: 1, 
        color: colors.white,
    },
    scrolllistaCates: {
        // height: 200,
        // weight: 330,
    },
    listaCates: {
        margin: 2,
        flexWrap: 'wrap',
    },
    cateItem: {
        margin: 3,
        flexWrap: 'wrap',
    },
    restauranteNombre: {
        marginVertical: 5,
        color: colors.white,
        fontSize: 20,
    },
});