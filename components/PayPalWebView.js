import React from 'react';
import {View, Image, Dimensions, ImageBackground, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';

import Navbar from '../components/Navbar';

const {width,height} = Dimensions.get('screen');

const Loading = () => {
    return(
        <View style={{height:height,width:width,justifyContent:'center',alignItems:'center'}}>
            <Image 
            source={require('../assets/images/paypal.png')}
            style={{
                flex: 1,
                width: null,
                height: null,
                resizeMode:'contain'}}
            />
        </View>
    )
}

const PayPalWebView = ({route, navigation}) => {
    // console.log('PayPalWebView')

    const { pedido, pedidos, token } = route.params;
    // console.log(pedidos)
    // console.log(pedido)
    // console.log(token)

    const stateChng = (navState) => {
        // console.log(navState);

        const { url } = navState ;
        
        // console.log("url ",url);

        let busqueda = url.search("/payment-confirmed")

        if(busqueda > (-1)){

            navigation.push('PaymentOk', { pedido:pedido, pedidos:pedidos, token:token })
        }
    }
      
    return (
        <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')} >
            <Navbar navigation={navigation} ocultarDireccion={true}/>
            <View style={styles.webViewContainer}>
                <WebView 
                    style={styles.webView}
                    startInLoadingState={true}
                    onNavigationStateChange={stateChng}
                    renderLoading={() => <Loading />}
                    source={{ uri: 'http://ec2-3-89-140-231.compute-1.amazonaws.com/paypal/payment.html?amount='+ pedido.monto }} />
            </View>
        </ImageBackground>
    );
}
export default PayPalWebView;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    webViewContainer: {
        flex: 8,
        alignItems: 'center',
        marginTop:50,
    },
    webView: {
        maxHeight: "100%",
        minWidth: "150%",
        backgroundColor:'transparent',
    }
})