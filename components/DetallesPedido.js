import React, {useState, useEffect} from 'react';
import { StyleSheet, ScrollView, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { Card, Button, Text, List, TextInput, Provider, IconButton, Surface, Paragraph, Dialog, Portal, ActivityIndicator, DarkTheme } from 'react-native-paper';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Navbar from '../components/Navbar';
import colors from '../assets/colors/colors';
import { getSession, getPedido, actualizarPedido } from '../components/auth/auth';
import axiosCliente from "../components/axios/AxiosCliente";

const DetallesPedido = ({route, navigation}) => {
    // console.log('DetallesPedido')

    const { idLocal } = route.params;
    // console.log("idLocal")
    // console.log(idLocal)

    //Animaciónn de carga 
    const [loading, setLoading] = useState(false);
    const [loadingCalif, setLoadingCalif] = useState(false);
    
    //Mensaje
    const [visibleMsg, setVisibleMsg] = useState(false);
    const [textoMsg, setTextoMsg] = useState('');
    const [severity, setSeverity] = useState('');

    const [visibleRechazoMsg, setVerRechazoMsg] = useState(false);

    const [pedidoState, setPedido] = useState(null);

    useEffect(() => {
        // console.log('useEffect')
        getPedidoBack();
        
    }, []);

    setTimeout(() => {
        // console.log('setTimeout 40000')
        getPedidoBack();
    }, 40000)

    const getPedidoBack = async () => {
        // console.log('getPedidoBack')
        // console.log("pedidoState ", pedidoState)

        await getPedido(idLocal).then(storedPedido => {
            // console.log("storedPedido ", storedPedido)
    
            if(storedPedido != null){
                //Consultar con el back el estado del pedido
                // console.log('pedidoState != null')
                getEstado(storedPedido);
            }
        })
        ;
    }

    const [estadoText, setEstadoText] = useState('El restaurante está revisando tu pedido');
    const [estadoLoading, setEstadoLoading] = useState(true);

    
    const getEstado = async (pedido) => {
        // console.log("getEstado<<<<<<<<<<<<<<<<<<<<<<<<");
        // console.log("pedido ",pedido);
        let session = await getSession();
        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        let idPedido = 1; 
        if(pedido != null && pedido.idPedido != null){
            idPedido = pedido.idPedido;
        }
        axiosCliente.get(
            `/pedido/buscar/${idPedido}`,
        {},
        { headers: { Authorization: `${token}` } }
        )
        .then(function (response) {
            // console.log("response.data ",response.data);
            if (response.status == 200) {
                
                if(response.data.estado=='CREADO'){
                    setEstadoText('El restaurante está revisando tu pedido');
                    setEstadoLoading(true);
                }else if(response.data.estado=='CONFIRMADO'){
                    setEstadoText('El restaurante está preparando tu orden');
                    setEstadoLoading(true);
                }else if(response.data.estado=='EN_CAMINO'){
                    setEstadoText('Tu orden va en camino a tu dirección');
                    setEstadoLoading(true);
                }else if(response.data.estado=='RECHAZADO'){
                    setEstadoText('Lo sentimos, el restaurante no podrá completar tu orden');
                    if(!pedido.reclamo && pedido.metodoPago=='PAYPAL' && pedido.estaPago==true){
                        setVerRechazoMsg(true);
                    }
                    setEstadoLoading(false);
                }else if(response.data.estado=='CANCELADO'){
                    setEstadoText('Lo sentimos, el restaurante no podrá completar tu orden');
                    if(!pedido.reclamo && pedido.metodoPago=='PAYPAL' && pedido.estaPago==true){
                        setVerRechazoMsg(true);
                    }
                    setEstadoLoading(false);
                }else if(response.data.estado=='ENTREGADO'){
                    setEstadoText('Esperamos que disfrutes tu pedido');
                    setEstadoLoading(false);
                }else if(response.data.estado=='FINALIZADO'){
                    setEstadoText('Esperamos que hayas disfrutado tu orden');
                    setEstadoLoading(false);
                }

                if(!pedido.estado || pedido.estado!=response.data.estado){
                    let tempPedido = {...pedido};
                    tempPedido.estado = response.data.estado;
                    tempPedido.tiempoEstimado = response.data.tiempoEstimado;
                    setPedido(tempPedido);
                    actualizarPedido(tempPedido)
                }
            } else {
                setTextoMsg(response.data);
                setVisibleMsg(true);
                setSeverity("error");
            }
        })
        .catch(function (error) {
            // console.log(error.message);
            if (error.response.status == 400) {
                // Request made and server responded
                setTextoMsg(error.response.data);
                setVisibleMsg(true);
                setSeverity("error");
            } else {
                // Something happened in setting up the request that triggered an Error
                setTextoMsg(error.message);
                setVisibleMsg(true);
                setSeverity("error");
            }
        });
    }


    const [expanded, setExpanded] = useState(false);
    const handlePress = () => setExpanded(!expanded);

    
    //PopUp de calificacion
    const [popupVisible, setVisiblePopUp] = useState(false);
    const hidePopUp = () => setVisiblePopUp(false);

    const estrellasDsp = (cali) => {
        var lis = [];
        let index = 0;
        for (let i = 0; i < cali; i++){
            index += 1;
            lis.push(<MaterialCommunityIcons key={index} name="star" size={25} color={colors.red}/> );
        }
        for (let j = 0; j < 5-cali; j++){
            index += 1;
            lis.push(<MaterialCommunityIcons key={index} name="star" size={25} color={colors.backBox}/> );
        }
        return lis;
    }

    const [starsSelected, setStars] = useState(0);
    const [descCalificacion, setDescCalificacion] = useState('');

    const onChangeDesc = descCalificacion => setDescCalificacion(descCalificacion);

    const estrellasPopup = (cali) => {
        let stars = 0
        if(starsSelected != 0){
            stars = starsSelected;
        }else if(cali && cali != 0 && cali!=starsSelected){
            setStars(cali)
            stars = cali
        }

        var lis = [];
        let index = 0;
        for (let i = 0; i < stars; i++){
            index += 1;
            lis.push(
                <TouchableOpacity key={index} onPress={() => setStars(i+1)} >
                    <MaterialCommunityIcons style={{marginHorizontal: 2}} name="star" size={35} color={colors.red}/> 
                </TouchableOpacity>
            );
        }
        for (let j = 0; j < 5-stars; j++){
            index += 1;
            lis.push(
                <TouchableOpacity key={index} onPress={() => setStars(stars+j+1)} >
                    <MaterialCommunityIcons style={{marginHorizontal: 2}} name="star" size={35} color={colors.backBox}/> 
                </TouchableOpacity>
            );
        }
        return lis;
    }
    
    const borrarCalificacion = () => {
        eliminarCalificacion();
        
        let tempPedido = {...pedidoState};
        tempPedido.calificacion = 0;
        tempPedido.descCalificacion = '';
        hidePopUp();
        setPedido(tempPedido);
        setStars(0);
        setDescCalificacion('');
        actualizarPedido(tempPedido)
    }

    const confirmarCalificacion = () => {
        if(pedidoState && (!pedidoState.calificacion || pedidoState.calificacion == 0)){
            agregarCalificacion();
        }else{
            modificarCalificacion();
        }

        let tempPedido = {...pedidoState};
        tempPedido.calificacion = starsSelected;
        tempPedido.descCalificacion = descCalificacion;

        hidePopUp();
        setPedido(tempPedido);
        actualizarPedido(tempPedido)
    }
    
    const agregarCalificacion = async () => {
        setLoadingCalif(true);
        let session = await getSession();
        let email = "";
        if(session != null && session.email != null){
            email = session.email;
        }
        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        let idPedido = 1;
        if(pedidoState != null && pedidoState.idPedido != null){
            idPedido = pedidoState.idPedido;
        }
        axiosCliente
        .post(`/calificar/restaurante/`, {
            descripcion: descCalificacion,
            idPedido: idPedido,
            usuarioDestino: "",
            valorCalificacion: starsSelected
        },
        {   headers: {Authorization: `${token}`}    }
        )
        .then(function (response) {
            // console.log(response);
            if (response.status == 200) {

                setTextoMsg("Gracias por calificar tu orden!");
                setVisibleMsg(true);
                setSeverity("succes");

                //Me guardo todas las Calificaciones menos la seleccionada
                
            } else {
                setTextoMsg(response.data);
                setVisibleMsg(true);
                setSeverity("error");
            }
            setLoadingCalif(false);
        })
        .catch(function (error) {
            if (error.response.status == 400) {
                // Request made and server responded
                setTextoMsg(error.response.data);
                setVisibleMsg(true);
                setSeverity("error");
            } else {
                // Something happened in setting up the request that triggered an Error
                setTextoMsg(error.message);
                setVisibleMsg(true);
                setSeverity("error");
            }
            setLoadingCalif(false);
        });
    }

    const modificarCalificacion = async () => {
        setLoadingCalif(true);
        let session = await getSession();
        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        let idPedido = 1;
        if(pedidoState != null && pedidoState.idPedido != null){
            idPedido = pedidoState.idPedido;
        }
        axiosCliente
        .put("/calificar/restaurante/modificar", { 
            descripcion: descCalificacion,
            idPedido: idPedido,
            valorCalificacion: starsSelected
        },
        {   headers: {Authorization: `${token}`}    }
        )
        .then(function (response) {
            // console.log(response);
            if (response.status == 200) {

                setTextoMsg("Gracias por calificar tu orden!");
                setVisibleMsg(true);
                setSeverity("succes");

                //Me guardo todas las Calificaciones menos la seleccionada
                
            } else {
                setTextoMsg(response.data);
                setVisibleMsg(true);
                setSeverity("error");
            }
            setLoadingCalif(false);
        })
        .catch(function (error) {
            if (error.response.status == 400) {
                // Request made and server responded
                setTextoMsg(error.response.data);
                setVisibleMsg(true);
                setSeverity("error");
            } else {
                // Something happened in setting up the request that triggered an Error
                setTextoMsg(error.message);
                setVisibleMsg(true);
                setSeverity("error");
            }
            setLoadingCalif(false);
        });
    }

    const eliminarCalificacion = async () => {
        setLoadingCalif(true);
        let session = await getSession();
        let email = "";
        if(session != null && session.email != null){
            email = session.email;
        }
        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        let idPedido = 1;
        if(pedidoState != null && pedidoState.idPedido != null){
            idPedido = pedidoState.idPedido;
        }
        axiosCliente
        .delete(`/calificar/pedido/${idPedido}/eliminar`,
        {   headers: {Authorization: `${token}`}    }
        )
        .then(function (response) {
            // (response);
            if (response.status == 200) {

                setTextoMsg("No te olvides de calificar tu orden!");
                setVisibleMsg(true);
                setSeverity("succes");

                //Me guardo todas las Calificaciones menos la seleccionada
                
                
            } else {
                setTextoMsg(response.data);
                setVisibleMsg(true);
                setSeverity("error");
            }
            setLoadingCalif(false);
        })
        .catch(function (error) {
            if (error.response.status == 400) {
                // Request made and server responded
                setTextoMsg(error.response.data);
                setVisibleMsg(true);
                setSeverity("error");
            } else {
                // Something happened in setting up the request that triggered an Error
                setTextoMsg(error.message);
                setVisibleMsg(true);
                setSeverity("error");
            }
            setLoadingCalif(false);
        });
    }

    return(
        <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
            <Provider>
                <Navbar navigation={navigation} ocultarGoBack={true} ocultarDireccion={true} tuPedido={true}/>
                <Card style={styles.card}>
                    <ScrollView>
                        {pedidoState ? 
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 10}}>
                                <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                    <View style={styles.producto}>
                                        <Text style={styles.textTitulo}>{estadoText}</Text>
                                        {estadoLoading ? 
                                            <ActivityIndicator style={{margin: 5}} animating={true} color={colors.red} size="small"  />
                                        : null}
                                    </View>
                                    {pedidoState.tiempoEstimado && pedidoState.tiempoEstimado != "" && pedidoState.tiempoEstimado != "0" 
                                    && (pedidoState.estado == 'CONFIRMADO' || pedidoState.estado == 'EN_CAMINO') ?  
                                        <View style={styles.producto}>
                                            <Text style={styles.text}>Tiempo estimado: {pedidoState.tiempoEstimado} min</Text>
                                        </View>
                                    : null}
                                </Surface>
                                
                                <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                    <View style={{paddingHorizontal: 5,width:"100%"}}>
                                        <Text style={styles.text}>Dirección de entrega</Text>
                                        <Text style={styles.textTitulo}>{pedidoState.direccion}</Text>
                                    </View>
                                </Surface>

                                <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                    <View style={{paddingHorizontal: 5, width:"100%"}}>
                                        <View style={{...styles.restaurante, marginBottom: 10}}>
                                            {pedidoState.restaurante.imagenRestaurante  && pedidoState.restaurante.imagenRestaurante != "" ?
                                                <Image style={styles.imagen} source={{uri: pedidoState.restaurante.imagenRestaurante}}/>
                                            : null }
                                            <View>
                                                <Text style={styles.textTitulo}>{pedidoState.restaurante.nombreRestaurante}</Text>
                                                <Text style={styles.text}>Envío $ {pedidoState.restaurante.costoEnvio}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.restaurante}>
                                            <Text style={styles.text}>Metodo de pago: {pedidoState.metodoPago}</Text>
                                        </View>
                                    </View>

                                    <List.Section style={styles.listSection} >
                                        <List.Accordion
                                            style={styles.listAccordion}
                                            title={`Tu compra: $ ${pedidoState.monto}`}
                                            expanded={expanded}
                                            theme={{ colors: { text: colors.white, primary: colors.white, accent: colors.background, background: colors.background} }}
                                            onPress={handlePress}>
                                            {pedidoState.productos && pedidoState.productos.map((prod, index) =>
                                                <List.Item 
                                                    style={styles.listItem} 
                                                    titleStyle={styles.textDirDesc} 
                                                    key={index} 
                                                    title={prod.titulo + " $" + prod.total + (prod.descuento ? ' - Descuento: $' + prod.descuento : "")} 
                                                    left={(props) => <Image {...props} style={styles.tinyLogo} source={{uri: prod.imagen}}/>}/>
                                            )}
                                        </List.Accordion>
                                    </List.Section>
                                </Surface>

                                {pedidoState.comentarios && pedidoState.comentarios!= "" ? 
                                    <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                        <View style={{width:"100%"}}>
                                            <Text style={styles.text}>Tus comentarios</Text>
                                            <Text style={styles.text}>{pedidoState.comentarios}</Text>
                                        </View>
                                    </Surface>
                                : null}

                                {pedidoState.calificacion && pedidoState.calificacion> 0 ? 
                                    <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                        <View style={styles.producto}>
                                            <Text style={styles.textTitulo}>Tu calificacion</Text>
                                            <TouchableOpacity onPress={() => setVisiblePopUp(true)}
                                            //disabled={!pedidoState.estado || pedidoState.estado!='FINALIZADO' }
                                            >
                                                <View style={styles.estrellas}>
                                                    {loadingCalif ? 
                                                        <ActivityIndicator animating={true} color={colors.red} size="small"  />
                                                    : 
                                                        estrellasDsp(pedidoState.calificacion)
                                                    }
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </Surface>
                                : 
                                    <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                        <TouchableOpacity style={styles.producto} onPress={() => setVisiblePopUp(true)}
                                        disabled={!pedidoState.estado || pedidoState.estado!='FINALIZADO' }
                                        >
                                            <Text 
                                                style={!pedidoState.estado || pedidoState.estado!='FINALIZADO' ? styles.textTituloDisabled : styles.textTitulo}
                                            >Califica tu orden</Text>
                                            {loadingCalif ? 
                                                <ActivityIndicator animating={true} color={colors.red} size="small"  />
                                            : 
                                                <MaterialCommunityIcons name="star" size={25} color={colors.red}/> 
                                            }
                                        </TouchableOpacity>
                                    </Surface>
                                }

                                {pedidoState.reclamo ? 
                                    <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                        <TouchableOpacity style={styles.producto} onPress={() => navigation.push('DetallesReclamo', { idLocal:idLocal })} 
                                        disabled={!pedidoState.estado || pedidoState.estado=='CREADO'}
                                        >
                                            <Text 
                                                style={!pedidoState.estado || pedidoState.estado=='CREADO' ? styles.textTituloDisabled : styles.textTitulo}
                                            >Detalle del reclamo</Text>
                                            <MaterialCommunityIcons name="forum" size={25} color={colors.red}/> 
                                        </TouchableOpacity>
                                    </Surface>
                                : 
                                    <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                        <TouchableOpacity style={styles.producto} onPress={() => navigation.push('IngresarReclamo', { idLocal:idLocal })} 
                                        disabled={!pedidoState.estado || pedidoState.estado=='CREADO'}
                                        >
                                            <Text 
                                                style={!pedidoState.estado || pedidoState.estado=='CREADO' ? styles.textTituloDisabled : styles.textTitulo}
                                            >Pide ayuda con tu orden</Text>
                                            <MaterialCommunityIcons name="forum" size={25} color={colors.red}/> 
                                        </TouchableOpacity>
                                    </Surface>
                                }
                            </View>
                        : null}
                    </ScrollView>
                </Card>

                <Portal>
                    {/* PopUp de agregar calificacion */}
                    <Dialog visible={popupVisible} onDismiss={hidePopUp} style={styles.alert}>
                        <Dialog.Title style={styles.textTitulo}>Califica tu orden</Dialog.Title>
                        <Dialog.Content>
                            {pedidoState ? 
                                <View style={styles.alertestrellas}>
                                    {estrellasPopup(pedidoState.calificacion)}
                                </View>
                            :null}
                            <View style={{marginTop:30}}>
                                <TextInput dense={true} mode="flat" placeholder="Cuentanos tu experiencia" value={descCalificacion} 
                                    onChangeText={onChangeDesc} 
                                    style={styles.textInput} 
                                    theme={{ colors: { text: colors.white, primary: colors.white, placeholder : colors.white} }}
                                    underlineColor={colors.white}
                                />
                            </View>
                        </Dialog.Content>
                        <Dialog.Actions style={{justifyContent: 'center'}}>
                            {pedidoState && pedidoState.calificacion && pedidoState.calificacion > 0 ? 
                                <Button mode="contained" style={styles.btn} onPress={borrarCalificacion}
                                icon={({ size, color }) => (<MaterialCommunityIcons name="trash-can" size={14} color={colors.white}/>)}
                                > Eliminar </Button>
                            :null}
                            <Button mode="contained" style={styles.btn} onPress={confirmarCalificacion}
                            icon={({ size, color }) => (<MaterialCommunityIcons name="check-outline" size={14} color={colors.white}/>)}
                            > Confirmar </Button>
                        </Dialog.Actions>
                    </Dialog>

                    <Dialog visible={visibleMsg} onDismiss={() => setVisibleMsg(false)} theme={DarkTheme}>
                        {severity == "error" ? 
                            <Dialog.Title style={styles.textTitulo}>Ha ocurrido un error</Dialog.Title>
                        : 
                            null // <Dialog.Title style={styles.textTitulo}>Genial!</Dialog.Title>
                        }
                        <Dialog.Content>
                            <Paragraph style={styles.text}>{textoMsg}</Paragraph>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button theme={{ colors: {primary : colors.red}}} onPress={() => setVisibleMsg(false)}>Ok</Button>
                        </Dialog.Actions>
                    </Dialog>

                    <Dialog visible={visibleRechazoMsg} onDismiss={() => setVerRechazoMsg(false)} theme={DarkTheme}>
                        <Dialog.Title style={styles.textTitulo}>El restaurante no podrá completar tu orden</Dialog.Title>
                        <Dialog.Content>
                            <Paragraph style={styles.text}>Por favor ingresa un reclamo para acordar la devolución con el restaurante.</Paragraph>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button theme={{ colors: {primary : colors.red}}} onPress={() => setVerRechazoMsg(false)}>Ok</Button>
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </Provider>
        </ImageBackground>
    )
}
export default DetallesPedido;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    card: {
        flex: 8,
        backgroundColor: colors.background,
        marginHorizontal: 10,
        marginVertical: 10,
        paddingVertical: 10,
        borderRadius: 10,
        borderWidth: 3,
        minWidth: "90%",
        maxHeight: "85%"
    },
    restaurante:{
        flex:1, 
        justifyContent: 'flex-start', 
        alignItems: 'center', 
        flexDirection: 'row', 
    },
    imagen: {
        height:40, 
        width:40, 
        marginRight: 10,
        borderRadius: 5,
    },
    tinyLogo: {
        height:30, 
        width:30,
    },
    text: {
        color: "white",
        fontSize: 14,
    },
    textTitulo: {
        color: "white",
        fontSize: 18,
        fontWeight: 'bold'
    },
    textTituloDisabled: {
        color: "gray",
        fontSize: 18,
        fontWeight: 'bold'
    },
    textPrecio: {
        color: "white",
        fontSize: 16,
    },
    textTotal: {
        color: "white",
        fontSize: 17,
        fontWeight: 'bold'
    },
    textProductos: {
        marginVertical: 10,
        color: "white",
        fontSize: 16
    },
    textPrecioOg: {
        color: colors.red,
        fontSize: 12,
        textDecorationLine: 'line-through'
    },
    textDescuento: {
        color: "white",
        fontSize: 12,
    },
    left: {
        flex: 1, 
        width: "90%", 
        minWidth: "90%", 
        justifyContent: 'center', 
        alignItems: 'flex-start',
        backgroundColor: colors.backgroundColor,
    },
    listAccordionleft: {
        flex: 1, 
        width: "100%", 
        minWidth: "100%", 
        justifyContent: 'center', 
        alignItems: 'center',
        //backgroundColor: colors.red,
        paddingHorizontal: 5,
        elevation: 2,
        borderRadius: 2,
        //marginBottom: 20,
    },
    leftSurface: {
        flex: 1, 
        width: "90%", 
        minWidth: "90%", 
        justifyContent: 'center', 
        alignItems: 'flex-start',
        backgroundColor: colors.backgroundColor,
        padding: 15,
        elevation: 2,
        borderRadius: 2,
        marginBottom: 20,
    },
    sinItems: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: colors.backgroundColor,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
        margin: 5,
    },
    right: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'flex-end'
    },
    precioTotal: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'flex-end',
        padding: 10,
    },
    center: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    listaProductos: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'center',
        padding: 5,
    },
    producto: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        backgroundColor: colors.backgroundColor,
        paddingHorizontal: 5,
    },
    estrellas: {
        flex: 1, 
        width: "80%", 
        justifyContent: 'flex-end', 
        alignItems: 'center', 
        flexDirection: 'row',
        //backgroundColor: colors.black,
        paddingHorizontal: 5,
    },
    listSection: {
        flex: 1, 
        width: "100%", 
        height: "100%", 
    },
    listAccordion: {
        flex: 1, 
        width: "100%", 
        height: "100%", 
    },
    listItem: {
        padding: 10,
        backgroundColor: colors.backBox,
    },
    textDirDesc: {
        color: colors.white,
        fontSize: 14,
    },
    monto: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        backgroundColor: colors.backgroundColor,
        paddingHorizontal: 10,
    },
    totalContainers: {
        flex: 1, 
        width: "100%", 
        alignItems: 'center', 
        paddingHorizontal: 10,
    },
    montoTotal: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        paddingTop: 3,
        marginTop: 3,
        borderTopColor: colors.white,
        borderTopWidth: 0.5,
    },
    btn: {
        marginVertical: 10,
        marginHorizontal: 10,
        color: colors.white,
        backgroundColor: colors.red,
    },
    textInput: {
        width: "100%",
        backgroundColor: "#ffffff00",
        fontSize: 15,
        marginBottom: 10
    },
    cantBtn: {
        marginHorizontal: 10,
    },
    vaciarCarrito: {
        marginVertical: 10,
    },
    vaciarCarritoText: {
        color: colors.red,
        fontSize: 16,
        textDecorationLine: 'underline',
    },
    slider: {
        flex: 1, 
        margin: 5, 
        transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }],
        width: "70%",
    },
    alert: {
        backgroundColor: colors.background,
        alignItems: 'center', 
        justifyContent: 'center', 
        //width: "80%", 
    },
    alertText: {
        color: colors.white,
    },
    alertestrellas: {
        marginVertical: 10,
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'row',
    },
});