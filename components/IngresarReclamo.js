import React, {useState, useEffect} from 'react';
import { StyleSheet, ScrollView, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { Card, Button, Text, List, TextInput, Provider, IconButton, Surface, Paragraph, Dialog, Portal, RadioButton, DarkTheme, ActivityIndicator } from 'react-native-paper';

import Navbar from '../components/Navbar';
import colors from '../assets/colors/colors';
import { getSession, getPedido, actualizarPedido } from '../components/auth/auth';
import axiosReclamo from "../components/axios/AxiosReclamo";

const IngresarReclamo = ({route, navigation}) => {

    const { idLocal, pedido } = route.params;
    // console.log("idLocal", idLocal)
    // console.log("pedido", pedido)

    const [loading, setLoadingVisible] = useState(true);
    const setLoading = (load) => setLoadingVisible(load);

    const [visibleMsg, setVisibleMsg] = useState(false);
    const [textoMsg, setTextoMsg] = useState('');
    const [severity, setSeverity] = useState('');

    const [pedidoState, setPedido] = useState(null);

    useEffect(() => {
        setLoading(true);
        getPedidoBack();
        
    }, []);

    const getPedidoBack = async () => {
      
        if(idLocal){
            let storedPedido = await getPedido(idLocal);
             if(storedPedido != null){
                setPedido(storedPedido);
                setLoading(false);
            }
        }else if(pedido){
            setPedido(pedido);
            setLoading(false);
        }
    }

    const [motivo, setMotivo] = useState('');

    const [comentarios, setComentarios] = useState('');
    const onChangeComentarios = comentarios => setComentarios(comentarios);

    const ingresarReclamo = async () => {
        setLoading(true);

        let session = await getSession();

        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        let email = "";
        if(session != null && session.email != null){
            email = session.email;
        }

        let datosEnviar = {
          descripcion: "[" + motivo + "] " + comentarios,
          email: `${email}`,
          pedido: pedidoState.idPedido 
        };
        // console.log(datosEnviar);
        axiosReclamo
          .post(`/crear/`, datosEnviar, {
            headers: { Authorization: `${token}` },
          })
          .then(function (response) {
            if (response.status == 200) {
                setSeverity("success");
                setVisibleMsg(true);
                setTextoMsg(`Reclamo ingresado!`);

                let tempPedido = pedidoState;
                tempPedido.reclamo = {
                    motivo:motivo,
                    comentarios:comentarios,
                };
                actualizarPedido(tempPedido).then(() => {
                    navigation.push('DetallesPedidoHistorico', { pedido:tempPedido })
                });
            } else {
                setSeverity("error");
                setVisibleMsg(true);
                setTextoMsg(response.message);
            }
            setLoading(false);
          })
          .catch(function (error) {
            setSeverity("error");
            setVisibleMsg(true);
            setLoading(false);
          });
      };

    return(
        <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
            <Provider>
                <Navbar navigation={navigation} ocultarDireccion={true}/>
                <Card style={styles.card}>
                    <ScrollView>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 10}}>
                            <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                <View style={styles.producto}>
                                    {loading ?
                                        <ActivityIndicator style={{marginVertical: 10}} animating={true} color={colors.red} size="small"/>
                                    : <Text style={styles.textTitulo}> Pedido #{pedidoState.idPedido} </Text> }
                                </View>
                            </Surface>
                        </View>

                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20}}>
                            <Text style={styles.textTitulo}>Indica el motivo del reclamo</Text>
                        </View>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <Surface style={styles.leftSurface}>
                                <View style={{ flexDirection: 'row', alignContent: 'flex-start' }}>
                                    <View style={{ flex: 1 }}>
                                        <RadioButton
                                            value="Problema con el restaurante"
                                            color={colors.red}
                                            uncheckedColor="grey"
                                            theme={{ colors: { text: colors.white} }}
                                            status={ motivo === 'Problema con el restaurante' ? 'checked' : 'unchecked' }
                                            onPress={() => setMotivo('Problema con el restaurante')}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={() => setMotivo('Problema con el restaurante')} style={{ flex: 7, alignSelf: 'center' }}>
                                        <Text style={motivo === 'Problema con el restaurante' ? styles.textTitulo : styles.text}>Problema con el restaurante</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', alignContent: 'flex-start' }}>
                                    <View style={{ flex: 1 }}>
                                        <RadioButton
                                            value="Inconformidad con la comida"
                                            color={colors.red}
                                            uncheckedColor="grey"
                                            status={ motivo === 'Inconformidad con la comida' ? 'checked' : 'unchecked' }
                                            onPress={() => setMotivo('Inconformidad con la comida')}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={() => setMotivo('Inconformidad con la comida')} style={{ flex: 7, alignSelf: 'center' }}>
                                        <Text style={motivo === 'Inconformidad con la comida' ? styles.textTitulo : styles.text}>Inconformidad con la comida</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', alignContent: 'flex-start' }}>
                                    <View style={{ flex: 1 }}>
                                        <RadioButton
                                            value="Dudas sobre el envio"
                                            color={colors.red}
                                            uncheckedColor="grey"
                                            status={ motivo === 'Dudas sobre el envio' ? 'checked' : 'unchecked' }
                                            onPress={() => setMotivo('Dudas sobre el envio')}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={() => setMotivo('Dudas sobre el envio')} style={{ flex: 7, alignSelf: 'center' }}>
                                        <Text style={motivo === 'Dudas sobre el envio' ? styles.textTitulo : styles.text}>Dudas sobre el envio</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', alignContent: 'flex-start' }}>
                                    <View style={{ flex: 1 }}>
                                        <RadioButton
                                            value="Reclamar devolucion de pedido cancelado"
                                            color={colors.red}
                                            uncheckedColor="grey"
                                            status={ motivo === 'Reclamar devolucion de pedido cancelado' ? 'checked' : 'unchecked' }
                                            onPress={() => setMotivo('Reclamar devolucion de pedido cancelado')}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={() => setMotivo('Reclamar devolucion de pedido cancelado')} style={{ flex: 7, alignSelf: 'center' }}>
                                        <Text style={motivo === 'Reclamar devolucion de pedido cancelado' ? styles.textTitulo : styles.text}>Reclamar devolucion de pedido cancelado</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', alignContent: 'flex-start' }}>
                                    <View style={{ flex: 1 }}>
                                        <RadioButton
                                            value="Otro motivo"
                                            color={colors.red}
                                            uncheckedColor="grey"
                                            status={ motivo === 'Otro motivo' ? 'checked' : 'unchecked' }
                                            onPress={() => setMotivo('Otro motivo')}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={() => setMotivo('OTRO')} style={{ flex: 7, alignSelf: 'center' }}>
                                        <Text style={motivo === 'Otro motivo' ? styles.textTitulo : styles.text}>Otro motivo</Text>
                                    </TouchableOpacity>
                                </View>
                            </Surface>
                        </View>

                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20}}>
                            <Text style={styles.textTitulo}>Cuéntanos el problema</Text>
                        </View>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                <View style={{width:"90%"}}>
                                    <TextInput dense={false} mode="flat" label="Comentarios" value={comentarios} 
                                        onChangeText={onChangeComentarios} 
                                        style={styles.textInput} 
                                        theme={{ colors: { text: colors.white, primary: colors.white} }}
                                    />
                                </View>
                            </Surface>
                        </View>

                        <Button mode="contained" style={styles.btn} onPress={() => ingresarReclamo()} 
                            disabled={!motivo}
                            loading={loading}
                        >Ingresar reclamo</Button>
                    </ScrollView>
                </Card>

                <Portal>
                    <Dialog visible={visibleMsg} onDismiss={() => setVisibleMsg(false)} theme={DarkTheme}>
                        {severity == "error" ? 
                            <Dialog.Title style={styles.textTitulo}>Ha ocurrido un error</Dialog.Title>
                        : null}
                        <Dialog.Content style={{ flexDirection: 'row', alignContent: 'space-between' }}>
                            <Paragraph style={styles.text}>{textoMsg}</Paragraph>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button theme={{ colors: {primary : colors.red}}} onPress={() => setVisibleMsg(false)}>Ok</Button>
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </Provider>
        </ImageBackground>
    )
}
export default IngresarReclamo;

const styles = StyleSheet.create({
    text: {
      color: "white",
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    card: {
        flex: 8,
        backgroundColor: colors.background,
        marginHorizontal: 10,
        marginVertical: 10,
        paddingVertical: 10,
        borderRadius: 10,
        borderWidth: 3,
        minWidth: "90%"
    },
    producto: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        backgroundColor: colors.backgroundColor,
        paddingHorizontal: 5,
    },
    textTitulo: {
        color: "white",
        fontSize: 18,
        fontWeight: 'bold'
    },
    textInput: {
        width: "100%",
        backgroundColor: "#ffffff00",
        fontSize: 15,
        marginBottom: 10
    },
    btn: {
        marginVertical: 10,
        marginHorizontal: 10,
        color: colors.white,
        backgroundColor: colors.red,
    },
    leftSurface: {
        flex: 1, 
        width: "90%", 
        minWidth: "90%", 
        justifyContent: 'center', 
        alignItems: 'flex-start',
        backgroundColor: colors.backgroundColor,
        padding: 15,
        elevation: 2,
        borderRadius: 2,
        marginBottom: 20,
    },
});