import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://ec2-3-89-140-231.compute-1.amazonaws.com:4567/pedido',
    headers: {'Access-Control-Allow-Origin': "*"}
  });

export default instance;