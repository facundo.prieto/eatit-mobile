import React, {useState, useEffect} from 'react';
import { View, Text, Dimensions, TouchableOpacity, StyleSheet, Image , ScrollView} from 'react-native';
import { Card, Provider, DarkTheme } from 'react-native-paper';
import { DeviceEventEmitter } from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../assets/colors/colors';
import { getSession, setPedidos } from '../components/auth/auth';
import axiosCliente from "../components/axios/AxiosCliente";

const {width,height} = Dimensions.get('screen');

const ListaPedidosActivos = ({navigation}) => {
    // console.log('ListaPedidosActivos')

    const [pedidosActivos, setPedidosActivos] = useState([]);

    useEffect(() => {
        // console.log('useEffect')
        let eventListenerPedidos = DeviceEventEmitter.addListener('actualizarPedidos',getListaPedidosActivos);
        getListaPedidosActivos();
        
        return function() {
            eventListenerPedidos.remove();
        };
    }, []);

    const getListaPedidosActivos = async () => {
        // console.log('getListaPedidosActivos')

        let session = await getSession();
        let email = "";
        if(session != null && session.email != null){
            email = session.email;
        }
        axiosCliente.get(`/pedidos/listar/${email}`)
        .then(function (response) {
            // console.log("response.data", response.data);

            let listaPedidosActivos = response.data.filter((item) => item.estado !== "RECHAZADO" && item.estado !== "FINALIZADO")
            setPedidosActivos(listaPedidosActivos);
            setPedidos(listaPedidosActivos)
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    const actualizarLista = () => {
        let eventListenerPedidos = DeviceEventEmitter.addListener('actualizarPedidos',getListaPedidosActivos);
        getListaPedidosActivos();
        eventListenerPedidos.remove();
    }

    const estadoTxt = (estado) => {
        let e = null;
        if(estado=='CREADO'){
            e = { txt: 'El restaurante está revisando tu pedido', loading:true}
        }else if(estado=='CONFIRMADO'){
            e = { txt: 'El restaurante está preparando tu orden', loading:true}
        }else if(estado=='EN_CAMINO'){
            e = { txt: 'Tu orden va en camino a tu dirección', loading:true}
        }else if(estado=='RECHAZADO'){
            e = { txt: 'Lo sentimos, el restaurante no podrá completar tu orden', loading:false}
        }else if(estado=='CANCELADO'){
            e = { txt: 'Lo sentimos, el restaurante no podrá completar tu orden', loading:false}
        }else if(estado=='ENTREGADO'){
            e = { txt: 'Esperemos disfrutes tu pedido', loading:false}
        }else if(estado=='FINALIZADO'){
            e = { txt: 'Esperemos hayas disfrutes tu orden', loading:false}
        }
        
        return e
    }

    // console.log('pedidosActivos', pedidosActivos)
    if(pedidosActivos && pedidosActivos.length > 0){
        return(
            <Provider>
                <Card style={styles.card}>
                    <Provider theme={DarkTheme} style={styles.container}>
                        <View style={styles.titleCard}>
                            <Text style={styles.text}>Pedidos activos:</Text>
                            <TouchableOpacity  onPress={actualizarLista} >
                                <MaterialCommunityIcons name="repeat" size={20} color={colors.white}/> 
                            </TouchableOpacity>
                        </View>
                        <View style={styles.listaPedidosContainer}>
                            <ScrollView horizontal={true}>
                                {pedidosActivos.map((item, index) =>
                                    <TouchableOpacity 
                                    onPress={() => navigation.push('DetallesPedidoHistorico', { pedido: item })} 
                                    key={index}
                                    style={styles.pedidoContainer}>
                                        <Card.Title style={{height: null, width: 80*width/100}}
                                            title={`${item.restauranteNombre}   $${item.monto}`}
                                            titleStyle={styles.titleStyle}
                                            subtitle={estadoTxt(item.estado).txt || ''}
                                            subtitleStyle={styles.subtitleStyle}
                                            left={(props) => <Image style={styles.tinyLogo} source={item.restauranteImage ? {uri: item.restauranteImage}: null}/>}
                                        />
                                    </TouchableOpacity>
                                )}
                            </ScrollView>
                        </View>
                    </Provider>
                </Card>
            </Provider>
        )

    }else{
        return null;
    }
}
export default ListaPedidosActivos;

const styles = StyleSheet.create({
    card: {
        backgroundColor: colors.background,
        margin: 10,
        borderRadius: 10,
        borderWidth: 3,
        padding:5
    },
    container: {
        backgroundColor: colors.background,
    },
    text: {
        color: colors.white,
        fontSize: 17,
        marginLeft: 10,
    },
    listaPedidosContainer: {
        height: null,
        alignItems: 'stretch',
        justifyContent: 'space-around',
        flexDirection: 'row',
    },
    titleCard: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginRight: 10,
    },
    pedidoContainer: {
        justifyContent: 'space-around',
        backgroundColor: colors.white,
        margin: 10,
        borderRadius: 10,
    },
    tinyLogo: {
        height:45, 
        width:45,
    },
    titleStyle: {
        color: colors.black,
        fontSize: 15
    },
    subtitleStyle: {
        color: colors.black,
        fontSize: 10
    },
});