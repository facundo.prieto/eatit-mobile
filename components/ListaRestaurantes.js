import React, {useState, useEffect} from 'react';
import { SafeAreaView, View, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { Searchbar, ActivityIndicator, Card, Button, Text, Dialog, Portal, Provider, DarkTheme, Chip, Paragraph  } from 'react-native-paper';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import Slider from '@react-native-community/slider';

import colors from '../assets/colors/colors';
// import ListaRestaurantesData from '../assets/data/ListaRestaurantesData';
// import Restaurante from '../assets/data/Restaurante';
// import CategoriasAlimentos from '../assets/data/CategoriasAlimentos';

import axiosRestaurante from "../components/axios/AxiosRestaurante";

import { getSession } from '../components/auth/auth'

const ListaRestaurantes = ({navigation, recargarlista}) => {
    const [listaCategorias, setListaCategorias] = useState([]);
    const [listaInicial, setListaInicial] = useState([]);
    const [listaFiltrada, setListaFiltrada] = useState([]);
    const [listaCerrados, setListaCerrados] = useState([]);
    const [listaCerrFiltrados, setCerradosFiltrados] = useState([]);
    const [selectedId, setSelectedId] = useState(null);
    const [token, setToken] = useState("");

    //Animaciónn de carga
    const [loading, setLoadingVisible] = useState(true);
    const setLoading = (load) => setLoadingVisible(load);

    useEffect(() => {
        // setListaFiltrada(Restaurante)
        // setLoading(false);
        getListaRestaurantes();
        return () =>{
        }
    }, []);

    const getListaRestaurantes = async () => {
        //datos de prueba
        // setListaInicial(ListaRestaurantesData)
        // setListaFiltrada(ListaRestaurantesData)
        // setListaCategorias(CategoriasAlimentos)
        
        setLoading(true);

        let loadtoken = "";
        if(token != ""){
            loadtoken = token;
        }else{
            let session = await getSession();
            if(session != null && session.token != null){
                loadtoken = session.token;
                setToken(session.token);
            }
        }
        axiosRestaurante.get(
            `/listar/full`,
        {},
        { headers: { Authorization: `${loadtoken}` } }
        )
        .then(function (response) {
            //console.log(response.data);

            let abiertos = response.data.filter((item) => item.abierto == true && item.estado === "ACTIVO")
            setListaInicial(abiertos);
            setListaFiltrada(abiertos);
            
            let cerrados = response.data.filter((item) => item.abierto == false && item.estado === "ACTIVO")
            setListaCerrados(cerrados);
            setCerradosFiltrados(cerrados);


            //cargo lista de categorias
            let listaCats = [];
            let activos = response.data.filter((item) => item.estado === "ACTIVO")
            activos.forEach(restaurante => {
                listaCats = listaCats.concat([...new Set(restaurante.categorias.map(x => x.descripcion))])
            });
            listaCats = [...new Set(listaCats)];
            setListaCategorias(listaCats)

            //calculo costo máximo para el slider
            let topePrecio = 0;
            activos.forEach(r => {
                if(r.costoEnvio > topePrecio){
                    topePrecio = r.costoEnvio
                }
            });
            setCostoTope(topePrecio)
            setCostoMaximo(topePrecio)

            setLoading(false);
        })
        .catch(function (error) {
            console.log(error);

            // setSeverity("error");
            // setMjeError(`Ocurrió un error en el sistema`);
            // setSnackbar(true);
            setLoading(false);
        });
    }

    const [recargarcont, setRecCont] = useState(0);
    
    if(recargarlista && recargarcont==0){
        getListaRestaurantes();
        setRecCont(1);
    }
    
    if(!recargarlista && recargarcont!=0){
        setRecCont(0);
    }

    //Barra de busqueda
    const [busqueda, setBusqueda] = useState('');
    const filtrarBusqueda = (texto) => {
        if(texto){
            const newData = listaInicial.filter((item) => {
                const itemData = item.nombre ? 
                                    item.nombre.toUpperCase() 
                                : ''.toUpperCase()
                const textData = texto.toUpperCase()
                return itemData.indexOf(textData) > -1;
            })
            setListaFiltrada(newData)

            let newCerrados = listaCerrados.filter((item) => {
                const itemData = item.nombre ? 
                                    item.nombre.toUpperCase() 
                                : ''.toUpperCase()
                const textData = texto.toUpperCase()
                return itemData.indexOf(textData) > -1;
            });
            setCerradosFiltrados(newCerrados)

            setBusqueda(texto)
        }else{
            setListaFiltrada(listaInicial)
            setCerradosFiltrados(listaCerrados)
            setBusqueda('')
        }
    }
    
    //Popup de filtros y ordenado
    const [popupVisible, setVisiblePopUp] = useState(false);
    const hidePopUp = () => setVisiblePopUp(false);
    
    //Ordenado
    const [orden, setOrden] = useState('');
    const [costoTope, setCostoTope] = useState(0);
    const [costoMaximo, setCostoMaximo] = useState(costoTope);
    const [categoriaSelected, setCategoriaSelected] = useState(null);
    
    const selecionarCat = (item) => {
        if(categoriaSelected == item){
            setCategoriaSelected(null)
        }else{
            setCategoriaSelected(item)
        }
    }

    const aplicarFiltros = () => {
        let newData = listaInicial;
        let newCerrados = listaCerrados;
        if(costoMaximo < costoTope){
            newData = listaInicial.filter((item) => item.costoEnvio < costoMaximo)
            newCerrados = listaCerrados.filter((item) => item.costoEnvio < costoMaximo)
        }
        if(categoriaSelected){
            newData = newData.filter(item => item.categorias.some(cat => cat.descripcion === categoriaSelected))
            newCerrados = newCerrados.filter(item => item.categorias.some(cat => cat.descripcion === categoriaSelected))
        }
        if(orden == 'calif'){
            newData = ordenarCalif(newData)
            newCerrados = ordenarCalif(newCerrados)
        }
        if(orden == 'costo'){
            newData = ordenarEnvio(newData)
            newCerrados = ordenarEnvio(newCerrados)
        }
        setListaFiltrada(newData)
        setCerradosFiltrados(newCerrados)

        setVisiblePopUp(false)
    }
    
    const ordenarCalif = (lista) => {
        let newData = lista;
        let direccion = 'desc'
        if(direccion === 'asc' || direccion === 'desc'){
            newData = lista.sort((a, b) => {
                let aCalif = 5
                if(a.calificacion) aCalif = a.calificacion
                let bCalif = 5
                if(b.calificacion) bCalif = b.calificacion
                const diff = aCalif - bCalif
                if(diff === 0) return 0;

                const sign = Math.abs(diff) / diff;

                return direccion === 'asc' ? sign : -sign;
            })
        }
        return newData;
    }
    
    const ordenarEnvio = (lista) => {
        let newData = lista;
        let direccion = 'asc'
        if(direccion === 'asc' || direccion === 'desc'){
            newData = lista.sort((a, b) => {
                const diff = a.costoEnvio - b.costoEnvio
                if(diff === 0) return 0;

                const sign = Math.abs(diff) / diff;

                return direccion === 'asc' ? sign : -sign;
            })
        }
        return newData;
    }

    //limpiarFiltros
    const limpiarFiltros = () => {
        setBusqueda('')
        setOrden('asc')
        setCategoriaSelected(null)
        setCostoMaximo(costoTope)
        setListaFiltrada(listaInicial)
        setCerradosFiltrados(listaCerrados)
        setVisiblePopUp(false)
    }

    
    const [visibleCerrado, setVisibleCerrado] = useState(false);
    const showCerrado = () => setVisibleCerrado(true);
    const hideCerrado = () => setVisibleCerrado(false);

    return(
        <View>
            <Provider>
                <Card style={styles.card}>
                    <SafeAreaView style={styles.container}>

                        <Provider theme={DarkTheme}>
                            <View style={styles.filtrosContainer}>
                                <View style={styles.filtroSearch}>
                                    <Searchbar
                                        placeholder="Buscar"
                                        onChangeText={filtrarBusqueda}
                                        value={busqueda}
                                    />
                                </View>
                                <TouchableOpacity onPress={() => setVisiblePopUp(true)} style={styles.filtroIcon}>
                                    <Ionicon name="options-outline" size={26} color={colors.white}/>
                                </TouchableOpacity>
                            </View>

                            {loading ? 
                                <ActivityIndicator style={{marginVertical: 10}} animating={true} color={colors.red} size="small"  />
                            : 
                                <View style={styles.listaContainer}>
                                    <ScrollView>
                                        {listaFiltrada.map((item, index) =>
                                            <TouchableOpacity 
                                            onPress={() => navigation.push('ListaMenusRestaurante', { restaurante: item })} 
                                            key={index} 
                                            style={styles.listaItem}>
                                                <Card.Title
                                                    title={item.nombre}
                                                    titleStyle={styles.titleStyle}
                                                    subtitle={`Envío $${item.costoEnvio}`}
                                                    subtitleStyle={styles.subtitleStyle}
                                                    left={(props) => <Image {...props} style={styles.tinyLogo} source={item.imagen ? {uri: item.imagen}: null }/>}
                                                    right={(props) => 
                                                        <View style={{flexDirection: 'row', marginRight:10, alignItems: 'center'}}>
                                                            {item.calificacion ? <Octicons name="star" size={12} color="green"/> : null}
                                                            <Text style={styles.itemCalif}> {item.calificacion ? item.calificacion : "NUEVO"}</Text>
                                                        </View>}
                                                />
                                            </TouchableOpacity>
                                        )}
                                    </ScrollView>
                                            
                                    {listaCerrFiltrados && listaCerrFiltrados.length > 0 ? 
                                        <View style={styles.listaContainer}>
                                            <Text style={styles.titleStyle}>Restaurantes cerrados:</Text>
                                            <ScrollView >
                                                {listaCerrFiltrados.map((item, index) =>
                                                    <TouchableOpacity 
                                                    onPress={showCerrado} 
                                                    key={index} 
                                                    style={styles.listaCerrado}>
                                                        <Card.Title
                                                            title={item.nombre}
                                                            titleStyle={styles.titleStyle}
                                                            subtitle={`Envío $${item.costoEnvio}`}
                                                            subtitleStyle={styles.subtitleStyle}
                                                            left={(props) => <Image {...props} style={styles.tinyLogo} source={item.imagen ? {uri: item.imagen}: null }/>}
                                                            right={(props) => 
                                                                <View style={{flexDirection: 'row', marginRight:10, alignItems: 'center'}}>
                                                                    <Text style={styles.itemCerrado}>CERRADO</Text>
                                                                </View>}
                                                        />
                                                    </TouchableOpacity>
                                                )}
                                            </ScrollView>
                                        </View>
                                    : null
                                    }
                                </View>
                            }
                        </Provider>
                    </SafeAreaView>
                </Card>
            </Provider>
              
            <Portal>
                {/* PopUp de filtros */}
                <Dialog visible={popupVisible} onDismiss={() => setVisiblePopUp(false)} style={styles.alert}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: 10, marginTop: 10}}>
                        <Text> </Text>
                        <TouchableOpacity onPress={() => limpiarFiltros()}> 
                            <Text >Quitar filtros</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{marginHorizontal: 5}}>
                        <Text style={styles.filterTitle}>Ordenar por:</Text>
                    </View>
                    <Dialog.Actions style={{justifyContent: 'center'}}>
                        <TouchableOpacity onPress={() => setOrden('costo')}> 
                            <Chip //icon="truck" 
                                style={orden=='costo'? {...styles.orden, backgroundColor: colors.red} : styles.orden}
                                icon={({ size, color }) => (<MaterialIcon name="delivery-dining" size={18} color={colors.white}/>)}
                                >Menor costo de envío</Chip>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => setOrden('calif')}>
                            <Chip //icon="star" 
                                style={orden=='calif'? {...styles.orden, backgroundColor: colors.red} : styles.orden}
                                icon={({ size, color }) => (<MaterialIcon name="star" size={15} color={colors.white}/>)}
                                >Mejor calificados</Chip>
                        </TouchableOpacity>
                    </Dialog.Actions>
                    
                    <View style={{ margin: 5}}>
                        <Text style={styles.filterTitle}>Filtrar por:</Text>
                    </View>
                    <Dialog.Actions style={{justifyContent: 'center'}}>
                        <View style={styles.row}>
                            <Text style={styles.costoext}> Costo de envío</Text>
                            <Slider 
                                value={costoMaximo}
                                maximumValue={costoTope}
                                minimumValue={0}
                                step={5}
                                minimumTrackTintColor={colors.red}
                                thumbTintColor={colors.red}
                                onSlidingComplete={(value) => setCostoMaximo(parseInt(value))}
                                style={styles.slider}
                            />
                            <Text style={styles.alertText} style={styles.costoMax}>{costoMaximo}</Text>
                        </View>
                    </Dialog.Actions>
                    <Dialog.Actions style={{justifyContent: 'center'}}>
                        <View style={styles.row}>
                            <Text style={styles.costoext}> Categoría:</Text>
                        </View>
                    </Dialog.Actions>
                    <ScrollView contentContainerStyle={styles.scrolllistaCates} horizontal>
                        <View style={styles.listaCates}>
                            <ScrollView horizontal={true}>
                                {listaCategorias.map((item, index) =>
                                    <TouchableOpacity 
                                    onPress={() => selecionarCat(item)} 
                                    key={index} 
                                    style={styles.cateItem}>
                                        {categoriaSelected && item==categoriaSelected ?
                                            <Chip mode='outlined' theme={{ colors: {surface : colors.red}}}>{item}</Chip>
                                        :
                                            <Chip mode='outlined' theme={{ colors: {surface : colors.background}}}>{item}</Chip>
                                        }
                                        
                                    </TouchableOpacity>
                                )}
                            </ScrollView>
                        </View>
                    </ScrollView>

                    <Button mode="contained" style={styles.btn} theme={{ colors: {primary : colors.red}}} onPress={() => aplicarFiltros()}> Aplicar </Button>
                </Dialog>

                <Dialog visible={visibleCerrado} onDismiss={hideCerrado} theme={DarkTheme}>
                    <Dialog.Content style={{alignItems: 'center'}}>
                        <Paragraph style={styles.cerradoText}>El restaurante está cerrado</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button theme={{ colors: {primary : colors.red}}} onPress={hideCerrado}>Ok</Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </View>
    )
}
export default ListaRestaurantes;

const styles = StyleSheet.create({
    card: {
        backgroundColor: colors.background,
        margin: 10,
        borderRadius: 10,
        borderWidth: 3,
        padding:5
    },
    text: {
      color: "white",
    },
    container: {
        flex: 1,
        backgroundColor: colors.background,
        marginHorizontal: 10,
    },
    filtrosContainer: {
        alignItems: 'center',
        alignContent: 'center',
        flexDirection: 'row',
        paddingVertical: 5,
    },
    filtroSearch: {
        flex: 9,
    },
    filtroIcon: {
        marginLeft: 5,
        alignItems: 'center',
        flex: 1,
    },
    listaContainer: {
        flex: 1,
    },
    listaItem: {
        backgroundColor: colors.white,
        marginVertical: 8,
        borderRadius: 10,
        padding: 5,
    },
    listaCerrado: {
        backgroundColor: "gray",
        marginVertical: 8,
        borderRadius: 10,
        padding: 5,
    },
    itemCalif: {
        color: "green",
        fontSize: 13
    },
    itemCerrado: {
        color: "black",
        fontSize: 13
    },
    titleStyle: {
        color: colors.black,
        fontSize: 18
    },
    subtitleStyle: {
        color: colors.black,
        fontSize: 12
    },
    tinyLogo: {
        height:45, 
        width:45,
    },
    btn: {
        marginTop: 20,
        // color: colors.white,
        // backgroundColor: colors.backBox,
        marginBottom: 20,
        marginHorizontal: 10
    },

    alert: {
        backgroundColor: colors.background,
    },
    alertText: {
        color: colors.white,
    },
    filterTitle: {
        margin: 5,
        color: colors.white,
        fontSize: 20,
    },
    cerradoText: {
        marginVertical: 20,
        color: colors.white,
        fontSize: 18,
    },
    orden: {
        // flexDirection: 'row', 
        // alignItems: 'center', 
        // marginHorizontal: 5, 
        // paddingHorizontal: 2, 
        // borderRadius: 15
    },
    row: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginHorizontal: 5
    },
    costoext: {
        flex: 3, 
        color: colors.white,
    },
    slider: {
        flex: 5, 
        marginHorizontal: 5, 
    },
    costoMax: {
        flex: 1, 
        color: colors.white,
    },
    scrolllistaCates: {
        // height: 200,
        // weight: 330,
    },
    listaCates: {
        margin: 2,
        flexWrap: 'wrap',
    },
    cateItem: {
        margin: 3,
        flexWrap: 'wrap',
    },
});