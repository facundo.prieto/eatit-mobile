import React, {useState, useEffect} from 'react';
import { StyleSheet, ScrollView, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { Card, Button, Text, List, TextInput, Provider, Surface, Paragraph, Dialog, Portal, RadioButton, DarkTheme, ActivityIndicator } from 'react-native-paper';

import Navbar from '../components/Navbar';
import colors from '../assets/colors/colors';
import { getSession, vaciarCarrito, agregarPedido } from '../components/auth/auth';
import axiosPedido from "../components/axios/AxiosPedido";

const RepetirPedido = ({route, navigation}) => {
    const { pedido } = route.params;
    // console.log('RepetirPedido')
    // console.log(pedido)

    //Animaciónn de carga
    const [loading, setLoading] = useState(false);
    
    //Mensaje
    const [visibleMsg, setVisibleMsg] = useState(false);
    const [textoMsg, setTextoMsg] = useState('');
    const [severity, setSeverity] = useState('');

    useEffect(() => {
        // console.log('useEffect')
        setLoading(false);
        getListaDirecciones();

        // console.log('pedido', pedido)
        if(pedido){
            setComentarios(pedido.comentarios);
            setListaMenus(pedido.menus);
            setCostoFinal(pedido.monto);
            getPedido();
        }
    }, []);
    
    const [listaMenus, setListaMenus] = useState([]);

    const [subtotal, setSubtotal] = useState(0);
    const [costoEnvio, setCostoEnvio] = useState(0);
    const [precioTotal, setCostoFinal] = useState(0);

    const [emailRestaurante, setEmailRestaurante] = useState('');

    const [comentarios, setComentarios] = useState('');
    const onChangeComentarios = comentarios => setComentarios(comentarios);
    
    const getPedido = async () => {
        
        let session = await getSession();
        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        // console.log('pedido', pedido)
        if(pedido && pedido.idPedido){
            
            axiosPedido.get(
                `/listar/${pedido.idPedido}`,
                {},
                {
                headers: { Authorization: `${token}` },
                }
            )
            .then(function (response) {
                setLoading(true);
                if (response.status == 200) {
                    // console.log(response.data);
                    
                    setComentarios(response.data.comentarios);
                    setListaMenus(response.data.menus);
                    setEmailRestaurante(response.data.emailrestaurante);
                    setCostoEnvio(response.data.costoEnvio);
                    setCostoFinal(response.data.monto);
                    setSubtotal(response.data.monto - response.data.costoEnvio);
                    setLoading(false);
                } else {
                    setSeverity("error");
                    setTextoMsg(response.message);
                    setLoading(false);
                }
            })
            .catch(function (error) {
                console.log(error);
                setLoading(false);
            });
        }
        setLoading(false);
    }
    
    const getListaDirecciones = async () => {
        let session = await getSession();
        if(session != null && session.direcciones != null){
            setDirecciones(session.direcciones);
        }
    }

    const [listaDirecciones, setDirecciones] = useState([]);
    const [expanded, setExpanded] = useState(false);
    const handlePress = () => setExpanded(!expanded);
    const [titleDireccion, setTitleDir] = useState('Seleccione una dirección');
    const [dirSelected, setDirSelected] = useState(null);

    const seleccionarDir = (dir) => {
        setTitleDir(dir.calle + " " + dir.numero);
        setDirSelected(dir);
        setExpanded(!expanded);
    }

    const [metodoPago, setMetodoPago] = useState('EFECTIVO');

    const crearPedido = async () => {
        setLoading(true);

        let session = await getSession();
        let email = "";
        if(session != null && session.email != null){
            email = session.email;
        }
        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        
        // console.log('crearPedido')
        // console.log(metodoPago)

        if(metodoPago == 'PAYPAL'){
            let menus = listaMenus.map(prod => prod.id)
            // console.log(menus)

            let pedidoData = {
                comentarios: comentarios,
                emailCliente: email,
                direccion: titleDireccion,
                emailRestaurante: emailRestaurante,
                estaPago: true,
                menus: menus,
                metodoPago: metodoPago,
                monto: precioTotal,
                tiempoEstimado: 0
            }
            // console.log("pedidoData", pedidoData)

            navigation.push('PayPalWebView', { pedido:pedidoData, token:token })

        }else{

            let menus = listaMenus.map(prod => prod.id)
            // console.log(menus)

            let pedidoData = {
                comentarios: comentarios,
                emailCliente: email,
                direccion: titleDireccion,
                emailRestaurante: emailRestaurante,
                estaPago: false,
                menus: menus,
                metodoPago: metodoPago,
                monto: precioTotal,
                tiempoEstimado: 0
            }
            // console.log("pedidoData", pedidoData)

            axiosPedido.post(`/crear/`, pedidoData, {
                headers: { Authorization: `${token}` },
            })
            .then(function (response) {
                // console.log("response")
                // console.log(response.data)
                if (response.status == 200) {
                    // console.log(response.data);
                    setSeverity("success");
                    setVisibleMsg(true);
                    setTextoMsg(`Estamos creando tu pedido `);
            
                    setComentarios('')
                    setTitleDir('Seleccione una dirección')
                    vaciarCarrito();
                    
                    //Armo un objeto con los detalles de los productos y lo seleccionado al confirmar pedido
                    let pedidoSave = {
                        ...pedidoData , 
                        idPedido: response.data.idPedido,
                        fecha: response.data.fecha,
                        tiempoEstimado: response.data.tiempoEstimado,
                    }

                    agregarPedido(pedidoSave).then(idLocal => {
                        setLoading(false);
                        setVisibleMsg(false);

                        // console.log("then")
                        // console.log(idLocal)
                        navigation.push('DetallesPedidoHistorico', { pedido:pedidoSave })
                    });
                } else {
                    setSeverity("error");
                    setVisibleMsg(true);
                    setTextoMsg(response.message);
                    // console.log(response.message);
                    setLoading(false);
                }
            })
            .catch(function (error) {
                if (error.response.status == 400) {
                    // Request made and server responded
                    setTextoMsg(error.response.data);
                    // console.log(error.response.data);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    setTextoMsg(error.message);
                    // console.log(error.mensage);
                }
                setSeverity("error");
                setVisibleMsg(true);
        
                setLoading(false);
            });
        }
    }

    return(
        <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
            <Provider>
                <Navbar navigation={navigation} ocultarDireccion={true} tuPedido={true}/>
                <Card style={styles.card}>
                    <ScrollView style={{maxHeight: "100%"}}>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20, marginTop: 20}}>
                            <Text style={styles.textTitulo}>Dirección</Text>
                            <Surface style={styles.listAccordionleft}>
                                <List.Section style={styles.listAccordion} >
                                    <List.Accordion
                                        title={titleDireccion}
                                        expanded={expanded}
                                        theme={{ colors: { text: colors.white, primary: colors.white, accent: colors.backBox, background: colors.background} }}
                                        onPress={handlePress}>
                                        {listaDirecciones && listaDirecciones.map((dir, index) =>
                                            <List.Item 
                                                style={styles.listItem} 
                                                titleStyle={styles.textDirDesc} 
                                                key={index} 
                                                onPress={() =>seleccionarDir(dir)}
                                                title={dir.calle + " " + dir.numero} />
                                        )}
                                    </List.Accordion>
                                </List.Section>
                            </Surface>
                        </View>

                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20}}>
                            <Text style={styles.textTitulo}>Método de pago</Text>
                        </View>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <Surface style={styles.leftSurface}>
                                <View style={{ flexDirection: 'row', alignContent: 'flex-start' }}>
                                    <View style={{ flex: 1 }}>
                                        <RadioButton
                                            value="EFECTIVO"
                                            color={colors.red}
                                            uncheckedColor="grey"
                                            theme={{ colors: { text: colors.white} }}
                                            status={ metodoPago === 'EFECTIVO' ? 'checked' : 'unchecked' }
                                            onPress={() => setMetodoPago('EFECTIVO')}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={() => setMetodoPago('EFECTIVO')} style={{ flex: 7, alignSelf: 'center' }}>
                                        <Text style={metodoPago === 'EFECTIVO' ? styles.textTitulo : styles.text}>Efectivo</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', alignContent: 'flex-start' }}>
                                    <View style={{ flex: 1 }}>
                                        <RadioButton
                                            value="PAYPAL"
                                            color={colors.red}
                                            uncheckedColor="grey"
                                            status={ metodoPago === 'PAYPAL' ? 'checked' : 'unchecked' }
                                            onPress={() => setMetodoPago('PAYPAL')}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={() => setMetodoPago('PAYPAL')} style={{ flex: 7, alignSelf: 'center' }}>
                                        <Text style={metodoPago === 'PAYPAL' ? styles.textTitulo : styles.text}>PayPal</Text>
                                    </TouchableOpacity>
                                </View>
                            </Surface>
                        </View>


                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20}}>
                            <Text style={styles.textTitulo}>Resumen</Text>
                        </View>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <Surface style={styles.leftSurface}>

                                <View style={styles.restaurante}>
                                    {pedido.restauranteImage  && pedido.restauranteImage != "" ?
                                        <Image style={styles.imagen} source={{uri: pedido.restauranteImage}}/>
                                    : null }
                                    <View>
                                        <Text style={styles.textTitulo}>{pedido.restauranteNombre}</Text>
                                    </View>
                                </View>

                                <View style={styles.monto}>
                                    <Text style={styles.textPrecio}>Subtotal</Text>
                                    <Text style={styles.textPrecio}>$ {subtotal}</Text>
                                </View>
                                <View style={styles.monto}>
                                    <Text style={styles.textPrecio}>Envío</Text>
                                    <Text style={styles.textPrecio}>$ {costoEnvio}</Text>
                                </View>

                                <View style={styles.totalContainers}>
                                    <View style={styles.montoTotal}>
                                        <Text style={styles.textTotal}>Total</Text>
                                        <Text style={styles.textTotal}>$ {precioTotal}</Text>
                                    </View>
                                </View>
                            </Surface>
                        </View>


                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20}}>
                            <Text style={styles.textTitulo}>Comentarios</Text>
                        </View>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                <View style={{width:"90%"}}>
                                    <TextInput dense={false} mode="flat" label="Comentarios" value={comentarios} 
                                        onChangeText={onChangeComentarios} 
                                        style={styles.textInput} 
                                        theme={{ colors: { text: colors.white, primary: colors.white} }}
                                    />
                                </View>
                            </Surface>
                        </View>
                    </ScrollView>
                </Card>

            
                <Button mode="contained" style={styles.btn} onPress={() => crearPedido()} 
                    disabled={!pedido || listaMenus.length == 0 || dirSelected==null || loading} 
                    loading={loading}
                >Realizar pedido</Button>

                <Portal>
                    <Dialog visible={visibleMsg} onDismiss={() => setVisibleMsg(false)} theme={DarkTheme}>
                        {severity == "error" ? 
                            <Dialog.Title style={styles.textTitulo}>Ha ocurrido un error</Dialog.Title>
                        : 
                            null // <Dialog.Title style={styles.textTitulo}>Genial!</Dialog.Title>
                        }
                        <Dialog.Content style={{ flexDirection: 'row', alignContent: 'space-between' }}>
                            <Paragraph style={styles.text}>{textoMsg}</Paragraph>
                            {severity == "success" ? 
                                <ActivityIndicator style={{marginHorizontal: 10}} animating={true} color={colors.red} size="small"  />
                            : 
                                null
                            }
                        </Dialog.Content>
                        <Dialog.Actions>
                            {severity == "error" ? 
                                <Button theme={{ colors: {primary : colors.red}}} onPress={() => setVisibleMsg(false)}>Ok</Button>
                            : 
                                null
                            }
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </Provider>
        </ImageBackground>
    )
}
export default RepetirPedido;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    card: {
        flex: 1,
        backgroundColor: colors.background,
        marginHorizontal: 10,
        borderRadius: 10,
        borderWidth: 3,
        minHeight: "70%"
    },
    restaurante:{
        flex:1, 
        justifyContent: 'flex-start', 
        alignItems: 'center', 
        flexDirection: 'row', 
        marginHorizontal: 10,
        marginVertical: 5,
    },
    imagen: {
        height:40, 
        width:40, 
        marginRight: 5,
        borderRadius: 5,
    },
    text: {
        color: "white",
        fontSize: 14,
    },
    textTitulo: {
        color: "white",
        fontSize: 18,
        fontWeight: 'bold'
    },
    textPrecio: {
        color: "white",
        fontSize: 16,
    },
    textTotal: {
        color: "white",
        fontSize: 17,
        fontWeight: 'bold'
    },
    textProductos: {
        marginVertical: 10,
        color: "white",
        fontSize: 16
    },
    textPrecioOg: {
        color: colors.red,
        fontSize: 12,
        textDecorationLine: 'line-through'
    },
    textDescuento: {
        color: "white",
        fontSize: 12,
    },
    left: {
        flex: 1, 
        width: "90%", 
        minWidth: "90%", 
        justifyContent: 'center', 
        alignItems: 'flex-start',
        backgroundColor: colors.backgroundColor,
    },
    listAccordionleft: {
        flex: 1, 
        width: "100%", 
        minWidth: "100%", 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: colors.backgroundColor,
        paddingHorizontal: 5,
        elevation: 2,
        borderRadius: 2,
        marginBottom: 20,
    },
    leftSurface: {
        flex: 1, 
        width: "90%", 
        minWidth: "90%", 
        justifyContent: 'center', 
        alignItems: 'flex-start',
        backgroundColor: colors.backgroundColor,
        padding: 5,
        elevation: 2,
        borderRadius: 2,
        marginBottom: 20,
    },
    sinItems: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: colors.backgroundColor,
        padding: 10,
        elevation: 2,
        borderRadius: 5,
        margin: 5,
    },
    right: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'flex-end'
    },
    precioTotal: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'flex-end',
        padding: 10,
    },
    center: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    listaProductos: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'center', 
        alignItems: 'center',
        padding: 5,
    },
    producto: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        backgroundColor: colors.backgroundColor,
        paddingHorizontal: 10,
    },
    listAccordion: {
        flex: 1, 
        width: "100%", 
    },
    listItem: {
        margin: 5,
        backgroundColor: colors.backBox,
    },
    textDirDesc: {
        color: colors.white,
        fontSize: 15,
    },
    monto: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        backgroundColor: colors.backgroundColor,
        paddingHorizontal: 10,
    },
    totalContainers: {
        flex: 1, 
        width: "100%", 
        alignItems: 'center', 
        paddingHorizontal: 10,
    },
    montoTotal: {
        flex: 1, 
        width: "100%", 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        flexDirection: 'row',
        paddingTop: 3,
        marginTop: 3,
        borderTopColor: colors.white,
        borderTopWidth: 0.5,
    },
    btn: {
        marginVertical: 10,
        marginHorizontal: 10,
        color: colors.white,
        backgroundColor: colors.red,
    },
    textInput: {
        width: "100%",
        backgroundColor: "#ffffff00",
        fontSize: 15,
        marginBottom: 10
    },
    cantBtn: {
        marginHorizontal: 10,
    },
    vaciarCarrito: {
        marginVertical: 10,
    },
    vaciarCarritoText: {
        color: colors.red,
        fontSize: 16,
        textDecorationLine: 'underline',
    },
});