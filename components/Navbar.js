import React, {useState, useEffect} from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Card, Provider } from 'react-native-paper';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { getSession } from '../components/auth/auth'

import colors from '../assets/colors/colors';

const Navbar = ({navigation, ocultarGoBack, ocultarDireccion, tuPedido}) => {
    // console.log('Navbar')
    // console.log(navigation)
    const [direccion, setDireccion] = useState('');
    
    const getDireccion = async () => {
        let session = await getSession();
        // console.log('getDireccion');
        // console.log(session)
        if(session != null && session.direcciones != null && session.direcciones.length > 0){
            setDireccion(session.direcciones[0].calle + " " + session.direcciones[0].numero);
        }
    };

    useEffect(() => {
        getDireccion();
        return () =>{
        }
    }, []);

    return(
        <Provider>
            <Card style={styles.card}>
                <View style={styles.container}>
                    {ocultarGoBack ?
                        null // <Text> </Text>
                    : 
                        <TouchableOpacity style={styles.gobackContainer} onPress={() => navigation.goBack()}>
                            <MaterialIcon name="chevron-left" size={40} color={colors.white}/>
                            {/* <Text style={{color: colors.white, underline: colors.white}}>Volver</Text> */}
                        </TouchableOpacity>
                    }
                    {tuPedido ?
                        <View  style={styles.directionContainer}>
                            <Text style={styles.textTitulo}>Tu pedido</Text>
                        </View>
                    : 
                        null // <Text> </Text>
                    }
                    {ocultarDireccion ?
                        null // <Text> </Text>
                    : 
                        <TouchableOpacity style={styles.directionContainer} onPress={() => navigation.push('Direcciones')}>
                            <MaterialIcon name="home" size={22} color={colors.white}/>
                            <Text style={styles.directionText}>{direccion}</Text>
                        </TouchableOpacity>
                    }
                </View>
            </Card>
        </Provider>
    )
}
export default Navbar;

const styles = StyleSheet.create({
    card: {
        backgroundColor: colors.background,
        margin: 10,
        borderRadius: 10,
        borderWidth: 3,
        padding:5
    },
    container: {
        //height: 50,
        backgroundColor: colors.background,
        justifyContent: 'space-between', 
        alignItems: 'center',
        flexDirection: 'row',
    },
    gobackContainer: {
        height: 40,
        //bbackgroundColor: colors.red,
        alignItems: 'center',
        justifyContent: 'center', 
        // paddingHorizontal: 10,
        // paddingBottom: 10,
        // flexDirection: 'row',
    },
    directionContainer: {
        height: 40,
        //backgroundColor: colors.red,
        alignItems: 'flex-end',
        justifyContent: 'center', 
        paddingHorizontal: 10,
        paddingBottom: 10,
        flexDirection: 'row',
    },
    directionText: {
        color: colors.white, 
        fontSize:16, 
        paddingHorizontal: 5
    },
    textTitulo: {
        color: "white",
        fontSize: 18,
        paddingHorizontal: 10,
        fontWeight: 'bold', 
    },
});