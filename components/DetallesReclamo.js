import React, {useState, useEffect} from 'react';
import { StyleSheet, ScrollView, View, ImageBackground } from 'react-native';
import { Card, Text, Provider, Surface, ActivityIndicator } from 'react-native-paper';

import Navbar from '../components/Navbar';
import colors from '../assets/colors/colors';
import { getSession, getPedido} from '../components/auth/auth';
import axiosReclamo from "../components/axios/AxiosReclamo";

const DetallesReclamo = ({route, navigation}) => {
    const { idLocal, pedido } = route.params;
    // console.log("idLocal", idLocal)
    // console.log("pedido", pedido)

    const [state, setState] = useState({
        numeroPedido: "",
        descripcion: "",
        fecha: "",
        numeroReclamo: "",
        estadoReclamo: "",
    });

    const [motivo, setMotivo] = useState(true);

    const [loading, setLoadingVisible] = useState(true);
    const setLoading = (load) => setLoadingVisible(load);

    const [pedidoState, setPedido] = useState(null);

    useEffect(() => {
        setLoading(true);
        getPedidoBack();
        
    }, []);

    const getPedidoBack = async () => {

        if(idLocal){
            let storedPedido = await getPedido(idLocal);
             if(storedPedido != null){
                setPedido(storedPedido);
                setLoading(false);
            }
        }else if(pedido){
            setPedido(pedido);
            setLoading(false);
        }
    }

    useEffect(() => {
        setLoading(true);
        detalleReclamo();
        
    }, [pedidoState]);

    const detalleReclamo = async () => {
        setLoading(true);

        let session = await getSession();

        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        let email = "";
        if(session != null && session.email != null){
            email = session.email;
        }
        
        //{"cliente": "Tatiana.mathon@gmail.com", "descripcion": "[Problema con el restaurante]4564556 ", "estadoReclamo": "NO_RESUELTO", "fecha": "03/11/2021 02:12:53", "numeroPedido": 1431, "numeroReclamo": 1432}
        axiosReclamo
          .get(`/pedido/${pedidoState.idPedido}`)
          .then(function (response) {
            if (response.status == 200) {
 
                let strMotivo = response.data.descripcion.split("[");
                let motivo = strMotivo[1].split("] ");
                setMotivo(motivo[0]);
                
                setState({
                    numeroPedido: response.data.numeroPedido,
                    descripcion: motivo[1],
                    fecha: response.data.fecha,
                    numeroReclamo: response.data.numeroReclamo,
                    estadoReclamo: response.data.estadoReclamo,
                });
            } else {
                setSeverity("error");
                setVisibleMsg(true);
                setTextoMsg(response.message);
            }
            setLoading(false);
          })
          .catch(function (error) {
            setSeverity("error");
            setVisibleMsg(true);
            setLoading(false);
          });
      };

    return(
        <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
            <Provider>
                <Navbar navigation={navigation} ocultarDireccion={true}/>
                <Card style={styles.card}>
                    <ScrollView>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 10}}>
                            <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                <View style={styles.producto}>
                                    <Text style={styles.textTitulo}> Reclamo #{state.numeroReclamo} </Text>
                                </View>
                            </Surface>

                            <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                <View style={{paddingHorizontal: 5,width:"100%"}}>
                                    <Text style={styles.textTitulo}>Pedido</Text>
                                    <Text style={styles.text}> # {state.numeroPedido} </Text>
                                </View>
                            </Surface>

                            <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                <View style={{paddingHorizontal: 5,width:"100%"}}>
                                    <Text style={styles.textTitulo}>Motivo</Text>
                                    <Text style={styles.text}> {motivo} </Text>
                                </View>
                            </Surface>

                            <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                <View style={{paddingHorizontal: 5,width:"100%"}}>
                                    <Text style={styles.textTitulo}>Comentarios</Text>
                                    <Text style={styles.text}> {state.descripcion} </Text>
                                </View>
                            </Surface>

                            <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                <View style={{paddingHorizontal: 5,width:"100%"}}>
                                    <Text style={styles.textTitulo}>Fecha ingresado</Text>
                                    <Text style={styles.text}> {state.fecha}</Text>
                                </View>
                            </Surface>

                            <Surface style={{...styles.leftSurface, justifyContent: 'center', alignItems: 'center'}}>
                                <View style={{paddingHorizontal: 5,width:"100%"}}>
                                    <Text style={styles.textTitulo}>Estado del Reclamo</Text>
                                    {state.estadoReclamo == 'ABIERTO' ?
                                        <Text style={styles.text}> Abierto </Text>
                                    : state.estadoReclamo == 'RESUELTO' ?
                                        <Text style={styles.text}> Resuelto </Text>
                                    : state.estadoReclamo == 'NO_RESUELTO' ?
                                        <Text style={styles.text}> No resuelto </Text>
                                    : null}
                                </View>
                            </Surface>
                        </View>
                    </ScrollView>
                </Card>
            </Provider>
        </ImageBackground>
    )
}
export default DetallesReclamo;

const styles = StyleSheet.create({
    text: {
        color: "white",
      },
      container: {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
      },
      card: {
          flex: 8,
          backgroundColor: colors.background,
          marginHorizontal: 10,
          marginVertical: 10,
          paddingVertical: 10,
          borderRadius: 10,
          borderWidth: 3,
          minWidth: "90%"
      },
      producto: {
          flex: 1, 
          width: "100%", 
          justifyContent: 'space-between', 
          alignItems: 'center', 
          flexDirection: 'row',
          backgroundColor: colors.backgroundColor,
          paddingHorizontal: 5,
      },
      textTitulo: {
          color: "white",
          fontSize: 18,
          fontWeight: 'bold'
      },
      leftSurface: {
          flex: 1, 
          width: "90%", 
          minWidth: "90%", 
          justifyContent: 'center', 
          alignItems: 'flex-start',
          backgroundColor: colors.backgroundColor,
          padding: 15,
          elevation: 2,
          borderRadius: 2,
          marginBottom: 20,
      },
  });