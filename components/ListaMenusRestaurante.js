import React, {useState, useEffect} from 'react';
import { RefreshControl, ImageBackground, SafeAreaView, View, ScrollView, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { Searchbar, ActivityIndicator, Card, Button, Text, Dialog, Portal, Provider, DarkTheme, Chip  } from 'react-native-paper';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import Slider from '@react-native-community/slider';
import MapView, { PROVIDER_GOOGLE , Marker, Circle} from 'react-native-maps';

import Navbar from '../components/Navbar';

import axiosRestaurante from "../components/axios/AxiosRestaurante";

import { getSession } from '../components/auth/auth';
import colors from '../assets/colors/colors';

// import CategoriasAlimentos from '../assets/data/CategoriasAlimentos';

const ListaMenusRestaurante = ({route, navigation}) => {
    const { restaurante } = route.params;
    //console.log(restaurante)
    
    const [listaCategorias, setListaCategorias] = useState([]);
    const [listaInicial, setListaInicial] = useState([]);
    const [listaFiltrada, setListaFiltrada] = useState([]);
    const [selectedId, setSelectedId] = useState(null);
    const [token, setToken] = useState("");

    //Animaciónn de carga
    const [loading, setLoadingVisible] = useState(true);
    const setLoading = (load) => setLoadingVisible(load);

    //Popup de detalles
    const [popupDetalles, setVisibleDetalles] = useState(false);
    const hideDetalles = () => setVisibleDetalles(false);

    //Ubicacion
    const [longitud, setLongitud] = useState(0);
    const [latitud, setLatitud] = useState(0);

    const  convertCoordinates = (lon, lat) => {
        // console.log(lon)
        // console.log(lat)

        // //Convertir EPSG:4326 a EPSG:3857
        // var x = (lon * 20037508.34) / 180;
        // var y = Math.log(Math.tan(((90 + lat) * Math.PI) / 360)) / (Math.PI / 180);
        // y = (y * 20037508.34) / 180;
        // console.log(x)
        // console.log(y)

        //Convertir EPSG:3857 a EPSG:4326
        if(lat > 1000 || lat<(-1000)){
            let long4326 = (lat*180)/20037508.34 
            let lat4326 = Math.atan(Math.exp(lon * Math.PI / 20037508.34)) * 360 / Math.PI - 90; 
            
            // console.log('lat > 1000 || lat<(-1000)')
            // console.log(long4326)
            // console.log(lat4326)
    
            setLongitud(long4326)
            setLatitud(lat4326) 
        }else{
            // console.log('else')
            // console.log(lon)
            // console.log(lat)
    
            setLongitud(lon)
            setLatitud(lat)
        }
    }
    
    const [promociones, setPromociones] = useState([]);
    const [mostrarPromos, setMostrarPromos] = useState(false);

    useEffect(() => {
        //convierto coordenadas    
        convertCoordinates(Number(restaurante.ubicacion.longitud), Number(restaurante.ubicacion.latitud));

        //cargo lista de menus
        if(restaurante.menu == null){
            getListaMenus();
        }else{
            //calculo costo máximo para el slider
            let topePrecio = 0;
            let promos = [];
            restaurante.menu.forEach(menu => {
                if(menu.precio > topePrecio){
                    topePrecio = menu.precio
                }
                if (menu.promocion) {
                    promos.push(menu);
                    setPromociones(promos);
                    setMostrarPromos(true);
                }
            });
            setCostoTope(topePrecio)
            setCostoMaximo(topePrecio)

            //cargo lista de categorias
            let listaCats = [];
            listaCats =[...new Set(restaurante.categorias.map(x => x.descripcion))]
            setListaCategorias(listaCats)


            setListaInicial(restaurante.menu)
            setListaFiltrada(restaurante.menu)
            setLoading(false);
        }
        
        return () =>{
        }
    }, []);

    const getListaMenus = async () => {
        setLoading(true);

        let loadtoken = "";
        if(token != ""){
            loadtoken = token;
        }else{
            let session = await getSession();
            if(session != null && session.token != null){
                loadtoken = session.token;
                setToken(session.token);
            }
        }

        axiosRestaurante.get(
            `/buscar/full/${restaurante.email}`,
        {},
        { headers: { Authorization: `${loadtoken}` } }
        )
        .then(function (response) {
            setListaInicial(response.data.menu);
            setListaFiltrada(response.data.menu);

            //calculo costo máximo para el slider
            let topePrecio = 0;
            let promos = [];
            response.data.menu.forEach(menu => {
                if(menu.precio > topePrecio){
                    topePrecio = menu.precio
                }
                if (menu.promocion) {
                    promos.push(menu);
                    setPromociones(promos);
                    setMostrarPromos(true);
                }
            });
            setCostoTope(topePrecio)
            setCostoMaximo(topePrecio)

            //cargo lista de categorias
            let listaCats = [];
            listaCats =[...new Set(response.data.categorias.map(x => x.descripcion))]
            setListaCategorias(listaCats)

            setLoading(false);
        })
        .catch(function (error) {
            console.log(error);

            // setSeverity("error");
            // setMjeError(`Ocurrió un error en el sistema`);
            // setSnackbar(true);
            setLoading(false);
        });
        setLoading(false);
    }

    //Barra de busqueda
    const [busqueda, setBusqueda] = useState('');
    const filtrarBusqueda = (texto) => {
        if(texto){
            const newData = listaInicial.filter((item) => {
                if(item.titulo.toLowerCase().includes(texto.toLowerCase()) 
                || item.descripcion.toLowerCase().includes(texto.toLowerCase())){
                    return item;
                }
            })
            setListaFiltrada(newData)
            setBusqueda(texto)
        }else{
            setListaFiltrada(listaInicial)
            setBusqueda('')
        }
    }

    let filteredListPromos = promociones.filter(
        (item) =>
            item.titulo.toLowerCase().includes(busqueda) ||
            item.titulo.includes(busqueda) ||
            item.descripcion.toLowerCase().includes(busqueda) ||
            item.descripcion.includes(busqueda)
    );
    
    //Popup de filtros
    const [popupVisible, setVisiblePopUp] = useState(false);
    const hidePopUp = () => setVisiblePopUp(false);
    const [costoTope, setCostoTope] = useState(0);
    const [costoMaximo, setCostoMaximo] = useState(costoTope);
    const [categoriaSelected, setCategoriaSelected] = useState(null);

    const selecionarCat = (item) => {
        if(categoriaSelected == item){
            setCategoriaSelected(null)
        }else{
            setCategoriaSelected(item)
        }
    }

    const aplicarFiltros = (item) => {
        let newData = listaInicial;
        if(costoMaximo < costoTope){
            newData = listaInicial.filter((item) => item.precio < costoMaximo)
        }
        if(categoriaSelected){
            newData = newData.filter(item => item.categorias.some(cat => cat.descripcion === categoriaSelected))
        }
        setListaFiltrada(newData)
        setVisiblePopUp(false)
    }

    //limpiarFiltros
    const limpiarFiltros = () => {
        setBusqueda('')
        setCategoriaSelected(null)
        setCostoMaximo(costoTope)
        setListaFiltrada(listaInicial)
    }

    const [refreshing, setRefreshing] = useState(false);
  
    const onRefresh = React.useCallback(() => {
      setRefreshing(true);
      getListaMenus().then(() => setRefreshing(false));
    }, []);

    return(
        <ImageBackground style={styles.fondocontainer} source={require('../assets/images/fondo.jpg')}>
            <ScrollView
                refreshControl={
                    <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                    />
                }
            >
                <Navbar navigation={navigation}/>
                
                <Provider>
                    <Card style={styles.card}>
                        <View style={styles.row}>
                            {restaurante.imagen ? 
                                <TouchableOpacity onPress={() => setVisibleDetalles(true)}>
                                    <Image style={styles.restauranteLogo} source={restaurante.imagen ? {uri: restaurante.imagen}: null}/>
                                </TouchableOpacity>
                            : null}
                            <View style={styles.colDetalles}>
                                <View style={styles.rowTitle}>
                                    <Text style={styles.restauranteNombre}>{restaurante.nombre}</Text>
                                    <TouchableOpacity style={styles.infoTouch} onPress={() => setVisibleDetalles(true)}>
                                        <Octicons name="info" size={17} color={colors.white}/> 
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.rowTitle}>
                                    <Text style={styles.text}>Envío $ {restaurante.costoEnvio}     </Text>
                                    {restaurante.calificacion != 0 ? 
                                        <View style={styles.rowCali}>
                                            <Octicons name="star" size={13} color="green"/>
                                            <Text style={styles.textCaliRestaurante}>{restaurante.calificacion}   </Text>
                                        </View>
                                    : 
                                        <View style={styles.rowCali}>
                                            <Text style={styles.textCali}>NUEVO</Text>
                                        </View>
                                    }
                                </View>
                                <View style={styles.contAceptaPedidos}>
                                    {!restaurante.abierto ? 
                                        <Text style={styles.textCerrado}>CERRADO</Text>
                                    : null}
                                    {/* {restaurante.abierto && !restaurante.aceptaPedidos ? 
                                        <View style={styles.contAceptaPedidos}>
                                            <Text style={styles.textAceptaPedidos}>El restaurante no está aceptando</Text>
                                            <Text style={styles.textAceptaPedidos}>pedidos de momento</Text>
                                        </View>
                                    : null} */}
                                </View>
                            </View>
                        </View>
                    </Card>
                </Provider>

                {mostrarPromos ? 
                    <View style={styles.listaPromosContainer}>
                        <Text style={{...styles.restauranteNombre, marginLeft:10}}>Promociones</Text>
                        <ScrollView horizontal={true}>
                            {filteredListPromos.map((item, index) =>
                                <TouchableOpacity 
                                onPress={() => navigation.push('Menu', { restaurante:restaurante, menu: item, usaPromo: true })} 
                                key={index} 
                                style={styles.promoContainer}>
                                    <Card.Title
                                        title={item.promocion.descripcion}
                                        titleStyle={styles.titleStyle}
                                        subtitle={item.titulo}
                                        subtitleStyle={styles.subtitleStyle}
                                        left={(props) => <Image {...props} style={styles.tinyLogo} source={item.imagen ? {uri: item.imagen}: null}/>}
                                        right={(props) => 
                                            <View style={{marginRight:10, flexDirection: 'row', alignItems: 'center'}}>
                                                <Text style={styles.descuento}>{`$${item.precio}`}</Text>
                                                <Text style={styles.precio}> {`$${item.precio - (item.precio * item.promocion.descuento /100)}`}</Text>
                                            </View>}
                                    />
                                </TouchableOpacity>
                            )}
                        </ScrollView>
                    </View>
                : null}
                        
                <Provider>
                    <Card style={styles.card}>
                        <SafeAreaView style={styles.container}>

                            <Provider theme={DarkTheme}>
                                <View style={styles.filtrosContainer}>
                                    <View style={styles.filtroSearch}>
                                        <Searchbar
                                            placeholder="Buscar"
                                            onChangeText={filtrarBusqueda}
                                            value={busqueda}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={() => setVisiblePopUp(true)} style={styles.filtroIcon}>
                                        <Ionicon name="options-outline" size={26} color={colors.white}/>
                                    </TouchableOpacity>
                                </View>

                                {loading ? 
                                    <ActivityIndicator style={styles.loading} animating={true} color={colors.red} size="large"  />
                                : 
                                    <View style={styles.listaContainer}>
                                        <ScrollView >
                                            {listaFiltrada.map((item, index) =>
                                                <TouchableOpacity 
                                                onPress={() => navigation.push('Menu', { restaurante: restaurante, menu: item })} 
                                                key={index} 
                                                style={styles.listaItem}>
                                                    <Card.Title
                                                        title={item.titulo}
                                                        titleStyle={styles.titleStyle}
                                                        subtitle={item.descripcion}
                                                        subtitleStyle={styles.subtitleStyle}
                                                        left={(props) => <Image {...props} style={styles.tinyLogo} source={item.imagen ? {uri: item.imagen}: null}/>}
                                                        right={(props) => 
                                                            <View style={{marginRight:10,}}>
                                                                <Text style={styles.precio}> {`$${item.precio}`}</Text>
                                                            </View>}
                                                    />
                                                </TouchableOpacity>
                                            )}
                                        </ScrollView>
                                    </View>
                                }
                            </Provider>
                        </SafeAreaView>
                    </Card>
                </Provider>
                
                <Portal>
                    {/* PopUp de filtros */}
                    <Dialog visible={popupVisible} onDismiss={() => setVisiblePopUp(false)} style={styles.alert}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: 10, marginTop: 10}}>
                            <Text> </Text>
                            <TouchableOpacity onPress={() => limpiarFiltros()}> 
                                <Text >Quitar filtros</Text>
                            </TouchableOpacity>
                        </View>
                        
                        <View style={{ margin: 5}}>
                            <Text style={styles.filterTitle}>Filtrar por:</Text>
                        </View>
                        <Dialog.Actions style={{justifyContent: 'center'}}>
                            <View style={styles.row}>
                                <Text style={styles.costoext}> Precio</Text>
                                <Slider 
                                    value={costoMaximo}
                                    maximumValue={costoTope}
                                    minimumValue={0}
                                    step={10}
                                    minimumTrackTintColor={colors.red}
                                    thumbTintColor={colors.red}
                                    onSlidingComplete={(value) => setCostoMaximo(parseInt(value))}
                                    style={styles.slider}
                                />
                                <Text style={styles.alertText} style={styles.costoMax}>{costoMaximo}</Text>
                            </View>
                        </Dialog.Actions>
                        <Dialog.Actions style={{justifyContent: 'center'}}>
                            <View style={styles.row}>
                                <Text style={styles.costoext}> Categoría:</Text>
                            </View>
                        </Dialog.Actions>
                        <ScrollView contentContainerStyle={styles.scrolllistaCates} horizontal>
                            <View style={styles.listaCates}>
                                <ScrollView horizontal={true}>
                                    {listaCategorias.map((item, index) =>
                                    <TouchableOpacity 
                                    onPress={() => selecionarCat(item)} 
                                    key={index} 
                                    style={styles.cateItem}>
                                        {categoriaSelected && item==categoriaSelected ?
                                            <Chip mode='outlined' theme={{ colors: {surface : colors.red}}}>{item}</Chip>
                                        :
                                            <Chip mode='outlined' theme={{ colors: {surface : colors.background}}}>{item}</Chip>
                                        }
                                        
                                    </TouchableOpacity>
                                    )}
                                </ScrollView>
                            </View>
                        </ScrollView>
                        
                        <Button mode="contained" style={styles.btn} theme={{ colors: {primary : colors.red}}} onPress={() => aplicarFiltros()}> Aplicar </Button>
                    </Dialog>
                    
                    {/* PopUp de detalles */}
                    <Dialog visible={popupDetalles} onDismiss={() => setVisibleDetalles(false)} style={styles.alert}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: 10, marginVertical: 10}}>
                            <Text style={styles.filterTitle}>Información</Text>
                            <TouchableOpacity style={styles.infoTouch} onPress={() => setVisibleDetalles(false)}> 
                                <Text >X</Text>
                            </TouchableOpacity>
                        </View>

                        <ScrollView>
                            <View style={{...styles.row, marginHorizontal:15}}>
                                {restaurante.imagen ? 
                                    <Image style={styles.detallesLogo} source={restaurante.imagen ? {uri: restaurante.imagen}: null}/>
                                : null}
                                <View style={styles.colDetalles}>
                                    <Text style={styles.restauranteNombre}>{restaurante.nombre}</Text>
                                    <Text style={styles.text}>Envío $ {restaurante.costoEnvio}     </Text>
                                    {restaurante.calificacion != 0 ? 
                                        <View style={styles.rowCali}>
                                            <Octicons name="star" size={13} color="green"/>
                                            <Text style={styles.textCaliRestaurante}>{restaurante.calificacion}   </Text>
                                        </View>
                                    : 
                                        <View style={styles.rowCali}>
                                            <Text style={styles.textCali}>NUEVO</Text>
                                        </View>
                                    }
                                </View>
                            </View>

                            <View style={{ marginHorizontal: 15,  marginVertical: 10}}>
                                <Text style={styles.text}>Email de contacto:</Text>
                                <Text style={styles.text}>{restaurante.email}</Text>
                            </View>

                            <View style={{ marginHorizontal: 15,  marginVertical: 10}}>
                                <Text style={styles.text}>Dirección: </Text>
                                <Text style={styles.text}>{restaurante.direccion.calle} {restaurante.direccion.numero}. Esquina {restaurante.direccion.esquina} ({restaurante.direccion.barrio})</Text>
                            </View>
                            
                            {/*                            PENDIENTE           agregar mapa       */}
                            <View style={{ margin: 10}}>
                                {/* <Text style={styles.filterTitle}>Mapa</Text>
                                <Text style={styles.text}>ubicacion: {restaurante.ubicacion.latitud} {restaurante.ubicacion.longitud}</Text>
                                <Text style={styles.text}>cobertura: {restaurante.cobertura}</Text>
                                <Text style={styles.text}>ubicacion: {Number(restaurante.ubicacion.latitud)} {Number(restaurante.ubicacion.longitud)}</Text>
                                <Text style={styles.text}>cobertura: {restaurante.cobertura}</Text>
                                <Text style={styles.text}>ubicacion: {Number(restaurante.ubicacion.latitud)} {Number(restaurante.ubicacion.longitud)}</Text>
                                <Text style={styles.text}>cobertura: {restaurante.cobertura}</Text> */}
                                {popupDetalles ? 
                                    <View style={styles.mapcontainer}>
                                        <Text style={styles.text}>popupDetalles</Text>
                                        <MapView
                                            provider={PROVIDER_GOOGLE}
                                            style={styles.map}
                                            initialRegion={{
                                                latitude: latitud,
                                                longitude: longitud,
                                                latitudeDelta: 0.02,
                                                longitudeDelta: 0.02,
                                            }}
                                            >
                                            <Marker coordinate={{latitude: latitud, longitude: longitud}} />
                                            <Circle 
                                                center={{latitude: latitud, longitude: longitud}} 
                                                radius={Number(restaurante.cobertura)}
                                                fillColor={colors.backRed}
                                                strokeWidth={2}
                                                strokeColor={colors.backRed}
                                            />
                                        </MapView>
                                    </View>
                                : null}
                            </View>
                        
                            <View style={{ marginHorizontal: 10}}>
                                <Text style={styles.filterTitle}>Horarios</Text>
                            </View>
                            <View style={styles.listaHoras}>
                                {restaurante.horarios.map((item, index) => {
                                    return(
                                        <View key={index} >
                                            {item.horaApertura != null && item.horaApertura != "" 
                                            && item.horaCierre != null && item.horaCierre != ""  ? 
                                                <Text style={styles.text}>{item.dia} {item.horaApertura} a {item.horaCierre}</Text>
                                            : (item.horaCierre == null || item.horaCierre == "")
                                            && (item.horaApertura != null && item.horaApertura != "") ?
                                                <Text style={styles.text}>{item.dia} a partir de las {item.horaApertura}</Text>
                                            : null }
                                        </View>
                                    )
                                })}
                            </View>
                        </ScrollView>
                    </Dialog>
                </Portal>

            </ScrollView>
        </ImageBackground>
    )
}
export default ListaMenusRestaurante;

const styles = StyleSheet.create({
    fondocontainer: {
        flex: 1,
        paddingTop: 10,
        // justifyContent: 'center', 
        // alignItems: 'center'
    },
    card: {
        backgroundColor: colors.background,
        margin: 10,
        borderRadius: 10,
        borderWidth: 3,
        padding:5
    },
    text: {
      color: "white",
    },
    container: {
        flex: 1,
        backgroundColor: colors.background,
        marginHorizontal: 10,
    },
    restauranteLogo: { 
        height:90, 
        width:90,
        borderRadius: 150 / 2,
        overflow: "hidden",
        borderWidth: 3,
    },
    filtrosContainer: {
        alignItems: 'center',
        alignContent: 'center',
        flexDirection: 'row',
        paddingVertical: 5,
    },
    filtroSearch: {
        flex: 9,
    },
    filtroIcon: {
        marginLeft: 5,
        alignItems: 'center',
        flex: 1,
    },
    listaContainer: {
        flex: 1,
    },
    listaItem: {
        backgroundColor: colors.white,
        marginVertical: 8,
        borderRadius: 10,
        padding: 5,
    },
    infoTouch: {
        //backgroundColor: colors.red,
        padding: 5,
    },
    listaCerrado: {
        backgroundColor: "gray",
        marginVertical: 8,
        borderRadius: 10,
        padding: 5,
    },
    itemCalif: {
        color: "green",
        fontSize: 13
    },
    itemCerrado: {
        color: "black",
        fontSize: 13
    },
    titleStyle: {
        color: colors.black,
        fontSize: 18
    },
    subtitleStyle: {
        color: colors.black,
        fontSize: 12
    },
    descuento: {
        color: colors.black,
        fontSize: 11,
        textDecorationLine: 'line-through'
    },
    precio: {
        color: colors.red,
        fontSize: 14,
        marginHorizontal: 5
    },
    tinyLogo: {
        height:40, 
        width:40,
    },
    btn: {
        marginTop: 20,
        // color: colors.white,
        // backgroundColor: colors.backBox,
        marginBottom: 20,
        marginHorizontal: 10
    },

    alert: {
        backgroundColor: colors.background,
    },
    alertText: {
        color: colors.white,
    },
    filterTitle: {
        margin: 5,
        color: colors.white,
        fontSize: 20,
    },
    row: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginHorizontal: 5
    },
    costoext: {
        flex: 1, 
        color: colors.white,
    },
    slider: {
        flex: 4, 
        marginHorizontal: 5, 
    },
    costoMax: {
        flex: 1, 
        color: colors.white,
    },
    scrolllistaCates: {
        // height: 200,
        // weight: 330,
    },
    listaCates: {
        margin: 2,
        flexWrap: 'wrap',
    },
    cateItem: {
        margin: 3,
        flexWrap: 'wrap',
    },
    colDetalles: {
        flex: 1, 
        // alignItems: 'flex-start', 
        //justifyContent: 'space-between',
        //paddingVertical: 5,
    },
    rowTitle: {
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between',
        marginHorizontal: 5,
        paddingVertical: 5,
        minWidth: "55%",
        maxWidth: "100%",
    },
    restauranteNombre: {
        marginVertical: 5,
        color: colors.white,
        fontSize: 20,
    },
    textCerrado: {
        color: colors.white,
        padding: 2,
        backgroundColor: colors.backRed, 
        borderRadius: 5,
    },
    contAceptaPedidos: {
        flexWrap: 'wrap',
        margin: 5,
    },
    textAceptaPedidos: {
        color: colors.white,
        paddingHorizontal: 5,
        backgroundColor: colors.backRed, 
        borderRadius: 5,
    },
    textCali: {
        color: "green",
        //fontSize: 14,
    },
    textCaliRestaurante: {
        color: "green",
        fontSize: 16,
    },
    rowCali: {
        flexDirection: 'row', 
        alignItems: 'center', 
        //backgroundColor: colors.backRed, 
        //paddingHorizontal: 15
    },
    detallesLogo: { 
        height:70, 
        width:70,
        marginRight: 10,
        borderRadius: 10,
        overflow: "hidden",
        borderWidth: 3,
    },
    listaHoras: {
        paddingHorizontal: 15,
        paddingBottom: 15,
    },
    mapcontainer: {
        //...StyleSheet.absoluteFillObject,
        height: 180,
        width: "100%",
        //justifyContent: 'flex-end',
        //alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
    listaPromosContainer: {
        //height: 100,
        marginLeft: 10,
        alignItems: 'stretch',
        justifyContent: 'space-around',
        //flexDirection: 'row',
    },
    promoContainer: {
        width: 340,
        justifyContent: 'space-around',
        backgroundColor: colors.white,
        margin: 10,
        borderRadius: 10,
    },
});