import React, {useState, useEffect} from 'react';
import { StyleSheet, ImageBackground , ScrollView, View} from 'react-native';
import { ActivityIndicator, List, Card, Button, Text, Paragraph, Dialog, Portal, Provider, TextInput as PaperText } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import Navbar from '../components/Navbar';
import { getSession, setDireccionesSession } from '../components/auth/auth'

import colors from '../assets/colors/colors';
//import ListaDirecciones from '../assets/data/ListaDirecciones';
//import ListaBarrios from '../assets/data/ListaBarrios';

import axiosCliente from "../components/axios/AxiosCliente";

Icon.loadFont();

const Direcciones = ({navigation, setLogueado}) => {
    // console.log(navigation);
    // console.log(setLogueado);

    //Mensaje de alerta
    const [visibleMsgSucces, setVisibleSucces] = useState(false);
    const [succesTexto, setSuccesTexto] = useState('');
    const [visibleMsgError, setVisibleError] = useState(false);
    const [errorTexto, setErrorTexto] = useState('');
    const hideDialogSucces = () => setVisibleSucces(false);
    const hideDialogError = () => setVisibleError(false);

    //Animaciónn de carga
    const [loading, setLoadingVisible] = useState(true);
    const setLoading = (load) => setLoadingVisible(load);

    //Lista direcciones
    const [misDirecciones, setListaDir] = useState([]);
    useEffect(() => {
        getListaDirecciones();
    }, []);

    //Lista Barrios    // No lo vamos a usar pero lo dejo como ejemplo de lista desplegable
    // const [listaBarrios, setBarrios] = useState(ListaBarrios);
    // const [expanded, setExpanded] = useState(false);
    // const handlePress = () => setExpanded(!expanded);

    //Datos dirección
    const [calle, setCalle] = useState('');
    const [numero, setNumero] = useState('');
    const [esquina, setEsquina] = useState('');
    const [barrio, setBarrio] = useState('');
    const onChangeCalle = calle => setCalle(calle);
    const onChangeNumero = numero => setNumero(numero);
    const onChangeEsquina = esquina => setEsquina(esquina);
    const onChangeBarrio = (barrio) => {
        setBarrio(barrio)
        //setExpanded(!expanded)
    }
    
    //Direccion seleccionada
    const [dirSelected, setDirSelected] = useState();

    //PopUp
    const [popupVisible, setVisiblePopUp] = useState(false);
    const hidePopUp = () => setVisiblePopUp(false);
    const [borrarVisible, setBorrarVisible] = useState(false);
    const [modo, setModo] = useState('INS');

    //PopUp agregar dirección
    const agregarNueva = () => {
        //Oculto boton de borrar del popup
        setBorrarVisible(false);

        //vacío estado
        setCalle('');
        setNumero('');
        setEsquina('');
        setBarrio('');
        setDirSelected();
        
        //Abro popup en modo Insert
        setVisiblePopUp(true);
        setModo('INS');
    };

    //PopUp modificar dirección
    const modificarVisible = (dir) => {
        //Muestro boton de borrar del popup
        setBorrarVisible(true);

        //Cargo datos editables en el estado
        setCalle(dir.calle);
        setNumero(dir.numero);
        setEsquina(dir.esquina);
        setBarrio(dir.barrio);

        //Me guardo la dirección seleccionada
        setDirSelected(dir);
        // console.log(dir);

        //Abro popup en modo Update
        setVisiblePopUp(true);
        setModo('UPD');
    }

    const getListaDirecciones = async () => {
        setLoading(true);

        let session = await getSession();

        let email = "";
        if(session != null && session.email != null){
            email = session.email;
        }
        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }

        axiosCliente
        .get(`/direcciones/${email}`,
        {   headers: {Authorization: `${token}`}    }
        )
        .then(function (response) {
            // console.log('getListaDirecciones');
            // console.log(response);
            if (response.status == 200) {
                setLoading(false);
                setListaDir(response.data);
                setDireccionesSession(response.data)
            } else {
                setErrorTexto(response.data);
                setVisibleError(true);
                setLoading(false);
            }
        })
        .catch(function (error) {
            // console.log(error.message);
            if (error.response.status == 400) {
                // Request made and server responded
                setErrorTexto(error.response.data);
                setVisibleError(true);
                setLoading(false);
            } else {
                // Something happened in setting up the request that triggered an Error
                setErrorTexto(error.message);
                setVisibleError(true);
                setLoading(false);
            }
        });
    }

    const agregarDireccion = async () => {
        setLoading(true);
        let session = await getSession();
        let email = "";
        if(session != null && session.email != null){
            email = session.email;
        }
        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        axiosCliente
        .post(`/direccion/agregar/${email}`, {
            calle: calle,
            numero: numero,
            esquina: esquina,
            barrio: barrio
        },
        {   headers: {Authorization: `${token}`}    }
        )
        .then(function (response) {
            // console.log(response);
            if (response.status == 200) {

                setVisiblePopUp(false);
                setSuccesTexto("Dirección agregada! Puedes continuar pidiendo tus comidas favoritas.");
                setVisibleSucces(true);

                //Me guardo todas las direcciones menos la seleccionada
                let nuevasDir = misDirecciones
                nuevasDir.push(response.data)
                setListaDir(nuevasDir)
                setDireccionesSession(nuevasDir)
                
                //vacío estado
                setCalle('');
                setNumero('');
                setEsquina('');
                setBarrio('');
                setDirSelected();
                setLoading(false);
            } else {
                setErrorTexto(response.data);
                setVisibleError(true);
                setLoading(false);
            }
        })
        .catch(function (error) {
            if (error.response.status == 400) {
                // Request made and server responded
                setErrorTexto(error.response.data);
                setVisibleError(true);
                setLoading(false);
            } else {
                // Something happened in setting up the request that triggered an Error
                setErrorTexto(error.message);
                setVisibleError(true);
                setLoading(false);
            }
        });
    }

    const modificarDireccion = async () => {
        setLoading(true);
        let session = await getSession();
        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        axiosCliente
        .put("/direccion/modificar", { 
            id: dirSelected.id,
            calle: calle,
            numero: numero,
            esquina: esquina,
            barrio: barrio
        },
        {   headers: {Authorization: `${token}`}    }
        )
        .then(function (response) {
            // console.log(response);
            if (response.status == 200) {
                setVisiblePopUp(false);
                setSuccesTexto("Dirección modificada con éxito.");
                setVisibleSucces(true);
                
                //vacío estado
                setCalle('');
                setNumero('');
                setEsquina('');
                setBarrio('');
                setDirSelected();
                setLoading(false);

                //Me guardo todas las direcciones menos la seleccionada
                let nuevasDir = misDirecciones.filter(d => (d.id != dirSelected.id ));
                //Agrego la modificada
                nuevasDir.push({id:dirSelected.id, calle:calle, numero:numero, esquina:esquina, barrio:barrio })

                setListaDir(nuevasDir)
            } else {
                setErrorTexto(response.data);
                setVisibleError(true);
                setLoading(false);
            }
        })
        .catch(function (error) {
            if (error.response.status == 400) {
                // Request made and server responded
                setErrorTexto(error.response.data);
                setVisibleError(true);
                setLoading(false);
            } else {
                // Something happened in setting up the request that triggered an Error
                setErrorTexto(error.message);
                setVisibleError(true);
                setLoading(false);
            }
        });
    }

    const eliminarDireccion = async () => {
        setLoading(true);
        let session = await getSession();
        let email = "";
        if(session != null && session.email != null){
            email = session.email;
        }
        let token = "";
        if(session != null && session.token != null){
            token = session.token;
        }
        axiosCliente
        .delete(`/direccion/eliminar/${email}/${dirSelected.id}`,
        {   headers: {Authorization: `${token}`}    }
        )
        .then(function (response) {
            // console.log(response);
            if (response.status == 200) {
                setVisiblePopUp(false);
                setSuccesTexto("Dirección eliminada con éxito.");
                setVisibleSucces(true);
                
                //vacío estado
                setCalle('');
                setNumero('');
                setEsquina('');
                setBarrio('');
                setDirSelected();
                setLoading(false);

                let nuevasDir = misDirecciones.filter(d => (d.id != dirSelected.id ));
                setListaDir(nuevasDir)
            } else {
                setErrorTexto(response.data);
                setVisibleError(true);
                setLoading(false);
            }
        })
        .catch(function (error) {
            if (error.response.status == 400) {
                // Request made and server responded
                setErrorTexto(error.response.data);
                setVisibleError(true);
                setLoading(false);
            } else {
                // Something happened in setting up the request that triggered an Error
                setErrorTexto(error.message);
                setVisibleError(true);
                setLoading(false);
            }
        });
    }

    //Continuar comprando
    const continuar = () => {
        if(misDirecciones.length == 0){
            setErrorTexto("Debe ingresar al menos una dirección.");
            setVisibleError(true);
        }else{
           //navigation.navigate('BottomTabNavigator')
           setLogueado.setLogueado(true);
        }
    }
    
    const [colorUnderCalle, setColorUnderCalle] = useState(colors.backBox);
    const [colorUnderNumero, setColorUnderNumero] = useState(colors.backBox);
    const [colorUnderEsquina, setColorUnderEsquina] = useState(colors.backBox);
    const [colorUnderBarrio, setColorUnderBarrio] = useState(colors.backBox);

    return(
        <Provider>
            <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
                {setLogueado && setLogueado.setLogueado ? 
                    null  //si estoy en el stack inicial no muestro el navbar
                :
                    <Navbar navigation={navigation} ocultarDireccion={true}/>
                }
                <ScrollView >
                    {/* <Image style={styles.tinyLogo} source={require('../assets/images/logoLetras.png')}/> */}
                    {loading ? 
                        <ActivityIndicator style={styles.loading} animating={true} color={colors.red} size="large"  />
                    :popupVisible?
                        <View>
                            <Card style={styles.card}>
                                
                                <PaperText dense={true} mode="flat" label="Calle" value={calle} 
                                    onChangeText={onChangeCalle} 
                                    style={styles.textInputPaper} 
                                    theme={{ colors: { text: colors.white, primary: colors.white} }}
                                />

                                <PaperText dense={true} mode="flat" label="Número" value={numero}
                                    onChangeText={onChangeNumero} 
                                    style={styles.textInputPaper} 
                                    theme={{ colors: { text: colors.white, primary: colors.white} }}
                                />

                                <PaperText dense={true} mode="flat" label="Esquina" value={esquina}
                                    onChangeText={onChangeEsquina} 
                                    style={styles.textInputPaper} 
                                    theme={{ colors: { text: colors.white, primary: colors.white} }}
                                />

                                <PaperText dense={true} mode="flat" label="Barrio" value={barrio}
                                    onChangeText={onChangeBarrio} 
                                    style={styles.textInputPaper} 
                                    theme={{ colors: { text: colors.white, primary: colors.white} }}
                                />
                                
                                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'space-around'}}>

                                    {borrarVisible ? 
                                        <Button mode="contained" style={styles.btn} onPress={eliminarDireccion}> Eliminar </Button>
                                    :null}
                                    {modo=='UPD' ? 
                                        <Button mode="contained" style={styles.btn} onPress={modificarDireccion}> Confirmar </Button>
                                    :
                                    <Button mode="contained" style={styles.btn} onPress={agregarDireccion}
                                        disabled={calle=='' || numero=='' || esquina=='' || barrio==''}
                                        >Agregar</Button>
                                    }
                                </View>
                            </Card>
                            <Button mode="contained" style={{...styles.btn, width: "90%", alignSelf: 'center'}} onPress={hidePopUp}> Volver </Button>
                        </View>
                    :
                        <Card style={styles.card}>
                            <Text style={styles.titleText}>Tus direcciones:</Text>

                            {/* Lista de durecciones del cliente */}
                            {misDirecciones.length > 0 ?
                                misDirecciones.map((dir, index) =>
                                    <List.Section key={index} style={styles.listSection}>
                                        <List.Section onPress={() =>modificarVisible(dir)}>
                                            <List.Item 
                                                style={styles.listItem} 
                                                titleStyle={styles.textDirTitulo}
                                                title={dir.calle + " " + dir.numero} 
                                                theme={{ colors: {text: colors.white} }}
                                                onPress={() =>modificarVisible(dir)}
                                                right={props => <Icon name="ios-pencil-sharp" size={20} color={colors.red} style={styles.listIcon}/>}
                                            />
                                            <List.Item 
                                                style={styles.listItem} 
                                                titleStyle={styles.textDirDesc}
                                                title={"Esquina: " + dir.esquina + " (" + dir.barrio + ")"} 
                                                theme={{ colors: {text: colors.white} }}
                                                onPress={() =>modificarVisible(dir)}/>
                                            </List.Section>
                                    </List.Section>
                                )
                            : 
                                null //<Text style={styles.textInput}>No has ingresado ninguna dirección aún.</Text>     
                            }
                            
                            <Button mode="contained" style={styles.btn} onPress={agregarNueva} 
                            >+ Agregar dirección</Button>
                        </Card>
                    }

                    {setLogueado && setLogueado.setLogueado ? 
                        <Button mode="contained" style={styles.btnContinuar} onPress={continuar} >Continuar</Button>
                    :
                        null  //si no estoy en el stack inicial no muestro el boton de continuar
                    }

                    <Portal>
                        {/* PopUp de agregar dirección */}
                        {/* <Dialog visible={popupVisible} onDismiss={hidePopUp} style={styles.alert}>
                            <Dialog.Title style={styles.alertText}>Detalles de dirección</Dialog.Title>
                            <Dialog.Content>
                                <PaperText dense={false} mode="flat" label="Calle" value={calle} 
                                    onChangeText={onChangeCalle} 
                                    style={styles.textInputPaper} 
                                    theme={{ colors: { text: colors.white, primary: colors.white} }}
                                />

                                <PaperText dense={false} mode="flat" label="Número" value={numero}
                                    onChangeText={onChangeNumero} 
                                    style={styles.textInputPaper} 
                                    theme={{ colors: { text: colors.white, primary: colors.white} }}
                                />

                                <PaperText dense={false} mode="flat" label="Esquina" value={esquina}
                                    onChangeText={onChangeEsquina} 
                                    style={styles.textInputPaper} 
                                    theme={{ colors: { text: colors.white, primary: colors.white} }}
                                />

                                <PaperText dense={false} mode="flat" label="Barrio" value={barrio}
                                    onChangeText={onChangeBarrio} 
                                    style={styles.textInputPaper} 
                                    theme={{ colors: { text: colors.white, primary: colors.white} }}
                                />
                            </Dialog.Content>
                            <Dialog.Actions style={{justifyContent: 'center'}}>
                                {borrarVisible ? 
                                    <Button mode="contained" style={styles.btn} onPress={eliminarDireccion}> Eliminar </Button>
                                :null}
                                {modo=='UPD' ? 
                                    <Button mode="contained" style={styles.btn} onPress={modificarDireccion}> Confirmar </Button>
                                :
                                <Button mode="contained" style={styles.btn} onPress={agregarDireccion}
                                    disabled={calle=='' || numero=='' || esquina=='' || barrio==''}
                                    >Agregar</Button>
                                }
                            </Dialog.Actions>
                        </Dialog> */}
                        
                        <Dialog visible={visibleMsgSucces} onDismiss={hideDialogSucces} style={styles.alert}>
                            <Dialog.Title style={styles.alertText}>Genial!</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph style={styles.alertText}>{succesTexto}</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button mode="contained" style={styles.btn} onPress={hideDialogSucces}>Entendido</Button>
                            </Dialog.Actions>
                        </Dialog>
                        <Dialog visible={visibleMsgError} onDismiss={hideDialogError} style={styles.alert}>
                            <Dialog.Title style={styles.alertText}>Ha ocurrido un error:</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph style={styles.alertText}>{errorTexto}</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button mode="contained" style={styles.btn} onPress={hideDialogError}>Entendido</Button>
                            </Dialog.Actions>
                        </Dialog>
                    </Portal>
                </ScrollView >
            </ImageBackground>
        </Provider>
    )
}
export default Direcciones;

const styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        flex:1,
        alignContent:"center",
        justifyContent:"center",
    },
    tinyLogo: {
        height:60, 
        width:150,
        marginHorizontal: 120,
    },
    card: {
        padding: 15,
        //marginHorizontal: 20,
        marginVertical: 10,
        alignContent:"center",
        backgroundColor: "#414141", //colors.backBox,
        borderRadius: 10,
        borderWidth: 3,
        borderEndColor: '#ffffff',
        width:"95%",
        alignSelf:"center",
    },
    titleText: {
        color: colors.white,
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 20,
        alignSelf: "center"
        //marginHorizontal: 90
    },
    textInputPaper: {
        backgroundColor: "#ffffff00",
        fontSize: 15,
        marginBottom: 10,
        color: colors.white,
    },
    textInput: {
        backgroundColor: "#ffffff00",
        fontSize: 15,
        marginBottom: 10,
        color: colors.white,
        textDecorationLine: 'underline',
        borderBottomWidth: 1,
        borderBottomColor: colors.white,
    },
    listSection: {
        backgroundColor: colors.backBox
    },
    listAccordion: {
        backgroundColor: colors.backBox
    },
    textDirTitulo: {
        fontSize: 15,
    },
    textDirDesc: {
        fontSize: 13,
    },
    listItem: {
        padding: 0,
    },
    listIcon: {
        margin: 10,
    },
    btn: {
        marginTop: 20,
        color: colors.white,
        backgroundColor: colors.red,
        marginBottom: 20,
        marginHorizontal: 10
    },
    btnContinuar: {
        marginTop: 20,
        color: colors.white,
        backgroundColor: colors.red,
        marginBottom: 20,
        marginHorizontal: 20,
    },
    alert: {
        backgroundColor: colors.background,
    },
    alertText: {
        color: colors.white,
    },
    loading: {
        marginVertical: 20,
    },
});