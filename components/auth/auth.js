import AsyncStorage from '@react-native-async-storage/async-storage';
import { DeviceEventEmitter } from 'react-native';

// Guarda un objeto en el local storage
export const setSession = async (session) => {
    try {
        const jsonSession = JSON.stringify(session)
        await AsyncStorage.setItem('@storage_Key', jsonSession)
        return jsonSession
        // Alert.alert("Guardar datos con éxito: " + user)
    } catch(e) {
        // Alert.alert ("Error al guardar los datos: " + e)
        return null
    }
};

// Lee un objeto en el local storage
export async function getSession() {
    try {
        const jsonValue = await AsyncStorage.getItem('@storage_Key')
        // console.log('jsonValue en auth: ')
        // console.log(jsonValue)
        return JSON.parse(jsonValue);
    } catch(e) {
        return e
    }
};

// Borra un objeto en el local storage
export const endSession = async () => {
    try {
        // const jsonSession = JSON.stringify(null)
        await AsyncStorage.removeItem('@storage_Key')
        // return jsonSession
        // Alert.alert("Guardar datos con éxito: " + user)
    } catch(e) {
        // Alert.alert ("Error al guardar los datos: " + e)
        // return null
    }
};

export const setDireccionesSession = async (direcciones) => {
    try {
        const jsonValue = await AsyncStorage.getItem('@storage_Key')
        let session = JSON.parse(jsonValue);
        session.direcciones = direcciones;
        const jsonSession = JSON.stringify(session)
        await AsyncStorage.setItem('@storage_Key', jsonSession)
    } catch(e) {
        // Alert.alert ("Error al guardar los datos: " + e)
        return null
    }
};

export const agregarAlCarrito = async (menu) => {
    try {
        const jsonValue = await AsyncStorage.getItem('@carrito')
        let carrito = [];
        if(jsonValue!=null){
            carrito = JSON.parse(jsonValue);
        }
        carrito.push(menu)
        
        // console.log('agregarAlCarrito')
        // console.log(carrito)

        DeviceEventEmitter.emit('incrementarBadge', {carrito});
        const jsonCarrito = JSON.stringify(carrito)
        await AsyncStorage.setItem('@carrito', jsonCarrito)
    } catch(e) {
        return null
    }
};

export const getCarrito = async () => {
    try {
        const jsonValue = await AsyncStorage.getItem('@carrito')
        return JSON.parse(jsonValue);
    } catch(e) {
        return null
    }
};

export const vaciarCarrito = async () => {
    try {
        await AsyncStorage.removeItem('@carrito')
        DeviceEventEmitter.emit('vaciarBadge');
    } catch(e) {
        // return null
    }
};

export const quitarItem = async (prod) => {
    try {
        const jsonValue = await AsyncStorage.getItem('@carrito')
        let carrito = [];
        if(jsonValue!=null){
            carrito = JSON.parse(jsonValue);
        }

        var index = carrito.findIndex(i => (i.id == prod.id && i.descuento == prod.descuento));
        if (index > -1) {
            carrito.splice(index, 1);
        }

        DeviceEventEmitter.emit('incrementarBadge', {carrito});
        const jsonCarrito = JSON.stringify(carrito)
        await AsyncStorage.setItem('@carrito', jsonCarrito)
    } catch(e) {
        return null
    }
};

//Agrega el pedido y retorna un indice
export const agregarPedido = async (pedido) => {
    // console.log('agregarPedido')
    // console.log(pedido)

    try {
        const jsonValue = await AsyncStorage.getItem('@pedidos')
        let pedidos = [];
        if(jsonValue!=null){
            pedidos = JSON.parse(jsonValue);
        }
        pedidos.push({...pedido, idLocal: pedidos.length+1})

        DeviceEventEmitter.emit('actualizarPedidos');
        const jsonPedidos = JSON.stringify(pedidos)
        await AsyncStorage.setItem('@pedidos', jsonPedidos)
        
        // console.log(pedidos)
        // console.log(pedidos.length)

        let idLocal = pedidos.length
        return idLocal
    } catch(e) {
        return null
    }
};

export const getPedido = async (id) => {
    // console.log('getPedido')
    // console.log(id)

    try {
        const jsonValue = await AsyncStorage.getItem('@pedidos')
        let pedidos = [];
        if(jsonValue!=null){
            pedidos = JSON.parse(jsonValue);
        }
        const pedido = pedidos.find(p => p.idLocal == id);
        
        // console.log(pedidos)
        // console.log(pedido)

        return pedido;
    } catch(e) {
        return null
    }
};

export const getPedidoIdPedido = async (idPedido) => {
    // console.log('getPedido')
    // console.log(id)

    try {
        const jsonValue = await AsyncStorage.getItem('@pedidos')
        let pedidos = [];
        if(jsonValue!=null){
            pedidos = JSON.parse(jsonValue);
        }
        const pedido = pedidos.find(p => p.idPedido == idPedido);
        
        // console.log(pedidos)
        // console.log(pedido)

        return pedido;
    } catch(e) {
        return null
    }
};

export const vaciarPedidos = async () => {
    try {
        await AsyncStorage.removeItem('@pedidos')
        //DeviceEventEmitter.emit('vaciarPedidos');
    } catch(e) {
        // return null
    }
};

export const quitarPedido = async (id) => {
    try {
        const jsonValue = await AsyncStorage.getItem('@pedidos')
        let pedidos = [];
        if(jsonValue!=null){
            pedidos = JSON.parse(jsonValue);
        }

        var index = pedidos.findIndex(i => (i.idLocal == id));
        if (index > -1) {
            pedidos.splice(index, 1);
        }

        //DeviceEventEmitter.emit('actualizarPedidos', {pedidos});
        const jsonPedidos = JSON.stringify(pedidos)
        await AsyncStorage.setItem('@pedidos', jsonPedidos)
    } catch(e) {
        return null
    }
};

export const actualizarPedido = async (pedido) => {
    try {
        const jsonValue = await AsyncStorage.getItem('@pedidos')
        let pedidos = [];
        if(jsonValue!=null){
            pedidos = JSON.parse(jsonValue);
        }

        var index = pedidos.findIndex(i => (i.idPedido == pedido.idPedido));
        if (index > -1) {
            pedidos[index] = pedido;
        }

        //DeviceEventEmitter.emit('actualizarPedidos', {pedidos});
        const jsonPedidos = JSON.stringify(pedidos)
        await AsyncStorage.setItem('@pedidos', jsonPedidos)
    } catch(e) {
        return null
    }
};

export const setPedidos = async (pedidos) => {
    let cont = 0;
    let listaPedidos = pedidos.map(p => {
        cont += 1;
        return{
            ...p,
            idLocal : cont
        }
    });
    
    try {
        const jsonPedidos = JSON.stringify(listaPedidos)
        await AsyncStorage.setItem('@pedidos', jsonPedidos)
        return jsonPedidos
    } catch(e) {
        return null
    }
};