import React, {useState} from 'react';
import { StyleSheet, ScrollView, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { Card, Button, Text, DarkTheme, Dialog, Portal, Provider, Paragraph } from 'react-native-paper';
import Feather from 'react-native-vector-icons/Feather';

import colors from '../assets/colors/colors';
import Navbar from '../components/Navbar';
import { agregarAlCarrito, getCarrito, vaciarCarrito } from '../components/auth/auth'

const Menu = ({route, navigation}) => {
    const { restaurante, menu, usaPromo } = route.params;
    // console.log(navigation)
    // console.log(menu)
    // console.log(usaPromo)

    //Animaciónn de carga
    const [loading, setLoading] = useState(false);

    //Mensaje
    const [visibleMsg, setVisibleMsg] = useState(false);
    const [titleMsg, setTitleMsg] = useState('');
    const [textoMsg, setTextoMsg] = useState('');
    
    const [cantidad, setCantidad] = useState(1);
    // const [aclaraciones, setAclaraciones] = useState('');
    // const onChangeAclaraciones = aclaraciones => setAclaraciones(aclaraciones);

    const control = async () => {
        setLoading(true);
        
        let carrito = await getCarrito();
        
        let error = false;
        if(carrito != null){
            carrito.forEach( item => {
                if(item.idRestaurante != restaurante.idRestaurante){
                    error = true;
                }
            })
        }

        if(error){
            setVisibleMsg(true);
            setTitleMsg(`Hay productos de otro restaurante en tu carrito`);
            setTextoMsg(`Si continúas se borrarán los otros productos de tu carrito`);
        }else{
            agregarMenu()
        }
    }
    
    const vaciarYAgregar = async () => {
        setVisibleMsg(false)
        await vaciarCarrito();
        agregarMenu()
    }

    const agregarMenu = async () => {
        setLoading(true);
        let porcDesc = 0
        let total = menu.precio
        if(usaPromo && usaPromo == true && menu.promocion.descuento != 0){
            porcDesc = menu.promocion.descuento
            total = menu.precio - (menu.precio * porcDesc /100)
        }
        let m = null;
        for (let i = 0; i < cantidad; i++){
            m = {
                idRestaurante: restaurante.idRestaurante, 
                emailRestaurante: restaurante.email, 
                nombreRestaurante: restaurante.nombre, 
                imagenRestaurante: restaurante.imagen, 
                costoEnvio: restaurante.costoEnvio, 
                id: menu.id,
                titulo: menu.titulo,
                imagen: menu.imagen,
                precio: menu.precio,
                descuento: porcDesc,
                total: total,
                //aclaraciones: aclaraciones,
            }
            // console.log('agregarMenu')
            // console.log(m)
            
            await agregarAlCarrito(m);
        }
        
        setCantidad(1)
        setLoading(false);
    }

    return(
        <ImageBackground style={styles.container} source={require('../assets/images/fondo.jpg')}>
            <Navbar navigation={navigation}/>
            <ScrollView>
                <Provider>
                    <Card style={styles.card}>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            
                            <View style={{flex: 1, width: "90%"}}>
                                <Image style={styles.imagen} source={{uri: menu.imagen}}/>
                            </View>

                            <View style={styles.right}>
                                <Text style={styles.textPrecio}>$ {menu.precio}</Text>
                            </View>
                            <View style={{...styles.left, marginBottom: 20}}>
                                    <Text style={styles.textTitulo}>{menu.titulo}</Text>
                                    <Text style={styles.text}>{menu.descripcion}</Text>
                            </View>
                            
                            <View style={{flex: 1, width: "90%", justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                                <View style={styles.left}>
                                    {menu.promocion && usaPromo && usaPromo==true ? 
                                        <View>
                                            <Text style={styles.textDescuento}>  $ {menu.precio * cantidad}  </Text>
                                            <Text style={styles.text}> - %{(menu.promocion.descuento)}</Text>
                                            <Text style={styles.textTitulo}>$ {(menu.precio * cantidad) - ((menu.precio * cantidad) * menu.promocion.descuento /100)}</Text>
                                        </View>
                                    : 
                                        <Text style={styles.text}>   $ {menu.precio * cantidad}</Text>
                                    }
                                </View>
                                <View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'center', flexDirection: 'row'}}>
                                    <TouchableOpacity onPress={() => setCantidad(cantidad-1)} disabled={cantidad<=1}>
                                        {cantidad<=1 ?
                                            <Feather name="minus-circle" style={styles.cantBtn} size={20} color={colors.backRed}/> 
                                        :
                                            <Feather name="minus-circle" style={styles.cantBtn} size={20} color={colors.red}/> 
                                        }
                                    </TouchableOpacity>
                                    <Text style={styles.text}>{cantidad}</Text>
                                    <TouchableOpacity onPress={() => setCantidad(cantidad+1)}>
                                        <Feather name="plus-circle" style={styles.cantBtn} size={20} color={colors.red}/> 
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* <View style={styles.center}>
                                <TextInput dense={false} mode="flat" label="Aclaraciones" value={aclaraciones} 
                                    onChangeText={onChangeAclaraciones} 
                                    style={styles.textInput} 
                                    theme={{ colors: { text: colors.white, primary: colors.white} }}
                                />
                            </View> */}

                            <View style={styles.center}>
                                <Button mode="contained" style={styles.btn} onPress={control} 
                                disabled={loading} 
                                loading={loading}
                                >Agregar al carrito</Button>
                            </View>
                        </View>
                    </Card>

                    <Portal>
                        <Dialog visible={visibleMsg} onDismiss={() => setVisibleMsg(false)} theme={DarkTheme}>
                            <Dialog.Title style={styles.textTitulo}>{titleMsg}</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph style={styles.text}>{textoMsg}</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button theme={{ colors: {primary : colors.red}}} onPress={vaciarYAgregar}
                                >Ok</Button>
                            </Dialog.Actions>
                        </Dialog>
                    </Portal>
                </Provider>
            </ScrollView>
        </ImageBackground>
    )
}
export default Menu;

const styles = StyleSheet.create({
    card: {
        backgroundColor: colors.background,
        margin: 10,
        borderRadius: 10,
        borderWidth: 3,
    },
    container: {
        flex: 1,
        backgroundColor: colors.background,
    },
    imagen: {
        height:200, 
        marginVertical: 20,
        borderRadius: 10,
        overflow: "hidden",
        borderWidth: 3,
    },
    text: {
        color: "white",
        fontSize: 14
    },
    textTitulo: {
        color: "white",
        fontSize: 17
    },
    textPrecio: {
        //marginVertical: 10,
        color: "white",
        fontSize: 16
    },
    textAclaraciones: {
        marginTop: 10,
        color: "white",
        fontSize: 16
    },
    textDescuento: {
        color: colors.red,
        fontSize: 15,
        textDecorationLine: 'line-through'
    },
    left: {
        flex: 1, 
        width: "90%", 
        justifyContent: 'center', 
        alignItems: 'flex-start'
    },
    right: {
        flex: 1, 
        width: "90%", 
        justifyContent: 'center', 
        alignItems: 'flex-end'
    },
    center: {
        flex: 1, 
        width: "90%", 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    btn: {
        marginVertical: 20,
        color: colors.white,
        backgroundColor: colors.red,
    },
    textInput: {
        width: "100%",
        backgroundColor: "#ffffff00",
        fontSize: 15,
        marginBottom: 10
    },
    cantBtn: {
        marginHorizontal: 10,
    },
});