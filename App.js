import 'react-native-gesture-handler';
import * as React from 'react';
import { SafeAreaView, StatusBar} from 'react-native';

import SplashScreen from './screens/SplashScreen';

import {LogBox } from 'react-native';
LogBox.ignoreLogs(['Reanimated 2']);

export default function App() {
  return (
    <SafeAreaView style={{flex: 1}}>
      {/* <StatusBar
        animated={true}
        //backgroundColor="#61dafb"
        barStyle={'light-content'}
        hidden={true} /> */}
      <SplashScreen/>
    </SafeAreaView>
  );
}
