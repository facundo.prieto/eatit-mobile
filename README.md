# [Documento](https://drive.google.com/file/d/1P1Ms7CWoTAApYSv9MsenbfVciqOk7L6c/view?usp=sharing) 

# Screenshots  

![Login screen](login.png?raw=true)  
![Start screen](inicio.png?raw=true)  
![Restaurant screentext](restaurante.png?raw=true)  
![Menu screen](menu.png?raw=true)   

# Primeros pasos  

- Abrir cmd en modo administrador  
- Instalar Chocolatey ejecutando 
`@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "[System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"`
- Run `choco install -y nodejs.install openjdk8`  
- Seguir los pasos de instalacion de Android Studio de el siguiente link: [Link](https://reactnative.dev/docs/environment-setup)  
- Pararse en la carpeta que queramos clonar el proyecto  
- Clonar Proyecto: `git@gitlab.com:proyecto-tecnologo/proyecto-mobile.git`  
- Borrar el archivo package-lock.json si existe en el directorio raiz  
- Instalar dependencias con `npm install`  
- Si no anda el install hacer  `npm install --legacy-peer-deps`
- Si no reconoce las fuentes ejeutar `npx react-native link`  

# Ejecutar

- Correr el comando: `npx react-native run-android`

# Descargar aplicación para Android
- [Descarga aquí](https://drive.google.com/uc?export=download&id=1JeQFGaVbdI5DoIa-VJt3l2qkzYxv8f99) 

# Firebase  
### Links
- [Firebase Auth](https://console.firebase.google.com/)
- [Firebase Cloud](https://console.cloud.google.com/)
### Credenciales
- Usuario: eatituy@gmail.com  
- Contraseña: 2021proyecto  

# Variables de entorno
Se deben tener definidas las siguientes variables de entorno:  
- JAVA_HOME	 		C:\Program Files\Java\jdk1.8.0_231  
- ANDROID_HOME 		C:\Users\usaruio\AppData\Local\Android\Sdk  
- ANDROID_SDK_ROOT 	C:\Users\usaruio\AppData\Local\Android\Sdk  


# Generación de apk   
- Desde una terminal ubicada en la carpeta del proyecto ejecutar:   
`react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/`  

- Al finalizar el comando anterior, eliminar de la ruta `~\android\app\src\main\res` las carpetas que comienzan con el prefijo `drawable-`

- Nuevamente desde la terminal ubicada en la carpeta del proyecto ejecutar:  
`cd android`  
`gradlew clean`  
`gradlew assembleRelease  --no-daemon`  

- El apk generada se encontrará en la carpeta `~\android\app\build\outputs\apk\release\`
